#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		int strStr(string haystack, string needle);
};

int main()
{
	Solution mysol;
	string haystack = "", needle = "";
	cout << "Enter haystack: ";
	cin >> haystack;
	cout << "Enter needle: ";
	cin >> needle;

	// string haystack = "abcdkmncdefgh", needle = "cde";

	cout << mysol.strStr( haystack, needle ) << endl;
	return 0;
}

int Solution::strStr(string haystack, string needle)
{
	int lenh = haystack.length(), lenn = needle.length(), next = 0, start = 0, i = 0;

	if ( lenn == 0 ) return 0;
	else if ( lenh == 0 ) return -1;

	while( i <= lenh-lenn )
	{

		if ( haystack[i] != needle[0] ) i++;
		else
		{
			start = i;
			next = ++i;
			int j;
			for(j=1;j<lenn;j++)
			{
				if ( haystack[i++] != needle[j] ) { i = next; break; }
			}
			
			if ( j == lenn ) return start;

		}
		cout << "i=" << i << endl;

	}

	return -1;
}
