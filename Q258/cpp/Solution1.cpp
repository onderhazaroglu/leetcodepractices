#include <iostream>
using namespace std;

class Solution
{
	public:
		int addDigits(int num);
};

int main()
{
	Solution mysol;

	int num;
	cout << "Enter num: ";
	cin >> num;

	cout << mysol.addDigits( num ) << endl;
	return 0;
}

int Solution::addDigits(int num)
{
	int temp;

	while(num>9)
	{
		cout << "Num: " << num << endl;
		temp = num;
		num = 0;
		while(temp>0)
		{
			num += temp % 10;
			cout << "num + temp %10 = " << num << endl;
			temp /= 10;
		}
	}
	return num;
}
