#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		string licenseKeyFormatting(string S, int K);
};

int main()
{
	Solution mysol;
	string S = "";
	int K;
	cout << "Enter license: ";
	cin >> S;
	cout << "Enter K: ";
	cin >> K;
	cout << mysol.licenseKeyFormatting(S,K) << endl;
	return 0;
}

string Solution::licenseKeyFormatting(string S, int K)
{
	string oldS = S;
	S = "";
	int grp = 0;
	string res;
	for(auto c: oldS)
	{
		if(c!='-') S += toupper(c);
	}
	// S.erase(remove(S.begin(), S.end(), '-'), S.end());
	// std::transform(S.begin(), S.end(),S.begin(), ::toupper);
	cout << "S: " << S << endl;
	while(!S.empty())
	{
		if(grp++>0) res = "-" + res;
		cout << "A" << endl;
		res = string((int(S.length())>K?S.end()-K:S.begin()),S.end()) + res;
		cout << "B" << endl;
		S = string(S.begin(),(int(S.length())>K?S.end()-K:S.begin()));
		cout << "C" << endl;
		cout << "res: " << res << ", S: " << (S.empty()?"":S) << endl;
	}

	cout << "res: " << res << endl;
	// for(auto & c: res) c = toupper(c);
	return res;
}
