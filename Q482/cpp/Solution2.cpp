#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>
using namespace std;

class Solution
{
	public:
		string licenseKeyFormatting(string S, int K);
};

int main()
{
	Solution mysol;
	string S = "";
	int K;
	cout << "Enter license: ";
	cin >> S;
	cout << "Enter K: ";
	cin >> K;
	cout << mysol.licenseKeyFormatting(S,K) << endl;
	return 0;
}

string Solution::licenseKeyFormatting(string S, int K)
{
	S = string(S.rbegin(),S.rend());
	string res;

	int i = 0;
	for(char c: S)
	{
		if(c=='-') continue;
		if(i++>0 && (i-1)%K==0) res.push_back('-');
		cout << "tu: " << (c>=97 && c<=122?char(c-32):c) << endl;
		res.push_back(c>=97 && c<=122?char(c-32):c);
		cout << "res: " << res << endl;
	}
	return string(res.rbegin(),res.rend());
}
