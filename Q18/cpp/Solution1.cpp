#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>
#include <algorithm>
#include <string>

using namespace std;

class Solution
{
	public:
		vector<vector<int>> fourSum(vector<int>& nums,int target);
};


int main()
{
	Solution mysol;

	// int my_ints[] = {-1, 0, 1, 2, -1, -4, 3,5,-3,1,0,0,7,-7};
	// int my_ints[] = {-1, 0, 1, 2, -1, -4};

	// vector<int> my_nums (&my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) );
	int target;
	vector<int> my_nums;
	string num = "";
	while(num != "q")
	{
		cout << "Enter number: ";
		cin >> num;
		if(num != "q") my_nums.push_back( stoi(num) );
		else break;
	}

	cout << "Enter target: ";
	cin >> target;

	vector<vector<int>> results = mysol.fourSum( my_nums, target);

	for(auto it: results)
	{
		cout << "[" << it[0] << "]" << "[" << it[1] << "]" << "[" << it[2] << "]" << "[" << it[3] << "]" << endl;
	}

	return 0;
}


vector<vector<int>> Solution::fourSum(vector<int>& nums,int target)
{
	vector<vector<int>> results;
	unsigned int size = nums.size();
	cout << "size:" << size << endl;
	if(size<4) return results;

	set<vector<int>> mquadruplets;

	std::sort(nums.begin(),nums.end());
	cout << "A:" << endl;

	for(unsigned int i=0;i<size;i++)
	{
		cout << "B" << i << endl;
		if(i>0 && nums[i] == nums[i-1]) continue;
		if(nums[i]+nums[i+1]+nums[i+2]+nums[i+3]>target) break;
		if(nums[i]+nums[size-3]+nums[size-2]+nums[size-1]<target) continue;
		int target_3 = target - nums[i];

		for(unsigned int j=i+1;j<size;j++)
		{
			if(j>i+1 && nums[j] == nums[j-1]) continue;
			if(nums[i]+nums[j]+nums[j+1]+nums[j+2]>target) break;
			if(nums[i]+nums[j]+nums[size-2]+nums[size-1]<target) continue;

			int target_2 = target_3 - nums[j];
			unsigned int front = j+1, back = size-1;

			while(front < back)
			{
				cout << "Processing " << nums[i] << "," << nums[j] << "," << nums[front] << "," << nums[back] << " target:" << target << ", target_3:" << target_3 << ", target_2:" << target_2 << endl;
				int sum = nums[front] + nums[back];

				if ( sum > target_2 ) { back--; cout << "op " << nums[i] << ", " << nums[j] << ", " << nums[front] << ", " << nums[back] << ", sum:" << sum << ", target_2:" << target_2 << endl; }
				else if ( sum < target_2 ) { front++; cout << "op " << nums[i] << ", " << nums[j] << ", " << nums[front] << ", " << nums[back] << ", sum:" << sum << ", target_2:" << target_2 << endl; }
				else
				{
					cout << "found " << nums[i] << ", " << nums[j] << ", " << nums[front] << ", " << nums[back] << endl;
					int my_ints[] = { nums[i], nums[j], nums[front], nums[back] };
					mquadruplets.insert( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );
					front++;
				}	
			}
		}
	}

	for(auto it: mquadruplets)
	{
		results.push_back( it );
	}

	return results;
}
