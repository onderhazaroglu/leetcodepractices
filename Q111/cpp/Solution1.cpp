#include <iostream>
#include <stack>
#include <queue>
#include <climits>
#include <algorithm>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		int minDepth(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->left->left = new TreeNode(5);

	print_tree(r1);
	cout << "min: " << mysol.minDepth(r1) << endl;
	print_tree(r1);
	cout << "min: " << mysol.minDepth(r2) << endl;
	return 0;
}

int Solution::minDepth(TreeNode* root)
{
	if(!root) return 0;
	int lev = 0;
	queue<TreeNode*> q;
	q.push(root);
	while(!q.empty())
	{
		lev++;
		int s = q.size();
		for(int i=0;i<s;i++)
		{
			TreeNode* t = q.front();
			q.pop();
			if(!t->left && !t->right) return lev;
			if(t->left) q.push(t->left);
			if(t->right) q.push(t->right);
		}
	}
	return lev;
}
