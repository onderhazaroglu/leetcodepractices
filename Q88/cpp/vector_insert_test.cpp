#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector<int> nums = {1,2,3,4,5};
	nums.insert(nums.begin(),7);
	nums.insert(nums.begin()+2,7);
	for(int i: nums) cout << i << " ";
	cout << endl;
	return 0;
}
