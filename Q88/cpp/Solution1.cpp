#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		void merge(vector<int>& nums1, int m, vector<int>& nums2, int n);
};

int main()
{
	Solution mysol;
	// vector<int> nums1 = {3,5,7,8,9,10,11}, nums2 = {1,2,3,4,6,8,10};
	vector<int> nums1 = {1,3,5,6,7,8}, nums2 = {1,2,4,6,8,10};
	int m = nums1.size(),n=nums2.size();
	mysol.merge(nums1,m,nums2,n);
	for(int i: nums1) cout << i << " ";
	cout << endl;
	return 0;
}

void Solution::merge(vector<int>& nums1, int m, vector<int>& nums2, int n)
{
	vector<int> res(m+n,0);
	int i=0,j=0, count=0;
	cout << m << endl;
	while(i<m || j<n)
	{
		cout << "i: " << i << "(" << nums1[i] << "), j: " << j << "(" << nums2[j] << ")" << endl;
		for(int k: nums1) cout << k << " ";
		cout << " (" << nums1.size() << ")" << endl;
		if(nums2[j]<=nums1[i]) res[count++] = nums2[j++];
		else res[count++] = nums1[i++];
	}
	nums1 = vector<int>(res);
}
