#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		void merge(vector<int>& nums1, int m, vector<int>& nums2, int n);
};

int main()
{
	Solution mysol;
	// vector<int> nums1 = {3,5,7,8,9,10,11}, nums2 = {1,2,3,4,6,8,10};
	vector<int> nums1 = {1,3,5,6,7,8}, nums2 = {1,2,4,6,8,10};
	int m = nums1.size(),n=nums2.size();
	mysol.merge(nums1,m,nums2,n);
	for(int i: nums1) cout << i << " ";
	cout << endl;
	return 0;
}

void Solution::merge(vector<int>& nums1, int m, vector<int>& nums2, int n)
{
	cout << m << n << endl;
	std::vector<int> dst;
	std::merge(nums1.begin(), nums1.end(), nums2.begin(), nums2.end(), std::back_inserter(dst));
	nums1 = dst;
}
