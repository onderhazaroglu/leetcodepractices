#include <iostream>
#include <stack>
#include <unordered_map>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		bool hasPathSum(TreeNode* root, int sum);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(5);
	r1->left = new TreeNode(4);
	r1->right = new TreeNode(8);
	r1->left->left = new TreeNode(11);
	r1->left->left->left = new TreeNode(7);
	r1->left->left->right = new TreeNode(2);
	r1->right->left = new TreeNode(13);
	r1->right->right = new TreeNode(4);
	r1->right->right->right = new TreeNode(1);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	print_tree(r1);

	cout << mysol.hasPathSum(r1,26) << endl;

	return 0;
}

bool Solution::hasPathSum(TreeNode* root, int sum)
{
	if(!root) return false;
	stack<TreeNode*> s;
	unordered_map<TreeNode*,int> ms;
	s.push(root);
	ms[root] = 0;
	while(!s.empty())
	{
		TreeNode* t = s.top();
		cout << "t: " << (t?t->val:-1) << ", ms[" << t->val << "]: " << ms[t] << ", sum: " << ms[t]+t->val;
		s.pop();
		if(!t->left && !t->right)
		{
			cout << " (leaf)" << endl;
			if(ms[t]+t->val==sum) return true;
		}
		else cout << endl;

		if(t->right) s.push(t->right), ms[t->right] = ms[t]+t->val;
		if(t->left) s.push(t->left), ms[t->left] = ms[t]+t->val;
	}
	return false;
}
