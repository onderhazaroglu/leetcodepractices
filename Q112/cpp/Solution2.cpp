#include <iostream>
#include <stack>
#include <unordered_map>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		bool checkSum(TreeNode* root, int target, int sum);
		bool hasPathSum(TreeNode* root, int sum);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(5);
	r1->left = new TreeNode(4);
	r1->right = new TreeNode(8);
	r1->left->left = new TreeNode(11);
	r1->left->left->left = new TreeNode(7);
	r1->left->left->right = new TreeNode(2);
	r1->right->left = new TreeNode(13);
	r1->right->right = new TreeNode(4);
	r1->right->right->right = new TreeNode(1);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	print_tree(r1);

	cout << mysol.hasPathSum(r1,26) << endl;

	return 0;
}

bool Solution::checkSum(TreeNode* root, int target, int sum)
{
	if(!root) return false;
	sum += root->val;
	if(!root->left && !root->right) return target==sum;
	else return checkSum(root->left,target,sum) || checkSum(root->right,target,sum);
}

bool Solution::hasPathSum(TreeNode* root, int sum)
{
	if(!root) return false;
	return checkSum(root,sum,0);
}
