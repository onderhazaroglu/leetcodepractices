#include <iostream>
#include <bitset>
using namespace std;

class Solution
{
	public:
		bool isPowerOfTwo(int n);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter number: ";
	cin >> num;
	cout << mysol.isPowerOfTwo(num) << endl;
	return 0;
}

bool Solution::isPowerOfTwo(int n)
{
	bitset<32> bn = n;
	return bn.count()==1;
}
