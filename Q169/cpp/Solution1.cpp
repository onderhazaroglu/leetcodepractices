#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		int majorityElement(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,3,4,2,1,3,4};
	cout << mysol.majorityElement(nums) << endl;
	return 0;
}

int Solution::majorityElement(vector<int>& nums)
{
	sort(nums.begin(),nums.end());
	int size = nums.size();
	unordered_map<int,int> map;
	for(int i=0;i<size;i++)
	{
		if(++map[nums[i]]>(size/2)) return nums[i];
	}
	return 0;
}
