#include <iostream>
#include <unordered_map>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int dominantIndex(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {3,6,1,0,40,8,10,19,2};
	cout << mysol.dominantIndex(nums) << endl;
	return 0;
}

int Solution::dominantIndex(vector<int>& nums)
{
	int size = nums.size();
	unordered_map<int,int> m;
	for(int i=0;i<size;i++)
	{
		m[nums[i]] = i;
	}
	sort(nums.begin(),nums.end());
	return nums[size-1]>=(2*nums[size-2])?m[nums[size-1]]:-1;
}
