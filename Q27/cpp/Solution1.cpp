#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int removeElement(vector<int>& nums, int val);
};

int main()
{
	Solution mysol;
	vector<int> nums = {3,2,2,3};

	cout << mysol.removeElement( nums, 3 ) << endl;

	return 0;
}

int Solution::removeElement(vector<int>& nums, int val)
{
	int size = nums.size(), k = 0;
	if ( size == 0 ) return k;
	else if ( size == 1 && nums[0] == val ) { nums[0] = 0; return k; }

	for(int i=0;i<size;i++)
	{
		if ( nums[i] != val ) nums[k++] = nums[i];
	}
	
	return k;
}
