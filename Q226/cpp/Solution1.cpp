#include <iostream>
#include <stack>
using namespace std;

struct TreeNode
{
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x): val(x), left(NULL), right(NULL) {};
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}

class Solution
{
	public:
		TreeNode* invertTree(TreeNode * root);
};

int main()
{
	Solution mysol;

	TreeNode* root = new TreeNode(4);
	root->left = new TreeNode(2);
	root->right = new TreeNode(7);
	root->left->left = new TreeNode(1);
	root->left->right = new TreeNode(3);
	root->right->left = new TreeNode(6);
	root->right->right = new TreeNode(9);

	print_tree( root );

	root = mysol.invertTree( root );

	print_tree( root );

	return 0;
}

TreeNode* Solution::invertTree(TreeNode * root)
{
	if(root == NULL) return root;
	TreeNode* tnode;
	TreeNode* temp;

	stack<TreeNode*> tstack;
	tstack.push(root);

	while(!tstack.empty())
	{
		tnode = tstack.top();
		tstack.pop();

		temp = tnode->left;
		tnode->left = tnode->right;
		tnode->right = temp;

		if(tnode->left != NULL) tstack.push(tnode->left);
		if(tnode->right != NULL) tstack.push(tnode->right);
	}
	return root;
}
