#include <iostream>
#include <vector>
#include <bitset>
using namespace std;

class Solution
{
	public:
		int hammingWeight(uint32_t n);
};

int main()
{
	Solution mysol;
	uint32_t num;
	cout << "Enter num: ";
	cin >> num;
	cout << mysol.hammingWeight(num) << endl;
	return 0;
}

int Solution::hammingWeight(uint32_t n)
{
	int count = 0;
	while(n>0)
	{
		count += n & 1;
		n >>= 1;
	}
	return count;
}
