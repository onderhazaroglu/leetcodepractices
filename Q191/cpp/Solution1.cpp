#include <iostream>
#include <vector>
#include <bitset>
using namespace std;

class Solution
{
	public:
		int hammingWeight(uint32_t n);
};

int main()
{
	Solution mysol;
	uint32_t num;
	cout << "Enter num: ";
	cin >> num;
	cout << mysol.hammingWeight(num) << endl;
	return 0;
}

int Solution::hammingWeight(uint32_t n)
{
	bitset<32> bn(n);
	return bn.count();
}
