#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		int countSegments(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	getline(cin, s);
	cout << mysol.countSegments(s) << endl;
	return 0;
}

int Solution::countSegments(string s)
{
	if(s.empty()) return 0;
	int count = (s[0]==' '?0:1), len = s.length();
	for(int i=1;i<len;i++)
	{
		if(s[i]!=' ' && s[i-1]==' ') count++;
	}
	return count;
}
