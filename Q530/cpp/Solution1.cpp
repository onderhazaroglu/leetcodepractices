#include <iostream>
#include <stack>
#include <vector>
#include <climits>
#include <algorithm>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		int getMinimumDifference(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(5);
	r1->left->right = new TreeNode(8);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	cout << mysol.getMinimumDifference(r1) << endl;
	cout << mysol.getMinimumDifference(r2) << endl;

	return 0;
}

int Solution::getMinimumDifference(TreeNode* root)
{
	int min = INT_MAX;
	if(!root || (!root->left && !root->right)) return min;

	vector<int> v;
	size_t count = 0;
	stack<TreeNode*> s;
	s.push(root);
	v.push_back(root->val);
	count++;

	while(!s.empty())
	{
		TreeNode* t = s.top();
		s.pop();
		if(t->left)
		{
			s.push(t->left);
			v.push_back(t->left->val);
			count++;
		}
		if(t->right)
		{
			s.push(t->right);
			v.push_back(t->right->val);
			count++;
		}
	}
	sort(v.begin(),v.end());
	for(size_t i=1;i<count;i++)
	{
		if(v[i]-v[i-1]<min) min = v[i]-v[i-1];
	}
	return min;
}
