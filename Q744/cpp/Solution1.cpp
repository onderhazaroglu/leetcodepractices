#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		char nextGreatestLetter(vector<char>& letters, char target);
};

int main()
{
	Solution mysol;
	string str = "cdegjkmpry";
	char target = 'l';
	cout << "String: " << str << endl;
	cout << "Enter target: ";
	cin >> target;

	vector<char> letters(str.begin(), str.end());
	cout << letters.size() << endl;
	cout << mysol.nextGreatestLetter(letters,target) << endl;
	return 0;
}

char Solution::nextGreatestLetter(vector<char>& letters, char target)
{
	int s=letters.size();
	int l=0, r=s-1;
	while(l<=r)
	{
		cout << "l: " << l << ", r: " << r;
		int m = (l+r)/2;
		cout << ", m: " << m << endl;
		if(letters[m]>target) r=m-1;
		else l=m+1;
	}
	return l == s ? letters[0] : letters[l];
}
