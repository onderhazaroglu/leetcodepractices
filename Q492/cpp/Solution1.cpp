#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Solution
{
	public:
		vector<int> constructRectangle(int area);
};

int main()
{
	Solution mysol;

	int area;

	cout << "Enter area: ";
	cin >> area;

	vector<int> res = mysol.constructRectangle(area);

	cout << "L = " << res[0] << ", W = " << res[1] << endl;

	return 0;
}

vector<int> Solution::constructRectangle(int area)
{
	int l,w;

	for(w = sqrt(area);w>0;w--)
	{
		if(area % w == 0)
		{
			l = area / w;
			break;
		}
	}

	return vector<int> {l, w};
}
