#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <cstdlib>

using namespace std;

class Solution
{
	public:
		int threeSumClosest(vector<int>& nums, int target);
};


int main()
{
	Solution mysol;

	// vector<int> my_nums {-1, 0, 1, 2, -1, -4, 3,5,-3,1,0,0,7,-7};
	// vector<int> my_nums {-1, 2, 1, -4};
	vector<int> my_nums {1, 1, -1, -1, 3};
	int target;
	cout << "Enter target: ";
	cin >> target;

	cout << "target is " << target << endl;
	cout << "closest sum = " << mysol.threeSumClosest( my_nums, target ) << endl;
	return 0;
}


int Solution::threeSumClosest(vector<int>& nums, int target)
{
	int size = nums.size(), csum = target, mindiff = 2147483647, diff = 0;
	vector<vector<int>> results;

	std::sort(nums.begin(),nums.end());

	for(int i=0;i<size-2;i++)
	{
		if (i>0 && (nums[i]==nums[i-1]) ) continue;
		unsigned int l = i+1, u = size-1;

		while(l < u)
		{
			int sum = nums[i] + nums[l] + nums[u];
			cout << nums[i] << ", " << nums[l] << ", " << nums[u] << ", sum = " << sum << endl;

			if ( sum == target ) return sum;
			else if ( sum < target )
			{
				l++;
				if ( l < u && nums[l]==nums[l-1] ) l++;
				diff = abs( target - sum );
				if ( diff < mindiff ) { mindiff = diff; csum = sum; }
			}
			else
			{
				u--;
				if ( l < u && nums[u]==nums[u+1] ) u--;
				diff = abs( target - sum );
				if ( diff < mindiff ) { mindiff = diff; csum = sum; }
			}
		}

	}

	return csum;
}
