#include <iostream>
#include <string>
#include <set>
using namespace std;

class Solution
{
	public:
		set<string> myset;
		void climb(int n,string s);
		int climbStairs(int n);
};

int main()
{
	Solution mysol;
	int n;
	cout << "Enter n: ";
	cin >> n;
	cout << mysol.climbStairs(n) << endl;
	return 0;
}

void Solution::climb(int n,string s)
{
	if(n==0) myset.insert(s);
	else
	{
		if(n>=2) climb(n-2,s+"2");
		if(n>=1) climb(n-1,s+"1");
	}
}

int Solution::climbStairs(int n)
{
	climb(n,"");
	return myset.size();
}
