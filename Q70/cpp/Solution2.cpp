#include <iostream>
#include <string>
#include <set>
#include <queue>
#include <unordered_map>
using namespace std;

int permutate(int n,int r,int a,int b)
{
	// cout << "P(" << n << "," << r << "," << a << "," << b << ")" << endl;
	r = n - r;
	if(n<r || a+b>n) return 1;

	unordered_map<int,int> top, bottom;
	for(int i=2;i<=n;i++)
	{
		top[i]++;
		if(i<r) top.find(i) != top.end() && top[i]>0?top[i]--:bottom[i]++;
		if(i<=a) top.find(i) != top.end() && top[i]>0?top[i]--:bottom[i]++;
		if(i<=b) top.find(i) != top.end() && top[i]>0?top[i]--:bottom[i]++;
	}

	/*
	cout << "top: ";
	for(auto i: top) cout << i.first << ":" << i.second << ", ";
	cout << endl;
	cout << "bottom: ";
	for(auto i: bottom) cout << i.first << ":" << i.second << ", ";
	cout << endl;
	*/

	long long int num = 1, denom = 1;
	for(auto t: top)
	{
		// cout << "top t: " << t.first << "(" << t.second << ")" << endl;
		while(top[t.first]>0) {num *= t.first; top[t.first]--;}
	}
	for(auto t: bottom)
	{
		// cout << "bottom t: " << t.first << "(" << t.second << ")" << endl; 
		while(bottom[t.first]>0) {denom *= t.first; bottom[t.first]--;}
	}
	// cout << "num: " << num << endl << "denom: " << denom << endl;
	long long int res = num / denom;
	return res;
}

class Solution
{
	public:
		int climbStairs(int n);
};

int main()
{
	cout << permutate(28,28,7,21) << endl;
	cout << permutate(3,3,1,2) << endl;
	Solution mysol;
	int n;
	cout << "Enter n: ";
	cin >> n;
	cout << mysol.climbStairs(n) << endl;
	return 0;
}

int Solution::climbStairs(int n)
{
	int sum = 1;
	for(int i=1;i<=n/2;i++)
	{
		// int j = n-2*i, num = 1, denom = 1;
		// cout << "i: " << i;
		int t = i + n - 2*i;
		sum += permutate(t,t,i,n-2*i);
		// sum += temp;
	}
	return sum;
}
