#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		int climb(int i, int n, int *memo);
		int climbStairs(int n);
};

int main()
{
	Solution mysol;
	int n;
	cout << "Enter n: ";
	cin >> n;
	cout << mysol.climbStairs(n) << endl;
	return 0;
}

int Solution::climb(int i, int n,int *memo)
{
	if(i>n) return 0;
	if(i==n) return 1;
	if(memo[i]>0) {cout << "found in memo[" << i << "]: " << memo[i] << endl; return memo[i]; }
	memo[i] = climb(i+1,n,memo) + climb(i+2,n,memo);
	return memo[i];
}

int Solution::climbStairs(int n)
{
	int * memo = new int[n+1];
	return climb(0,n,memo);
}
