#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool isUgly(int num);
};

int main()
{
	Solution mysol;
	int num;
	cin >> num;
	cout << mysol.isUgly(num) << endl;
	return 0;
}

bool Solution::isUgly(int num)
{
	if(num<=0) return false;
	while(num>1)
	{
		if(num%30==0) num /= 30;
		else if(num%15==0) num /= 15;
		else if(num%6==0) num /= 6;
		else if(num%5==0) num /= 5;
		else if(num%3==0) num /= 3;
		else if(num%2==0) num /= 2;
		else break;
		cout << "num: " << num << endl;
	}
	cout << num << endl;
	return num==1;
}
