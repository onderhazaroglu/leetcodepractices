#include <iostream>
using namespace std;

class Solution
{
	public:
		int getSum(int a, int b);
};

int main()
{
	Solution mysol;
	int x,y;
	cout << "Enter x: ";
	cin >> x;

	cout << "Enter y: ";
	cin >> y;

	cout << mysol.getSum(x, y) << endl;

	return 0;
}

int Solution::getSum(int a, int b)
{
	int sum = a;
	if(a==0) return b;

	while(b != 0)
	{
		sum = a ^ b;
		b = (a & b) << 1;
		a = sum;
	}
	return sum;
}
