#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		bool isNumber(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	getline(cin,s);
	// cin >> s;
	if(mysol.isNumber(s)) cout << s << " is a number." << endl;
	else cout << s << " is NOT a number!" << endl;
	return 0;
}

bool Solution::isNumber(string s)
{
	unsigned int i = 0, j = s.length()-1;
	while(i<j && (s[i] == ' ' || s[j] == ' '))
	{
		if(s[i] == ' ') i++;
		if(s[j] == ' ') j--;
	}
	string ns;
	if(i<=j) ns = s.substr(i,j-i+1);
	else ns = s;

	unsigned len = ns.length();

	int point = 0, exp = 0;

	for(unsigned int i = 0;i<len;i++)
	{
		if(ns[i]>=48 && ns[i]<=57) continue;
		else if(ns[i] == '.')
		{
			if(point==0 && len>1) point++;
			else return false;
		}
		else if(ns[i] == 'e')
		{
			if(i<len-1 && ns[len-1] != '.' && exp==0) exp++;
			else return false;
		}
		else if((ns[i] == '-' || ns[i] == '+')&& i==0) continue;
		else return false;
	}
	return true;
}
