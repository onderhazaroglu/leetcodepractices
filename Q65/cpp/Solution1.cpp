#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		bool isNumber(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	getline(cin,s);
	// cin >> s;
	if(mysol.isNumber(s)) cout << s << " is a number." << endl;
	else cout << s << " is NOT a number!" << endl;
	return 0;
}

bool Solution::isNumber(string s)
{
	unsigned int i = 0;
	for(; s[i] == ' '; i++) {}

	if(s[i] == '+' || s[i] == '-') i++;

	int ndigits = 0, ndots = 0;

	for(ndigits=0, ndots=0; (s[i]>='0' && s[i]<='9') || s[i]=='.'; i++)
		s[i] == '.' ? ndots++:ndigits++;

	cout << ndots << endl << ndigits << endl;

	if(ndots>1 || ndigits<1) return false;

	cout << "A" << endl;

	if(s[i] == 'e')
	{
		i++;
		if(s[i] == '+' || s[i] == '-') i++;

		int ndigits = 0;
		for(; s[i]>='0' && s[i]<='9'; i++, ndigits++) {}

		if(ndigits<1) return false;
	}

	cout << "END" << endl;
	return s[i] == 0;
}
