#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<vector<int>> imageSmoother(vector<vector<int>>& M);
};

int main()
{
	Solution mysol;
	vector<vector<int>> M = { {1,1,1},
														{1,0,1},
														{1,1,1} };

	for(auto i: mysol.imageSmoother(M))
	{
		for(auto j: i) cout << j << "\t";
		cout << endl;
	}
	return 0;
}

vector<vector<int>> Solution::imageSmoother(vector<vector<int>>& M)
{
	int nrows = M.size();
	if(nrows==0) return M;
	int ncols = M[0].size();

	vector<vector<int>> res(M);
	for(int i=0;i<nrows;i++)
	{
		cout << "A" << i << endl;
		for(int j=0;j<ncols;j++)
		{
			cout << "B" << j << endl;
			int sum = M[i][j], cc = 1;
			if(i-1>=0 && j-1>=0) { sum+=M[i-1][j-1]; cc++; }
			if(i-1>=0) { sum+=M[i-1][j]; cc++; }
			if(i-1>=0 && j+1<ncols) { sum+=M[i-1][j+1]; cc++; }
			if(j-1>=0) { sum+=M[i][j-1]; cc++; }
			if(j+1<ncols) { sum+=M[i][j+1]; cc++; }
			if(j-1>=0 && i+1<nrows) { sum+=M[i+1][j-1]; cc++; }
			if(i+1<nrows) { sum+=M[i+1][j]; cc++; }
			if(i+1<nrows && j+1<ncols) { sum+=M[i+1][j+1]; cc++; }
			res[i][j] = sum/cc;
		}
	}
	return res;
}
