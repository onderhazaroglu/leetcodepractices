#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		vector<int> findErrorNums(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,5,3,4,5};
	for(int i: mysol.findErrorNums(nums)) cout << i << endl;
	return 0;
}

vector<int> Solution::findErrorNums(vector<int>& nums)
{
	unordered_map<int,int> map;
	int size = nums.size();
	int tsum = size * (size+1) /2;
	int sum = tsum, twice = 0;

	for(int i=0;i<size;i++)
	{
		if(map[nums[i]]>0) twice = nums[i];
		else map[nums[i]]++;
		sum -= nums[i];
	}
	return vector<int> {twice,sum+twice};
}
