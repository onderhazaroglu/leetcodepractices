#include <iostream>
#include <stack>
#include <queue>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		vector<vector<int>> levelOrderBottom(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	vector<vector<int>> res = mysol.levelOrderBottom(r2);
	for(auto v: res)
	{
		for(auto i: v) cout << i << ", ";
		cout << endl;
	}

	return 0;
}

vector<vector<int>> Solution::levelOrderBottom(TreeNode* root)
{
	vector<vector<int>> res;
	if(!root) return res;
	queue<TreeNode*> q;
	q.push(root);

	while(!q.empty())
	{
		vector<int> lev;
		int s = q.size();
		for(int i=0;i<s;i++)
		{
			TreeNode* t = q.front();
			q.pop();
			if(t->left) q.push(t->left);
			if(t->right) q.push(t->right);
			lev.push_back(t->val);
		}
		res.push_back(lev);
	}
	return vector<vector<int>> (res.rbegin(),res.rend());
}
