#include <iostream>
#include <vector>
#include <string>
#include <bitset>
using namespace std;

class Solution
{
	public:
		vector<string> readBinaryWatch(int num);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter num: ";
	cin >> num;
	for(auto s: mysol.readBinaryWatch(num)) cout << s << endl;
	return 0;
}

vector<string> Solution::readBinaryWatch(int num)
{
	vector<string> res;
	for(int h=0;h<12;h++)
	{
		for(int m=0;m<60;m++)
		{
			if((int)bitset<10>(h<<6 | m).count() == num)
			{
				res.push_back(to_string(h)+ (m<10?":0":":")+to_string(m));
			}
		}
	}
	return res;
}
