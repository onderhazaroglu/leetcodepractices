#include <iostream>
#include <string>
#include <stack>
#include <queue>
#include <unordered_map>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		string tree2str(TreeNode* t);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(5);
	r1->left = new TreeNode(3);
	r1->left->left = new TreeNode(2);
	r1->left->right = new TreeNode(4);
	r1->right = new TreeNode(6);
	r1->right->right = new TreeNode(7);

	print_tree(r1);

	cout << mysol.tree2str(r1) << endl;
	return 0;
}

string Solution::tree2str(TreeNode* t)
{
	if(t == NULL) return "";

	string res = to_string(t->val);
	string lstr = tree2str(t->left);
	string rstr = tree2str(t->right);
	if(lstr.empty() && !rstr.empty()) res += string("()")+"("+rstr+")";
	else 
	{
		if(!lstr.empty()) res += "("+lstr+")";
		if(!rstr.empty()) res += "("+rstr+")";
	}
	return res;
}
