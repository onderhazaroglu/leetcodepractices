#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		bool isEqualTree(TreeNode* s, TreeNode* t);
		bool isSymEqualTree(TreeNode* s, TreeNode* t);
		bool isSymmetric(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->right = new TreeNode(2);
	r1->left->left = new TreeNode(3);
	r1->left->right = new TreeNode(4);
	r1->right->left = new TreeNode(3);
	r1->right->right = new TreeNode(4);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	TreeNode* r3 = new TreeNode(1);
	r3->left = new TreeNode(2);
	r3->right = new TreeNode(2);
	r3->left->left = new TreeNode(3);
	r3->left->right = new TreeNode(4);
	r3->right->left = new TreeNode(4);
	r3->right->right = new TreeNode(3);

	cout << mysol.isEqualTree(r3->left,r3->right) << endl;
	cout << mysol.isSymEqualTree(r3->left,r3->right) << endl;

	cout << mysol.isSymmetric(r1) << endl;
	cout << mysol.isSymmetric(r2) << endl;
	return 0;
}

bool Solution::isEqualTree(TreeNode* s, TreeNode* t)
{
	if(!s && !t) return true;
	else if(s && t && s->val == t->val) return isEqualTree(s->left,t->left) && isEqualTree(s->right,t->right);
	else return false;
}

bool Solution::isSymEqualTree(TreeNode* s, TreeNode* t)
{
	if(!s && !t) return true;
	else if(s && t && s->val == t->val) return isSymEqualTree(s->left,t->right) && isSymEqualTree(s->right,t->left);
	else return false;
}

bool Solution::isSymmetric(TreeNode* root)
{
	return isEqualTree(root->left,root->right);
}
