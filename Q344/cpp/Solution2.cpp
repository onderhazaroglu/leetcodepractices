#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		string reverseString(string s);
};

int main()
{
	Solution mysol;
	string str;
	cout << "Enter a string: ";
	cin >> str;
	cout << mysol.reverseString(str) << endl;
	return 0;
}

string Solution::reverseString(string s)
{
	int size = s.length();

	for(int i=0;i<size/2;i++)
	{
		swap( s[i], s[size-1-i] );
	}
	return s;
}
