#include <iostream>
#include <unordered_map>
using namespace std;

struct mapcmp
{
	bool operator() (const ListNode* nl, const ListNode* nr) const
	{
		return nl->val > nr->val;
	}
}

struct ListNode
{
	int val;
	ListNode* next;
	ListNode(int x): val(x), next(NULL) {}
};

int main()
{
	unordered_map<ListNode*,int,mapcmp> map;
	ListNode* a = new ListNode(5);
	ListNode* b = new ListNode(2);
	ListNode* c = new ListNode(3);
	ListNode* d = new ListNode(1);
	ListNode* e = new ListNode(4);
	ListNode* f = new ListNode(2);
	ListNode* g = new ListNode(5);

	map[a]++;
	map[b]++;
	map[c]++;
	map[d]++;
	map[e]++;
	map[f]++;
	map[g]++;

	for(auto p: map)
	{
		cout << p.first << ": " << p.second << endl;
	}
	return 0;
}
