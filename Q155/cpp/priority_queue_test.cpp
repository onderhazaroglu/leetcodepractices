#include <iostream>
#include <vector>
#include <queue>
using namespace std;

struct cmpless
{
	bool operator() (const int& l, const int& r) const
	{
		return l>=r;
	}
};

int main()
{
	priority_queue<int,vector<int>,cmpless> pq;
	pq.push(5);
	pq.push(1);
	pq.push(3);
	pq.push(7);
	pq.push(4);
	pq.push(2);
	pq.push(1);
	pq.push(2);
	pq.push(7);
	pq.push(5);

	while(!pq.empty())
	{
		cout << pq.top() << endl;
		pq.pop();
	}
	return 0;
}
