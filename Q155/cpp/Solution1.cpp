#include <iostream>
#include <vector>
#include <queue>
using namespace std;

struct ListNode
{
	int val;
	ListNode* next;
	bool deleted;
	ListNode(int x): val(x), next(NULL), deleted(false) {}
};

struct compare
{
	bool operator()(const ListNode* l, const ListNode* r)
	{
		return l->val > r->val;
	}
};

class MinStack
{
	private:
		ListNode* tn;
		priority_queue<ListNode*, vector<ListNode*>, compare> q;
	public:
		MinStack()
		{
			tn = NULL;
		}

		void push(int x)
		{
			ListNode* nn = new ListNode(x);
			q.push(nn);
			nn->next = tn;
			tn = nn;

			cout << "q values: " << endl;

			priority_queue<ListNode*,vector<ListNode*>,compare> nq = q;

			while(!nq.empty())
			{
				cout << nq.top() << ": " << nq.top()->val << endl;
				nq.pop();
			}
			cout << "end of push" << endl;
		}

		void pop()
		{
			cout << "popping: " << (tn?tn->val:0) << endl;
			ListNode* p = tn;
			cout << "p: " << p << ": " << p->val << endl;
			tn = tn->next;
			p->deleted = true;
		}

		int top()
		{
			priority_queue<ListNode*,vector<ListNode*>,compare> nq = q;

			while(!nq.empty())
			{
				cout << nq.top() << ": " << nq.top()->val << endl;
				nq.pop();
			}
			cout << "end of top" << endl;

			return tn?tn->val:0;
		}

		int getMin()
		{
			if(!q.empty() && !q.top()->deleted) { cout << "value exists: " << endl; return q.top()->val; }
			if(q.empty()) return 0;
			cout << "getMin A" << endl;
			if(q.top()->deleted) { cout << "empty..removing"; q.pop(); }
			cout << "getMin B" << endl;
			return getMin();
		}
};

int main()
{
	MinStack ms;
	ms.push(-2);
	ms.push(0);
	ms.push(-3);
	cout << " min: " << ms.getMin() << endl;
	ms.pop();
	cout << " top: " << ms.top() << endl;
	cout << " min: " << ms.getMin() << endl;
	
	return 0;
}
