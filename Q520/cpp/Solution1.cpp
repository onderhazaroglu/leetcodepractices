#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		bool detectCaputalUse(string word);
};

int main()
{
	Solution mysol;
	string word = "";
	cout << "Enter word: ";
	cin >> word;
	if(mysol.detectCaputalUse(word)) cout << "Correct." << endl;
	else cout << "Not correct!" << endl;
	return 0;
}

bool Solution::detectCaputalUse(string word)
{
	// bool res = true;
	bool fcap = word[0] >= 65 && word[0] <= 90;
	bool scap = word[1] >= 65 && word[1] <= 90;
	if(scap && !fcap) return false;

	for(unsigned int i=2;i<word.length();i++)
	{
			if(word[i] >= 65 && word[i] <= 90)
			{
				if(!fcap || !scap) return false;
			}
			else if(scap) return false;
	}
	return true;
}
