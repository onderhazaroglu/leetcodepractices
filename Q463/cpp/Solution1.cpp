#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int islandPerimeter(vector<vector<int>>& grid);
};

int main()
{
	Solution mysol;

	// vector<vector<int>> grid = {vector<int> {0,1,0,0,1,1}, vector<int> {1,1,1,0,1,1}, vector<int> {0,1,0,0,1,1}, vector<int> {1,1,0,0,1,1} };
	// vector<vector<int>> grid = {vector<int> {0,1,0,0}, vector<int> {1,1,1,0}, vector<int> {0,1,0,0}, vector<int> {1,1,0,0} };
	vector<vector<int>> grid = {vector<int> {0,1,0,1}, vector<int> {1,1,1,0}, vector<int> {0,1,0,1}, vector<int> {0,0,0,0} };

	cout << mysol.islandPerimeter( grid ) << endl;
	cout << endl;
	return 0;
}

int Solution::islandPerimeter(vector<vector<int>>& grid)
{
	int w = grid[0].size(), h = grid.size(), per = 0;
	cout << "width: " << w << ", height: " << h << endl;

	for(int i=0;i<h;i++)
	{
		for(int j=0;j<w;j++)
		{
			cout << "coords: " << i << "x" << j << endl;
			if ( grid[i][j] == 0 ) continue;
			else {cout << "Adding 4." << endl; per += 4; }

			if( j-1 >= 0 && grid[i][j-1] == 1 ) { cout << "Removing 2 for left index." << endl; per -= 2; }
			if( i-1 >= 0 && grid[i-1][j] == 1 ) { cout << "Removing 2 for upper index." << endl; per -= 2; }
		}
	}
	return per;
}
