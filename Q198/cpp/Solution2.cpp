#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		int sub_rob(vector<int>& nums,int i,int s,unordered_map<int,int>& map);
		int rob(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,3,6,4,8,10,0,5,7,8,8,9,0,1};
	cout << mysol.rob(nums) << endl;
	return 0;
}

int Solution::sub_rob(vector<int>& nums,int i,int s,unordered_map<int,int>& map)
{
	if(map.find(i)!=map.end()) return map[i];
	cout << "i:" << i << ",s:" << s << endl;
	if(i>s) { map[i]=0; return 0; }
	else if(i==s) { map[i]=nums[i]; return nums[i]; }
	return map[i] = max( nums[i]+sub_rob(nums,i+2,s,map), (i+1<=s?nums[i+1]+sub_rob(nums,i+3,s,map):0) );
}

int Solution::rob(vector<int>& nums)
{
	int a=0,b=0;
	for(unsigned int i=0;i<nums.size();i++)
	{
		if(i%2==0) a = max(a+nums[i], b);
		else b = max(a, b+nums[i]);
	}
	return max(a,b);
}
