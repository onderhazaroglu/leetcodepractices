#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		string longestCommonPrefix(vector<string>& strs);
};

int main()
{
	Solution mysol;
	vector<string> strs = { "abcdef", "abcd", "abcde", "abc" };

	cout << "LCP: " << mysol.longestCommonPrefix( strs ) << endl;
	return 0;
}

string Solution::longestCommonPrefix(vector<string>& strs)
{
	unsigned int size = strs.size(), min = -1, cur;
	string l = "";

	if ( size == 0 ) return l;

	for(unsigned int i=0;i<size;i++)
	{
		cur = strs[i].length();
		if ( cur < min ) min = cur;
	}

	cout << "min: " << min << endl;

	bool diff = false;

	for(unsigned int i=0;i<min;i++)
	{
		unsigned j = 0;
		while ( !diff && j + 1 < size )
		{
			if ( strs[j][i] != strs[j+1][i] ) diff = true;
			j++;
		}
		if ( diff ) break;
		else
		{
			l += strs[0][i];
		}
	}
	return l;
}
