#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		bool isSubtree(TreeNode* s, TreeNode* t);
		bool isEqualTree(TreeNode* s, TreeNode* t);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(3);
	r1->left = new TreeNode(4);
	r1->left->left = new TreeNode(1);
	r1->left->right = new TreeNode(2);
	r1->right = new TreeNode(5);

	TreeNode* r2 = new TreeNode(4);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(2);

	TreeNode* r3 = new TreeNode(3);
	r3->left = new TreeNode(4);
	r3->left->left = new TreeNode(1);
	r3->left->right = new TreeNode(2);
	r3->left->right->left = new TreeNode(0);
	r3->right = new TreeNode(5);

	TreeNode* r4 = new TreeNode(4);
	r4->left = new TreeNode(1);
	r4->right = new TreeNode(2);


	cout << mysol.isSubtree(r2,r1) << endl;
	cout << mysol.isSubtree(r4,r3) << endl;
	return 0;
}

bool Solution::isEqualTree(TreeNode* s, TreeNode* t)
{
	if(s && t) cout << "s: " << s->val << ", t: " << t->val << endl;
	if(!s && !t) return true;
	else if(s && t && s->val == t->val) return isEqualTree(s->left,t->left) && isEqualTree(s->right,t->right);
	else return false;
}

bool Solution::isSubtree(TreeNode* s, TreeNode* t)
{
	stack<TreeNode*> st;
	st.push(t);
	while(!st.empty())
	{
		TreeNode* tn = st.top();
		if(tn) cout << "Looking " << tn->val << endl;
		if(isEqualTree(s,tn)) return true;
		st.pop();
		if(tn->left) st.push(tn->left);
		if(tn->right) st.push(tn->right);
		cout << "size: " << st.size() << endl;
	}
	return false;
}
