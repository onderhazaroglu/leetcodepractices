#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		bool checkRecord(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	if(mysol.checkRecord(s)) cout << "Rewarded." << endl;
	else cout << "Not Rewarded!" << endl;
	return 0;
}

bool Solution::checkRecord(string s)
{
	int size = s.length(), acount = 0, lcount = 0;
	for(int i=0;i<size;i++)
	{
		if(s[i]=='A') acount++;
		else if(s[i]=='L') lcount++;

		if(s[i]!='L') lcount = 0;

		cout << "ac: " << acount << endl << "lc: " << lcount << endl;

		if(acount>1 || lcount>2) return false;
	}
	return true;
}
