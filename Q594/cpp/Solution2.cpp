#include <iostream>
#include <vector>
#include <unordered_map>
#include <utility>
using namespace std;

class Solution
{
	public:
		int findLHS(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums {1,3,2,2,5,2,3,7};
	cout << mysol.findLHS(nums) << endl;
	return 0;
}

int Solution::findLHS(vector<int>& nums)
{
	unordered_map<int,int> freqs;
	for(int n: nums)
	{
		freqs[n]++;
	}

	// int max = freqs.begin()->second;
	int max = 0;

	for(pair<int,int> p: freqs)
	{
		if(freqs.find(p.first+1) != freqs.end())
		{
			if(p.second + freqs[p.first+1]>max) max = p.second + freqs[p.first+1];
		}

		if(freqs.find(p.first-1) != freqs.end())
		{
			if(p.second + freqs[p.first-1]>max) max = p.second + freqs[p.first-1];
		}
	}
	return max;
}
