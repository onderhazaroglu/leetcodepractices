#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int findLHS(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums {1,3,2,2,5,2,3,7};
	cout << mysol.findLHS(nums) << endl;
	return 0;
}

int Solution::findLHS(vector<int>& nums)
{
	vector<vector<int>> lhs {vector<int> {nums[0]}};
	vector<int> max {nums[0]};
	vector<int> min {nums[0]};

	for(unsigned int i=1;i<nums.size();i++)
	{
		for(unsigned int j=0;j<lhs.size();j++)
		{
			cout << "nums[i]: " << nums[i] << ", lhs[" << j << "]: ";
			for(auto e: lhs[j]) cout << e << ", ";
			cout << endl;
			if( (nums[i]<=max[j] && nums[i]>=min[j]) || (max[j]==min[j] && (nums[i]==max[j]+1 || nums[i]==min[j]-1) ) )
			{
				lhs[j].push_back(nums[i]);
				if(nums[i]>max[j]) max[j] = nums[i];
				else if(nums[i]<min[j]) min[j] = nums[i];
			}
			else
			{
				cout << "adding new seq for: " << nums[i] << endl;
				lhs.push_back( vector<int> {nums[i]} );
				max.push_back( nums[i] );
				min.push_back( nums[i] );
			}
		}
	}
	unsigned int maxsize = 0;
	for(auto v: lhs)
	{
		for(auto e: v)
		{
			cout << e << ", ";
		}
		cout << endl;
		if(v.size()>maxsize) maxsize = v.size();
	}
	return maxsize;
}
