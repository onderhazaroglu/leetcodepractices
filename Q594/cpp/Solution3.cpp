#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>
using namespace std;

struct cmp
{
	bool operator() (int a, int b)
	{
		return a<b;
	}
};

class Solution
{
	public:
		int findLHS(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums {1,3,2,2,5,2,3,7};
	cout << mysol.findLHS(nums) << endl;
	return 0;
}

int Solution::findLHS(vector<int>& nums)
{
	unordered_map<int,int> freqs;
	set<int,cmp> s;
	for(int n: nums)
	{
		s.insert(n);
		freqs[n]++;
	}

	for(auto p: freqs)
	{
		cout << p.first << ": " << p.second << endl;
	}

	for(auto i: s) cout << i << ", ";
	cout << endl;

	int c = 0, prev = 0, max = 0;
	for(int n: s)
	{
		if(c++>0)
		{
			cout << "freqs[" << prev << "]: " << freqs[prev] << ", freqs[" << n << "]: " << freqs[n] << endl;
			if(prev+1==n && freqs[n]+freqs[prev]>max) max = freqs[n]+freqs[prev];
		}
		prev = n;
		cout << "max: " << max << endl;
	}
	return max;
}
