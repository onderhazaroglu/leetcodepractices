#include <iostream>
using namespace std;

class Solution {
	public:
		string convert(string s, int numRows);
};

int main() {
	int n = 5;
	string input = "";
	// string *p = new string[n], input = "";

	cout << "Enter string: ";
	cin >> input;

	cout << "Enter numRows: ";
	cin >> n;

	Solution sol1;
	string result = sol1.convert( input, n );

	/*
	for(int i=0;i<20;i++) {
		k+=t;
		p[k].append(to_string( i ) );

		if(i>0 && i%(n-1) == 0) t = t * (-1);
	}

	string result = "";
	for(int i=0;i<n;i++) result += p[i];
	*/

	cout << result << endl;
	return 0;
}


string Solution::convert(string s, int numRows) {
	int slen = s.length(), k=-1, t=1;
	if(slen<=numRows || numRows == 1) return s;

	string *p = new string[numRows];

	for(int i=0;i<slen;i++) {
		k+=t;
		p[k] += s[i];
		if(i>0 && i%(numRows-1) == 0) t = t * (-1);
	}

	string res = "";
	for(int i=0;i<numRows;i++) res += p[i];

	return res;
}
