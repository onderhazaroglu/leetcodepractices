#include <iostream>
#include <vector>
#include <queue>
using namespace std;

struct ListNode
{
	int val;
	ListNode *next;
	ListNode(int x): val(x), next(NULL) {}
};

struct compare
{
	bool operator()(const ListNode* l, const ListNode* r)
	{
		return l->val > r->val;
	}
};

class Solution
{
	public:
		ListNode* mergeKLists(vector<ListNode*>& lists);
};

void print_list( ListNode* head )
{
	ListNode* temp = head;
	if ( temp == NULL )
	{
		cout << "List doesn't have a node." << endl;
		return;
	}

	do
	{
		cout << temp->val;
		if ( temp->next != NULL )
		{
			cout << "->";
		}
		temp = temp->next;
	}
	while ( temp != NULL );
	cout << endl;
}

void print_list2( ListNode* head )
{
	ListNode* temp = head;
	if ( temp == NULL )
	{
		cout << "List doesn't have a node." << endl;
		return;
	}

	do
	{
		cout << temp->val;
		if ( temp->next != NULL )
		{
			cout << "->";
		}
		temp = temp->next;
	}
	while ( temp != NULL );
	cout << endl;
}


int main()
{
	vector<ListNode*> lists;
	int numlists = 0;

	cout << "Enter # of lists: ";
	cin >> numlists;

	for(int i=0;i<numlists;i++)
	{
		ListNode* tlist = NULL;
		ListNode* temp1 = tlist;

		int val = 0, j=0;

		while( val >= 0 )
		{
			j++;
			cout << "Enter " << j << ". element or -1 to quit: ";
			cin >> val;
			if ( val == -1 ) break;
			else if ( j == 1 )
			{
				tlist = new ListNode(val);
				temp1 = tlist;
			}
			else { temp1->next = new ListNode(val); temp1 = temp1->next; }
		}

		lists.push_back( tlist );
	}


	Solution mysol;
	ListNode* ml;
	
	for(int i=0;i<numlists;i++)
	{
		print_list( lists[i] );
	}

	ml = mysol.mergeKLists( lists );
	cout << "Final:\n";
	print_list( ml );

	return 0;
}


ListNode* Solution::mergeKLists(vector<ListNode*>& lists)
{
	priority_queue<ListNode*, vector<ListNode*>, compare> q;

	for(auto l: lists)
	{
		if (l) q.push(l);
	}

	if (q.empty()) return NULL;

	ListNode* result = q.top();

	q.pop();

	if (result->next) q.push(result->next);
	ListNode* tail = result;
	while(!q.empty())
	{
		tail->next = q.top();
		q.pop();
		tail = tail->next;
		if(tail->next) q.push(tail->next);
	}
	return result;
}
