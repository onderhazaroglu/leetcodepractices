#include <iostream>
#include <vector>
using namespace std;

struct ListNode
{
	int val;
	ListNode *next;
	ListNode(int x): val(x), next(NULL) {}
};

class Solution
{
	public:
		ListNode* swapPairs(ListNode* head);
		ListNode* mergeTwoLists(ListNode* l1, ListNode* l2);
		ListNode* mergeTwoLists2(ListNode* l1, ListNode* l2);
		ListNode* mergeKLists(vector<ListNode*>& lists);
};

void print_list( ListNode* head )
{
	ListNode* temp = head;
	if ( temp == NULL )
	{
		cout << "List doesn't have a node." << endl;
		return;
	}

	do
	{
		cout << temp->val;
		if ( temp->next != NULL )
		{
			cout << "->";
		}
		temp = temp->next;
	}
	while ( temp != NULL );
	cout << endl;
}

void print_list2( ListNode* head )
{
	ListNode* temp = head;
	if ( temp == NULL )
	{
		cout << "List doesn't have a node." << endl;
		return;
	}

	do
	{
		cout << temp->val;
		if ( temp->next != NULL )
		{
			cout << "->";
		}
		temp = temp->next;
	}
	while ( temp != NULL );
	cout << endl;
}


int main()
{
	vector<ListNode*> lists;
	int numlists = 0;

	cout << "Enter # of lists: ";
	cin >> numlists;

	for(int i=0;i<numlists;i++)
	{
		ListNode* tlist = NULL;
		ListNode* temp1 = tlist;

		int val = 0, j=0;

		while( val >= 0 )
		{
			j++;
			cout << "Enter " << j << ". element or -1 to quit: ";
			cin >> val;
			if ( val == -1 ) break;
			else if ( j == 1 )
			{
				tlist = new ListNode(val);
				temp1 = tlist;
			}
			else { temp1->next = new ListNode(val); temp1 = temp1->next; }
		}

		lists.push_back( tlist );
	}


	Solution mysol;
	ListNode* ml;
	
	for(int i=0;i<numlists;i++)
	{
		print_list( lists[i] );
	}

	ml = mysol.mergeKLists( lists );
	cout << "Final:\n";
	print_list( ml );

	return 0;
}


ListNode* Solution::mergeKLists(vector<ListNode*>& lists)
{
	int size = lists.size();
	if (size == 0) return NULL;
	else if (size == 1) return lists[0];

	ListNode* head = lists[0];

	for(unsigned int i=1;i<lists.size();i++)
	{
		head = mergeTwoLists2( head, lists[i] );
	}
	return head;
}


ListNode* Solution::swapPairs(ListNode* head)
{
	if ( head == NULL || head->next == NULL ) return head;
	ListNode* t = head;
	ListNode* tn = t->next;
	ListNode **k = &head;

	while((t = *k) && (tn = t->next))
	{
		t->next = tn->next;
		tn->next = t;
		*k = tn;
		k = &(t->next);
	}
	return head;
}




ListNode* Solution::mergeTwoLists(ListNode* l1, ListNode* l2)
{
	ListNode* t1 = l1;
	ListNode* t2 = l2;
	int i = 0;
	ListNode* ml = NULL;
	ListNode* head = NULL;


	while( t1 != NULL || t2 != NULL )
	{
		if ( t2 == NULL )
		{
			cout << "t2 is null" << endl;
			if ( ml == NULL ) ml = t1;
			else ml->next = t1;

			if ( i++ == 0 )
			{
				cout << "head is set" << endl;
				head = ml;
			}
			else ml = ml->next;

			break;
		}
		else if ( t1 == NULL )
		{
			cout << "t1 is null" << endl;
			if ( ml == NULL ) ml = t1;
			else ml->next = t2;

			if ( i++ == 0 )
			{
				cout << "head is set" << endl;
				head = ml;
			}
			else ml = ml->next;

			break;
		}
		else
		{
			if ( t1->val <= t2->val )
			{
				cout << "t1(" << t1->val << ") <= t2(" << t2->val << ")" << endl;
				ml == NULL ? ml = new ListNode(t1->val) : ml->next = new ListNode(t1->val);
				t1 = t1->next;
			}
			else
			{
				cout << "t2(" << t2->val << ") < t1(" << t1->val << ")" << endl;
				ml == NULL ? ml = new ListNode(t2->val) : ml->next = new ListNode(t2->val);
				t2 = t2->next;
			}
			if ( i++ == 0 )
			{
				cout << "head is set" << endl;
				head = ml;
			}
			else ml = ml->next;
		}
		cout << "ml --> " << ml->val << endl;
		print_list( head );
	}
	return head;
}

ListNode* Solution::mergeTwoLists2(ListNode* l1, ListNode* l2)
{
	if ( l1 == NULL ) return l2;
	if ( l2 == NULL ) return l1;

	if ( l1->val < l2->val )
	{
		l1->next = mergeTwoLists2( l1->next, l2 );
		return l1;
	}
	else
	{
		l2->next = mergeTwoLists2( l2->next, l1 );
		return l2;
	}
}
