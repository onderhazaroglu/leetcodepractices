#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		int numberOfBoomerangs(vector<pair<int,int>>& points);
};

int main()
{
	Solution mysol;
	vector<pair<int,int>> points = {make_pair(0,0),make_pair(1,0),make_pair(2,0)};
	cout << mysol.numberOfBoomerangs(points) << endl;
	return 0;
}

int Solution::numberOfBoomerangs(vector<pair<int,int>>& points)
{
	int res = 0;
	for(size_t i=0;i<points.size();i++)
	{
		unordered_map<int,int> boom_ends(points.size());

		for(size_t j=0;j<points.size();j++)
		{
			if(i==j) continue;
			int d1 = points[i].first-points[j].first;
			int d2 = points[i].second-points[j].second;
			boom_ends[d1*d1+d2*d2]++;
		}

		for(auto& p: boom_ends) res += p.second * (p.second -1);
	}
	return res;
}
