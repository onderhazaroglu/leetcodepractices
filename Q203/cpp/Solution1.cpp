#include <iostream>
using namespace std;

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(NULL) {}
};

void print_listnode(ListNode* head)
{
	ListNode* temp = head;

	while(temp != NULL)
	{
		cout << temp->val;
		temp = temp->next;
		if(temp != NULL) cout << "->";
		else cout << endl;
	}
}

class Solution
{
	public:
		ListNode* removeElements(ListNode* head,int val);
};

int main()
{
	Solution mysol;
	/*
	int val;
	cout << "Enter node: ";
	cin >> val;

	ListNode* head = new ListNode(val);
	ListNode* temp = head;

	while(val != -1)
	{
		cout << "Enter node: ";
		cin >> val;
		if(val != -1) temp->next = new ListNode(val);
		temp = temp->next;
	}
	int v;
	cout << "Enter val: ";
	cin >> v;
	*/
	ListNode* head = new ListNode(1);
	ListNode* temp = head;
	temp->next = new ListNode(2); temp = temp->next;
	temp->next = new ListNode(6); temp = temp->next;
	temp->next = new ListNode(3); temp = temp->next;
	temp->next = new ListNode(4); temp = temp->next;
	temp->next = new ListNode(5); temp = temp->next;
	temp->next = new ListNode(6); temp = temp->next;
	int v = 6;


	print_listnode(head);
	cout << "Start" << endl;
	print_listnode(mysol.removeElements(head,v));

	return 0;
}

ListNode* Solution::removeElements(ListNode* head,int val)
{
	ListNode* t = head;
	ListNode* p = nullptr;
	while(t)
	{
		if(t) cout << "t: " << t->val << endl;
		if(p) cout << "p: " << p->val << endl;
		if(t->val==val)
		{
			cout << "found: " << t->val << endl;
			if(t==head) { head = head->next; }
			else
			{
				p->next = t->next;
			}
		}
		else p = t;
		t = t->next;
		print_listnode(head);
	}
	return head;
}
