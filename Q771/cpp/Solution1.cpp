#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		int numJewelsInStones(string J, string S);
};

int main()
{
	Solution mysol;
	string J, S;
	cout << "Enter J: ";
	cin >> J;
	cout << "Enter S: ";
	cin >> S;
	cout << mysol.numJewelsInStones(J,S) << endl;
	return 0;
}

int Solution::numJewelsInStones(string J,string S)
{
	int mj[128] = {0}, res = 0;
	for(int i=0;i<int(J.length());i++) mj[int(J[i])]++;

	for(int i=0;i<int(S.length());i++)
	{
		if(mj[int(S[i])]>0) res++;
	}
	return res;
}
