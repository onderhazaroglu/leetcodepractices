#include <iostream>
#include <vector>
#include <queue>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
	public:
		vector<double> averageOfLevels(TreeNode* root);
};

int main()
{
	Solution mysol;

	TreeNode* root = new TreeNode(3);
	root->left = new TreeNode(9);
	root->right = new TreeNode(20);
	root->right->left = new TreeNode(15);
	root->right->right = new TreeNode(7);

	vector<double> avgs = mysol.averageOfLevels(root);
	for(auto i: avgs)
	{
		cout << i << endl;
	}
	return 0;
}

vector<double> Solution::averageOfLevels(TreeNode* root)
{
	vector<double> avgs;
	if(!root) return avgs;
	queue<TreeNode*> q;
	q.push(root);

	while(!q.empty())
	{
		double temp = 0.0;
		int s = q.size();
		for(int i=0;i<s;i++)
		{
			TreeNode* t = q.front();
			q.pop();
			if(t->left) q.push(t->left);
			if(t->right) q.push(t->right);
			temp+=t->val;
		}
		avgs.push_back(temp/s);
	}
	return avgs;
}
