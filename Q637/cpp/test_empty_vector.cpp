#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector<int> v;
	v.push_back( 5 );
	v[1]++;
	v[1]++;
	v.push_back( 28 );
	cout << v[1] << endl;
	cout << v.size() << endl;
	return 0;
}
