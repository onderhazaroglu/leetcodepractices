#include <iostream>
#include <vector>
using namespace std;

int guess(int num);

class Solution
{
	public:
		int guessNumber(int n);
};

int main()
{
	Solution mysol;
	int num;
	cin >> num;
	cout << mysol.guessNumber(num) << endl;
	return 0;
}

int Solution::guessNumber(int n)
{
	int l = 1, r = n, mid = 0;
	while(l<r)
	{
		mid = ((r-l)>>1) + l;
		if(guess(mid)==0) return mid;
		else if(guess(mid)==-1) r = mid-1;
		else l = mid+1;
	}
	return mid;
}
