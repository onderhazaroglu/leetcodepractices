#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		int treeDepth(TreeNode* root);
	 bool isBalanced(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(3);
	r1->left = new TreeNode(9);
	r1->right = new TreeNode(20);
	r1->right->left = new TreeNode(15);
	r1->right->right = new TreeNode(7);

	TreeNode* r2 = new TreeNode(1);
	r2->left = new TreeNode(2);
	r2->right = new TreeNode(2);
	r2->left->right = new TreeNode(3);
	r2->left->left = new TreeNode(3);
	r2->left->left->left = new TreeNode(4);
	r2->left->left->right = new TreeNode(4);

	cout << mysol.treeDepth(r2) << endl;
	cout << mysol.treeDepth(r2->left) << endl;
	cout << mysol.treeDepth(r2->left->left) << endl;

	cout << mysol.isBalanced(r1) << endl;
	cout << mysol.isBalanced(r2) << endl;

	return 0;
}

int Solution::treeDepth(TreeNode* root)
{
	if(!root) return 0;
	return 1 + max( treeDepth(root->left), treeDepth(root->right) );
}

bool Solution::isBalanced(TreeNode* root)
{
	if(!root) return true;
	cout << "node: " << root->val << endl;
	// int tdl = (root->left?treeDepth(root->left):1), tdr = (root->right?treeDepth(root->right):1);
	int tdl = treeDepth(root->left), tdr = treeDepth(root->right);
	cout << "tdl: " << tdl << ", tdr: " << tdr << endl;
	return abs( treeDepth(root->left) - treeDepth(root->right) ) <= 1 && isBalanced(root->right) && isBalanced(root->left);
}
