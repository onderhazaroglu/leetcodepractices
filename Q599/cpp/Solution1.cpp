#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <climits>
using namespace std;

class Solution
{
	public:
		vector<string> findRestaurant(vector<string>& list1,vector<string>& list2);
};

int main()
{
	Solution mysol;
	// vector<string> list1 = {"Shogun", "Tapioca Express", "Burger King", "KFC"};
	// vector<string> list2 = {"KFC", "Shogun", "Burger King"};
	vector<string> list1 = {"Shogun","Tapioca Express","Burger King","KFC"};
	vector<string> list2 = {"KFC","Burger King","Tapioca Express","Shogun"};
	for(auto s: mysol.findRestaurant(list1,list2)) cout << s << endl;
	return 0;
}

vector<string> Solution::findRestaurant(vector<string>& list1,vector<string>& list2)
{
	unordered_map<string,int> list1_ind, list2_ind;
	for(size_t i=0;i<list1.size();i++) list1_ind[list1[i]] = i;
	for(size_t i=0;i<list2.size();i++) list2_ind[list2[i]] = i;

	int min = INT_MAX;
	string minel = "";
	unordered_map<int,vector<string>> res;

	for(auto t: list1_ind)
	{
		if(list2_ind.find(t.first) != list2_ind.end() && list1_ind[t.first]+list2_ind[t.first]<=min)
		{
			min = list1_ind[t.first]+list2_ind[t.first];
			res[min].push_back(t.first);
		}
	}
	return res[min];
}
