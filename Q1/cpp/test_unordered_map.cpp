#include <iostream>
#include <unordered_map>
using namespace std;

int main() {
	unordered_map<int, int> map;
	map[5] = 1;
	map[6] = 2;

	std::cout << map[5] << endl;
	std::cout << map[8] << endl;
	return 0;
}
