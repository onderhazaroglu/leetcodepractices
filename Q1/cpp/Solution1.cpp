#include <iostream>
#include <vector>
using namespace std;

class Solution {
	public:
		vector<int> twoSum(vector<int> &nums, int target);
};



int main() {
	Solution sol1;

	int my_ints[] = {2, 11, 7, 15};

	vector<int> my_nums2, my_nums (&my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) );

	my_nums2 = sol1.twoSum(my_nums, 9);

	for(unsigned int i=0;i<my_nums2.size();i++) {
		std::cout<< my_nums2[i] << endl;
	}
	return 0;
}


vector<int> Solution::twoSum(vector<int> &nums, int target) {
	vector<int> indices;

	for (unsigned int i=0;i<nums.size();i++) {
		for (unsigned int j=nums.size()-1;j>i;j--) {
			if ( nums[i] + nums[j] == target ) {
				indices.push_back(i);
				indices.push_back(j);
				break;
			}
		}
	}

	return indices;
}
