#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution {
	public:
		vector<int> twoSum(vector<int> &nums, int target);
};



int main() {
	Solution sol1;

	// int my_ints[] = {2, 11, 7, 15};
	int my_ints[] = {0,4,3,0};

	vector<int> my_nums2, my_nums (&my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) );

	my_nums2 = sol1.twoSum(my_nums, 0);

	for(unsigned int i=0;i<my_nums2.size();i++) {
		std::cout<< my_nums2[i] << endl;
	}
	return 0;
}


vector<int> Solution::twoSum(vector<int> &nums, int target) {
	vector<int> indices;
	unordered_map <int, int> map;

	for (unsigned int i=0;i<nums.size();i++) {
		std::unordered_map<int,int>::const_iterator got = map.find( target-nums[i] );

		if ( ( got != map.end() && got->second != int(i) ) ) {
			cout << "found" << target-nums[i] << " for " << nums[i] << endl;
			indices.push_back( got->second );
			indices.push_back( i );

			break;
		}
		map[nums[i]] = i;
	}

	return indices;
}
