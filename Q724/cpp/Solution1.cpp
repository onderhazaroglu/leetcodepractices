#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int pivotIndex(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums {1, 7, 3, 6, 5, 6};
	vector<int> nums {1};
	cout << mysol.pivotIndex(nums) << endl;
	return 0;
}

int Solution::pivotIndex(vector<int>& nums)
{
	int sum = accumulate(nums.begin(), nums.end(), 0);
	int lsum = 0, rsum = sum;
	for(int i=0;i<int(nums.size());i++)
	{
		lsum += i-1>=0?nums[i-1]:0;
		rsum -= nums[i];
		cout << "lsum: " << lsum << ", rsum: " << rsum << endl;
		if(lsum==rsum) return i;
	}
	return -1;
}
