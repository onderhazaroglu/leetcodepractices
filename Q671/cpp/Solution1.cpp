#include <iostream>
#include <stack>
#include <climits>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		int findSecondMinimumValue(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	cout << mysol.findSecondMinimumValue(r2) << endl;

	return 0;
}

int Solution::findSecondMinimumValue(TreeNode* root)
{
	if(!root) return -1;
	stack<TreeNode*> s;
	int min = INT_MAX, sec_min = INT_MAX;
	s.push(root);
	while(!s.empty())
	{
		TreeNode* n = s.top();
		if(n->val<min) { sec_min = min; min = n->val; }
		else if(n->val<sec_min && n->val>min) sec_min = n->val;
		s.pop();
		if(n->left) s.push(n->left);
		if(n->right) s.push(n->right);
	}
	return sec_min==INT_MAX?-1:sec_min;
}
