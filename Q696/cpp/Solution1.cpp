#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		int countBinarySubstrings(string s);
};

int main()
{
	Solution mysol;
	string s = "0001101110011";
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.countBinarySubstrings(s) << endl;
	return 0;
}

int Solution::countBinarySubstrings(string s)
{
	int count = 1, prev = -1, cbs = 0;
	int size = s.length();
	if(size<2) return 0;
	for(unsigned int i=1;i<size;i++)
	{
		if(s[i]!=s[i-1])
		{
			if(prev>-1)
			{
				cbs += prev<count ? prev : count;
				prev = count;
				count = 1;
			}
			else
			{
				prev = count;
				count = 1;
			}
		}
		else count++;
	}
	if(prev==-1) return 0;
	cbs += prev<count ? prev : count;
	return cbs;
}
