#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		vector<string> fizzBuzz(int n);
};

int main()
{
	Solution mysol;
	int n;
	cout << "Enter n: ";
	cin >> n;
	vector<string> nums = mysol.fizzBuzz(n);
	for(auto i: nums)
	{
		cout << i << ", ";
	}
	cout << endl;
	return 0;
}


vector<string> Solution::fizzBuzz(int n)
{
	vector<string> res;
	for(int i=1;i<=n;i++)
	{
		if ( i % 15 == 0 ) res.push_back("FizzBuzz");
		else if ( i % 3 == 0 ) res.push_back("Fizz");
		else if ( i % 5 == 0 ) res.push_back("Buzz");
		else res.push_back(to_string(i));
	}
	return res;
}
