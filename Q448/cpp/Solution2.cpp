#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
using namespace std;

class Solution
{
	public:
		vector<int> findDisappearedNumbers(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {4,3,2,7,8,2,3,1};
	// vector<int> nums = {1,1};
	// vector<int> nums = {1,1,2,2};
	// vector<int> nums = {2,2,3,3,5};
	vector<int> nums = {2,2,3,3,5,5};

	for(int i: nums)
	{
		cout << i << ", ";
	}
	cout << endl << endl;

	vector<int> res = mysol.findDisappearedNumbers(nums);

	for(int j: res)
	{
		cout << j << ", ";
	}
	cout << "END" << endl;

	return 0;
}

vector<int> Solution::findDisappearedNumbers(vector<int>& nums)
{
	vector<int> res;
	int size = nums.size();

	for(int i: nums)
	{
		int idx = abs(i)-1;
		nums[idx] = nums[i]>0 ? -nums[i] : nums[i];
	}

	for(int i=0;i<size;i++)
	{
		if(nums[i]>0) res.push_back(i+1);
	}

	return res;
}
