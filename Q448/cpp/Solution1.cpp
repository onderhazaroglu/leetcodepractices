#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		vector<int> findDisappearedNumbers(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {4,3,2,7,8,2,3,1};
	// vector<int> nums = {1,1};
	// vector<int> nums = {1,1,2,2};
	// vector<int> nums = {2,2,3,3,5};
	vector<int> nums = {2,2,3,3,5,5};

	for(auto i: nums)
	{
		cout << i << ", ";
	}
	cout << endl << endl;

	vector<int> res = mysol.findDisappearedNumbers(nums);

	for(auto i: res)
	{
		cout << i << ", ";
	}
	cout << endl;
	return 0;
}

vector<int> Solution::findDisappearedNumbers(vector<int>& nums)
{
	vector<int> res;
	int size = nums.size();

	if (size < 2) return nums;

	std::sort(nums.begin(),nums.end());
	int i = 0, j = 1;

	bool end = false;

	while( !end )
	{
		if (i == size-1 && j == size) end = true;

		cout << "i: " << i << "(" << nums[i] << "), j:" << j << endl;
		if (j < nums[i]) { res.push_back( j); if(j<size) j++; }
		else if ( j == nums[i] )
		{
			do { if(i<size-1) i++; }
			while(i<size-1 && nums[i] == nums[i-1]);

			if(j<size) j++;
		}
		else
		{
			if (i == size-1)
			{
				res.push_back( j );
				if(j<size) j++;
			}
			else
			{
				do { if(i<size-1) i++; }
				while(i<size-1 && nums[i] == nums[i-1]);
			}
		}
	}
	return res;
}
