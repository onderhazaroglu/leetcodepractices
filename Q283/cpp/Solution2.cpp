#include <iostream>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		void moveZeroes(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {0,1,0,3,4,2,1,0,3,4,0};
	vector<int> nums = {0,0,0,1,0};
	mysol.moveZeroes(nums);
	for(int i: nums)
	{
		cout << i << ", ";
	}
	cout << endl;
	return 0;
}

void Solution::moveZeroes(vector<int>& nums)
{
	std::fill( remove(nums.begin(), nums.end(), 0), nums.end(), 0);
}
