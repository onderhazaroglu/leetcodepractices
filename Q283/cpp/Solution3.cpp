#include <iostream>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		void moveZeroes(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {0,1,0,3,4,2,1,0,3,4,0};
	vector<int> nums = {0,0,0,1,0};
	mysol.moveZeroes(nums);
	for(int i: nums)
	{
		cout << i << ", ";
	}
	cout << endl;
	return 0;
}

void Solution::moveZeroes(vector<int>& nums)
{
	int j = nums.size()-1;
	if(j < 1) return;
	int i = 0;
	while(i<j)
	{
		if(nums[i] == 0)
		{
			int k = i+1;
			while(k<=j) { nums[k-1] = nums[k]; k++; }
			nums[j--] = 0;
		}
		if(nums[i] != 0) i++;
	}
}
