#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		string convertToBase7(int num);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter int: ";
	cin >> num;
	cout << mysol.convertToBase7(num) << endl;
	return 0;
}

string Solution::convertToBase7(int num)
{
	if(num==0) return 0;
	string sign = num>=0?"":"-";
	if(num<0) num = -num;
	string res = "";
	int rem;
	while(num>0)
	{
		rem = num % 7;
		res = to_string(rem) + res;
		num = (num-rem)/7;
	}

	return sign+res;
}
