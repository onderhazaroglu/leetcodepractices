#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		void ff(vector<vector<int>>& image, vector<vector<int>>& map,int sr,int sc,int nr,int nc,int color,int newColor);
		vector<vector<int>> floodFill(vector<vector<int>>& image,int sr, int sc, int newColor);
};

int main()
{
	Solution mysol;
	vector<vector<int>> image = {
		vector<int> {0,0,1,0,0,0,0,1,0,0,0,0,0},
		vector<int> {0,0,0,0,0,0,0,1,1,1,0,0,0},
		vector<int> {0,1,1,0,1,0,0,0,0,0,0,0,0},
		vector<int> {0,1,0,0,1,1,0,0,1,0,1,0,0},
		vector<int> {0,1,0,0,1,1,0,0,1,1,1,0,0},
		vector<int> {0,0,0,1,1,0,0,0,0,0,1,0,0},
		vector<int> {0,0,0,0,0,0,0,1,1,1,1,0,0},
		vector<int> {0,0,0,0,0,0,0,1,1,0,0,0,0} };


	// cout << "Maximum area: " << mysol.maxAreaOfIsland(grid) << endl;
	
	for(auto& i: image)
	{
		for(auto j: i)
		{
			cout << j << " ";
		}
		cout << endl;
	}

	for(auto& i: mysol.floodFill(image,2,4,5))
	{
		for(auto j: i)
		{
			cout << j << " ";
		}
		cout << endl;
	}

	return 0;
}


void Solution::ff(vector<vector<int>>& image, vector<vector<int>>& map,int sr,int sc,int nr,int nc,int color,int newColor)
{
	cout << "=== Running fill for (" << sr << "," << sc << ") ===" << endl;
	if(image[sr][sc] == color) image[sr][sc] = newColor;
	map[sr][sc] = 1;

	if(sr>0 && image[sr-1][sc] == color && map[sr-1][sc]<1) ff(image,map,sr-1,sc,nr,nc,color,newColor);
	else cout << "Nope (" << sr-1 << "," << sc << ")" << endl;
	if(sr<nr-1 && image[sr+1][sc] == color && map[sr+1][sc]<1) ff(image,map,sr+1,sc,nr,nc,color,newColor);
	else cout << "Nope (" << sr+1 << "," << sc << ")" << endl;
	if(sc>0 && image[sr][sc-1] == color && map[sr][sc-1]<1) ff(image,map,sr,sc-1,nr,nc,color,newColor);
	else cout << "Nope (" << sr << "," << sc-1 << ")" << endl;
	if(sc<nc-1 && image[sr][sc+1] == color && map[sr][sc+1]<1) ff(image,map,sr,sc+1,nr,nc,color,newColor);
	else cout << "Nope (" << sr << "," << sc+1 << ")" << endl;
}


vector<vector<int>> Solution::floodFill(vector<vector<int>>& image,int sr, int sc, int newColor)
{
	if(image.size()==0 && image[0].size() == 0) return image;
	unsigned int nr = image.size(), nc = image[0].size();
	int color = image[sr][sc];

	vector<vector<int>> map(nr,vector<int>(nc,-1));
	ff(image,map,sr,sc,nr,nc,color,newColor);
	return image;
}
