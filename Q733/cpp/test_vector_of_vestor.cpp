#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector<vector<int>> map(20,vector<int>(10,-1) );
	map[3][5] = 20;
	if(map.size() >= 3 && map[3].size() >= 5) cout << map[3][5] << endl;
	else cout << "Element doesn't exist." << endl;
	return 0;
}
