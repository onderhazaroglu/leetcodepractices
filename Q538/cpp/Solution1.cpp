#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	private:
		int sum = 0;
	public:
		TreeNode* convertBST(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(5);
	r2->left = new TreeNode(3);
	r2->right = new TreeNode(6);
	r2->left->left = new TreeNode(2);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	TreeNode* r3 = mysol.convertBST(r2);
	print_tree(r3);
	cout << r3->val << endl;

	return 0;
}

TreeNode* Solution::convertBST(TreeNode* root)
{
	if(!root) return root;
	convertBST(root->right);
	sum += root->val;
	root->val = sum;
	convertBST(root->left);
	return root;
}
