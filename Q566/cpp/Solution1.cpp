#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<vector<int>> matrixReshape(vector<vector<int>>& nums, int r, int c);
};

int main()
{
	Solution mysol;
	vector<vector<int>> nums = { vector<int> {1,2,3}, vector<int> {4,5,6} };
	// vector<vector<int>> res = mysol.matrixReshape(nums,3,2);

	// for(auto v: res)
	for(auto v: mysol.matrixReshape(nums,3,2))
	{
		for(auto j: v) cout << j << " ";
		cout << endl;
	}
	return 0;
}

vector<vector<int>> Solution::matrixReshape(vector<vector<int>>& nums, int r, int c)
{
	if(static_cast<int>(nums.size() * nums[0].size()) != r * c) return nums;

	vector<int> all;
	vector<vector<int>> res;

	for(auto v: nums) all.insert(all.end(),v.begin(),v.end());
	cout << "all size: " << all.size() << endl;
	for(auto i: all) cout << i << " ";
	cout << endl;

	for(int i=0;i<r;i++)
	{
		cout << "indexes " << i*c << "(" << *(all.begin()+i*c)<< "), " << i*c+c-1 << "(" << *(all.begin()+i*c+c-1)<< ")" << endl;
		res.push_back( vector<int>(all.begin()+i*c,all.begin()+(i*c+c)) );
		cout << "res[" << i << "]:" << endl;
		for(auto tv: res[i]) cout << tv << " ";
		cout << endl;
	}
	cout << res.size() << endl;
	cout << res[0].size() << endl;
	int a;
	cin >> a;
	for(unsigned int i=0;i<res.size();i++)
	{
		for(unsigned int j=0;j<res[i].size();j++)
		{
			cout << res[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
	return res;
}
