#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

class Solution {
	public:
		string longestPalindrome(string s);
};

int main() {

	Solution sol1;
	string input = "abcbade";
	cout << "Enter string: ";
	cin >> input;

	string result = sol1.longestPalindrome( input );

	cout << "Longest palindrome: " << result << endl;

	return 0;
}

string Solution::longestPalindrome(string s) {
	int slen = s.length(), l=0, r=0, maxcindex = -1, maxlen = 0, z=0;
	int alen = 2 * slen;
	unordered_map<int,int> mymap;

	if(slen < 1) return "";

	for(int i=0;i<alen;i++) {
		if( i % 2 == 1 ) {
			l = (i-1)/2-1;
			r = (i-1)/2+1;
			z = 1;
		}
		else {
			l = (i/2)-1;
			r = (i/2);
			z = 0;
		}

		while(l>=0 && r<=slen-1 && s[l--] == s[r++]) {
			z += 2;
		}
		mymap[i] = z;
		if( z > maxlen ) {
			maxlen = z;
			maxcindex = i;
		}
	}


	cout << "max index: " << maxcindex << endl
		<< "max len: " << maxlen << endl;

	if( maxcindex % 2 == 1 ) {
		return s.substr( (maxcindex-1)/2-maxlen/2, maxlen );
	}
	else return s.substr( maxcindex/2-maxlen/2, maxlen );
}
