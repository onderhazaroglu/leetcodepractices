#include <iostream>
#include <string>
using namespace std;

class Solution {
	public:
		bool check_palindrome(string s);
		string longestPalindrome(string s);
};

int main() {

	Solution sol1;
	string input = "abcbade";

	if ( sol1.check_palindrome ( input ) ) {
		cout << "palindrome." << endl;
	}
	else cout << "not palindrome!" << endl;

	string result = sol1.longestPalindrome( input );

	cout << "Longest palindrome: " << result << endl;

	return 0;
}

string Solution::longestPalindrome(string s) {
	int slen = s.length(), i = 0, j = 0;
	string cur_str = "";

	if(slen < 1) return "";

	for(int z=slen;z>0;z--) {
		cout << "z = " << z << endl;
		j = i + z;
		while(i<slen && j<=slen) {
			cout << "i: " << i << ", z: " << z << ", j: " << j << endl;
			cur_str = s.substr(i,z);
			cout << "checking for: " << cur_str << endl;
			if( check_palindrome( cur_str ) ) {
				return cur_str;
			}
			j = ++i + z;		
		}
		i = 0;
	}
	return "";
}

bool Solution::check_palindrome(string s) {
	cout << "checking for str: " << s << endl;
	int slen = s.length();
	if(slen == 1 || ( slen == 2 && s[0] == s[1] ) ) {
		cout << "A" << endl;
		return true;
	}
	else {
		cout << "B" << endl;
		if(s[0] == s[slen-1]) {
			cout << "C" << endl;
			return check_palindrome( s.substr(1, slen-2) );
		}
		else return false;
	}
}
