#include <iostream>
#include <string>
using namespace std;

class Solution {
	public:
		string longestPalindrome(string s);
};

bool check_palindrome(string s);

int main() {

	Solution sol1;

	string input = "";
	cout << "Please enter the text: ";
	cin >> input;

	string result = sol1.longestPalindrome( input );
	cout << "longest palindrome size: " << result << endl;

	return 0;
}

string Solution::longestPalindrome(string s) {
	int slen = s.length(), maxlen = 0;
	string cstr = "", lp = "";

	for(int i=0;i<slen;i++ ) {

		for(int j=i;j<slen;j++) {
			cstr = s.substr(i,j-i+1);
			if( check_palindrome( cstr ) ) {
				cout << "yes, " << cstr << " is a palindrome." << endl;
				if ( j-i+1 > maxlen ) {
					maxlen = j-i+1;
					lp = cstr;
				}
			}
			else {
				cout << "NO, " << cstr << " is NOT a palindrome!" << endl;
			}
		}
	}
	return lp;
}


bool check_palindrome(string s) {
	bool pal = true;
	int size = s.length();
	cout << "Running for text: " << s << ", size: " << size << endl;
	for(int i=0;i<=size/2;i++) {
		if( s[i] != s[size-i-1] ) {
			cout << "\tif string: " << s << ", s[" << i << "]=" << s[i] << " <-> s[" << size-i-1 << "]=" << s[size-i-1] << endl;
			pal = false;
			break;
		}
		else {
			cout << "\t\telse string: " << s << ", s[" << i << "]=" << s[i] << " <-> s[" << size-i-1 << "]=" << s[size-i-1] << endl;
		}
	}
	return pal;
}
