#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

class Solution {
	public:
		string longestPalindrome(string s);
};

int main() {

	Solution sol1;
	string input = "abcbade";
	cout << "Enter string: ";
	cin >> input;

	string result = sol1.longestPalindrome( input );

	cout << "Longest palindrome: " << result << endl;

	return 0;
}

string Solution::longestPalindrome(string s) {

	int slen = s.length(), l=0, r=0, maxcindex = -1, maxlen = 0, z=0;
	int alen = 2 * slen +1, /*pp1 = 0,*/ pp2 = 0;

	string old = s;
	s.resize( alen, '$' );

	for(int i=slen-1;i>=0;i--) {
		s[2*i+1] = old[i];
		s[i] = '$';
		cout << "str: " << s << endl;
	}

	cout << "input: " << s << endl;

	for(int j=0;j<alen;j++) cout << j << "\t";
	cout << endl;

	for(int j=0;j<alen;j++) cout << s[j] << "\t";
	cout << endl;


	unordered_map<int,int> mymap;

	if(slen < 1) return "";

	for(int i=0;i<alen;i++) {
		l = i-1;
		r = i+1;
		z = 1;

		while(l>=0 && r<=alen-1 && s[l--] == s[r++]) {
			cout << "\tc: " << i << ", l: " << l+1 << ", r: " << r-1 << ", z: ";
			z += 2;
			// pp1 = l+1;
			pp2 = r-1;
			cout << z << endl;
			// cout << pp1 << endl;
		}


		mymap[i] = z;

		cout << "ci: " << i << ", size: " << z << endl;

		if( z > maxlen ) {
			maxlen = z;
			maxcindex = i;
		}

		for(int k=i+1;k<pp2;k++) {
			if(mymap[i-k+i]/2 + k < pp2) {
				mymap[k] = mymap[i-k+i];

				if( mymap[k] > maxlen ) {
					maxlen = mymap[k];
					maxcindex = k;
				}

			}
			else break;
		}

	}


	cout << "max index: " << maxcindex << endl
		<< "max len: " << maxlen << endl
		<< "result: " << s.substr( maxcindex-maxlen/2, maxlen ) << endl
		<< "real: " << old.substr( maxcindex/2-maxlen/2/2, maxlen/2) << endl;


	return s.substr( maxcindex-maxlen/2, maxlen );
}
