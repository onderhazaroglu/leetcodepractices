#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<int> getRow(int rowIndex);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter rowIndex: ";
	cin >> num;
	for(int i:mysol.getRow(num)) cout << i << " ";
	cout << endl;
	return 0;
}

vector<int> Solution::getRow(int rowIndex)
{
	vector<vector<int>> r = { vector<int> {1}, vector<int> {1,1}, vector<int> {1,2,1} };

	for(int i=3;i<=rowIndex;i++)
	{
		vector<int> temp(i+1,1);
		for(int j=1;j<=i-1;j++)
		{
			temp[j] = r[i-1][j-1]+r[i-1][j];
		}
		r.push_back( temp );
	}
	return r[rowIndex];
}
