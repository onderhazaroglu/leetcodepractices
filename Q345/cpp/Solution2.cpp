#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		bool isVowel(char c);
		size_t nextVowelBack(string s,size_t i);
		string reverseVowels(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.reverseVowels(s) << endl;
	return 0;
}

bool Solution::isVowel(char c)
{
	unordered_map<char,int> vw { {'a',1}, {'A',1}, {'e',1}, {'E',1}, {'i',1}, {'I',1}, {'o',1}, {'O',1}, {'u',1}, {'U',1} };
	return vw[c]>0;
}

size_t Solution::nextVowelBack(string s,size_t i)
{
	i--;
	while(!isVowel(s[i]) && i>0) i--;
	return i;
}

string Solution::reverseVowels(string s)
{
	size_t l=0, r=s.length();

	while(l<r)
	{
		if(!isVowel(s[l])) l++;
		else
		{
			r = nextVowelBack(s,r);
			cout << "r: " << r << endl;
			if(l<r) { cout << "swapping: " << s[l] << " and " << s[r] << endl; swap(s[l++],s[r]); cout << "new s: " << s << endl; }
			else break;
		}
	}
	return s;
}
