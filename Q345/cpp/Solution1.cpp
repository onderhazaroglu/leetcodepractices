#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		string reverseVowels(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.reverseVowels(s) << endl;
	return 0;
}

string Solution::reverseVowels(string s)
{
	vector<char> l;
	vector<int> v;

	for(size_t i=0;i<s.length();i++)
	{
		if(s[i]=='a' || s[i]=='e' || s[i]=='i' || s[i]=='o' || s[i]=='u')
		{
			l.push_back(s[i]);
			v.push_back(i);
		}
	}
	vector<char> rev(l.rbegin(),l.rend());
	for(size_t i=0;i<rev.size();i++)
	{
		s[v[i]] = rev[i];
	}
	return s;
}
