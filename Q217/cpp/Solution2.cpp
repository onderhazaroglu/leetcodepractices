#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool containsDuplicate(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {1,3,4,2,1,3,4};
	vector<int> nums = {-1200000005,-1200000005};
	if(mysol.containsDuplicate(nums)) cout << "contains duplicate." << endl;
	else cout << "does NOT contain duplicate." << endl;
	return 0;
}

bool Solution::containsDuplicate(vector<int>& nums)
{
	vector<int> v(nums.size(),0);
	for(auto i: nums)
	{
		if(v[i]>0) return true;
		else v[i]++;
	}
	return false;
}
