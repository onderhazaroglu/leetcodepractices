#include <iostream>
#include <vector>
#include <set>
using namespace std;

class Solution
{
	public:
		bool containsDuplicate(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,3,4,2,1,3,4};
	if(mysol.containsDuplicate(nums)) cout << "contains duplicate." << endl;
	else cout << "does NOT contain duplicate." << endl;
	return 0;
}

bool Solution::containsDuplicate(vector<int>& nums)
{
	set<int> s;
	for(auto i: nums)
	{
		if(s.find(i) != s.end()) return true;
		else s.insert(i);
	}
	return false;
}
