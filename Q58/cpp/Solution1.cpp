#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		int lengthOfLastWord(string s);
};

int main()
{
	Solution mysol;
	string s;
	cin.ignore();
	getline(cin,s);
	cout << mysol.lengthOfLastWord(s) << endl;
	return 0;
}

int Solution::lengthOfLastWord(string s)
{
	if(s.empty()) return 0;
	int slen = s.length();
	int i = slen-1;
	while(s[i]==' ' && i>=0) i--;

	int len = 0;
	if(i>=0)
	{
		for(int j=i;j>=0;j--)
		{
			if(s[j]==' ') return len;
			else len++;
		}
	}
	return len;
}
