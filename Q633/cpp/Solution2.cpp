#include <iostream>
#include <vector>
#include <unordered_map>
#include <cmath>
using namespace std;

class Solution
{
	public:
		bool judgeSquareSub(int c);
};

int main()
{
	Solution mysol;
	int num;
	cin >> num;
	cout << mysol.judgeSquareSub(num) << endl;
	return 0;
}

bool Solution::judgeSquareSub(int c)
{
	int i=0,j=sqrt(c);

	while(i<=j)
	{
		int sum = i*i + j*j;
		if(sum<c) i++;
		else if(sum>c) j--;
		else return true;
	}
	return false;
}
