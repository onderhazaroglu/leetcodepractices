#include <iostream>
#include <vector>
#include <unordered_map>
#include <cmath>
using namespace std;

class Solution
{
	public:
		bool judgeSquareSub(int c);
};

int main()
{
	Solution mysol;
	int num;
	cin >> num;
	cout << mysol.judgeSquareSub(num) << endl;
	return 0;
}

bool Solution::judgeSquareSub(int c)
{
	unordered_map<int,int> map;
	for(int i=0;i<=int(sqrt(c));i++)
	{
		map[i*i]=c-(i*i);
		// for(auto p: map) cout << p.first << ": " << p.second << endl;
		// cout << "----" << endl;

		if(map.count(c-(i*i))>0 && map[c-(i*i)]==i*i) 
		{
			cout << i*i << " <> " << c-i*i << endl;
			return true;
		}
	}
	return false;
}
