#include <iostream>
using namespace std;

struct TreeNode
{
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(): left(NULL), right(NULL) {}
	TreeNode(int x): val(x), left(NULL), right(NULL) {}
};

class Solution
{
	public:
		int maxDepth(TreeNode* root);
};

int main()
{
	Solution mysol;

	TreeNode* root = new TreeNode(1);
	root->left = new TreeNode(2);
	root->right = new TreeNode(3);
	root->right->right = new TreeNode(9);
	root->left->left = new TreeNode(4);
	root->left->right = new TreeNode(5);
	root->left->right->left = new TreeNode(7);

	cout << mysol.maxDepth( root ) << endl;

	TreeNode* root2 = new TreeNode(1);
	root2->left = new TreeNode(2);
	root2->right = new TreeNode(3);

	cout << mysol.maxDepth( root2 ) << endl;

	TreeNode* root3 = new TreeNode(1);

	cout << mysol.maxDepth( root3 ) << endl;

	TreeNode* root4 = NULL;

	cout << mysol.maxDepth( root4 ) << endl;

	return 0;
}

int Solution::maxDepth(TreeNode* root)
{
	if(root == NULL) return 0;
	else return 1+max(maxDepth(root->left),maxDepth(root->right));
}
