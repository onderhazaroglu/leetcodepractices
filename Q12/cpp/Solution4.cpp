#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		string intToRoman(int num);
};

int main()
{
	int num;
	Solution my_sol;

	cout << "Enter integer (1-3999): ";
	cin >> num;

	cout << my_sol.intToRoman( num ) << endl;

	return 0;
}

string Solution::intToRoman(int num)
{
	string M[] = { "", "M", "MM", "MMM" };
	string C[] = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
	string X[] = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
	string I[] = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };

	return M[num/1000].append( C[(num%1000)/100].append( X[(num%100)/10].append( I[num%10] ) ) );
}
