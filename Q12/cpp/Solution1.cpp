#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		string intToRoman(int num);
};

int main()
{
	int num;
	Solution my_sol;

	cout << "Enter integer (1-3999): ";
	cin >> num;

	cout << my_sol.intToRoman( num ) << endl;

	return 0;
}

string Solution::intToRoman(int num)
{
	string result = "";

	while ( num > 0 )
	{
		if ( num >= 1000 )
		{
			for(int i=0;i<num/1000;i++) result += "M";
			num %= 1000;
		}
		else if ( num >= 900 )
		{
			result += "CM";
			num %= 900;
		}
		else if ( num >= 500 )
		{
			result += "D";
			num %= 500;
		}
		else if ( num >= 400 )
		{
			result += "CD";
			num %= 400;
		}
		else if ( num >= 100 )
		{
			for(int i=0;i<num/100;i++) result += "C";
			num %= 100;
		}
		else if ( num >= 90 )
		{
			result += "XC";
			num %= 90;
		}
		else if ( num >= 50 )
		{
			result += "L";
			num %= 50;
		}
		else if ( num >= 40 )
		{
			result += "XL";
			num %= 40;
		}
		else if ( num >=10 )
		{
			for(int i=0;i<num/10;i++) result += "X";
			num %= 10;
		}
		else if ( num == 9 )
		{
			result += "IX";
			num %= 9;
		}
		else if ( num >= 5 )
		{
			result += "V";
			num %= 5;
		}
		else if ( num == 4 )
		{
			result += "IV";
			num %= 4;
		}
		else
		{
			for(int i=0;i<num;i++) result += "I";
			num %= 1;
		}
	}

	return result;
}
