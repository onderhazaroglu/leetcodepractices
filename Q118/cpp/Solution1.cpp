#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<vector<int>> generate(int numRows);
};

int main()
{
	Solution mysol;
	int numRows;
	cout << "Enter numRows: ";
	cin >> numRows;
	for(auto v: mysol.generate(numRows))
	{
		for(auto i: v) cout << i << ", ";
		cout << endl;
	}
	return 0;
}

vector<vector<int>> Solution::generate(int numRows)
{
	if(numRows==0) return vector<vector<int>>();
	else if(numRows==1) return vector<vector<int>> { vector<int> {1} };
	else if(numRows==2) return vector<vector<int>> { vector<int> {1}, vector<int> {1,1} };

	vector<vector<int>> res {vector<int> {1}, vector<int> {1,1} };

	for(int i=3;i<=numRows;i++)
	{
		vector<int> temp(i,0);
		vector<int> last = res.back();

		temp[0] = temp[i-1] = 1;
		for(int j=1;j<i-1;j++)
		{
			temp[j] = last[j-1]+last[j];
		}
		res.push_back( temp );
	}
	return res;
}
