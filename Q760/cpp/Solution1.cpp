#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<int> anagramMappings(vector<int>& A, vector<int>& B);
};

int main()
{
	Solution mysol;
	vector<int> A {12,28,46,32,50};
	vector<int> B {50,12,32,46,28};
	for(int i: mysol.anagramMappings(A,B))
	{
		cout << i << endl;
	}
	return 0;
}

vector<int> Solution::anagramMappings(vector<int>& A, vector<int>& B)
{
	unordered_map<int,int> m;
	for(unsigned int i=0;i<B.size();i++)
	{
		m[B[i]] = i;
	}

	vector<int> res;
	for(unsigned int i=0;i<A.size();i++)
	{
		res.push_back( m[A[i]] );
	}
	return res;
}
