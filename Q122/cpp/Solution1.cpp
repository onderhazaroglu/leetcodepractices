#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int maxProfit(vector<int>& prices);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,3,4,2,1,3,4};
	cout << mysol.maxProfit(nums) << endl;
	return 0;
}

int Solution::maxProfit(vector<int>& prices)
{
	int profit = 0;
	size_t psize = prices.size();
	if(psize==0) return 0;
	for(size_t i = 0;i<psize-1;i++)
	{
		profit += max(prices[i+1]-prices[i],0);
	}
	return profit;
}
