#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int minMoves(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {1,3,4,2,1,3,4};
	// vector<int> nums = {1,2,3};
	vector<int> nums = {1,5,8,20};
	cout << mysol.minMoves(nums) << endl;
	return 0;
}

int Solution::minMoves(vector<int>& nums)
{
	return accumulate(nums.begin(), nums.end(), 0L) - nums.size() * *min_element(nums.begin(), nums.end());
}
