#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
using namespace std;

class Solution
{
	public:
		int minMoves(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {1,3,4,2,1,3,4};
	// vector<int> nums = {1,2,3};
	vector<int> nums = {1,5,8,20};
	cout << mysol.minMoves(nums) << endl;
	return 0;
}

int Solution::minMoves(vector<int>& nums)
{
	int min = INT_MAX;
	for(unsigned int i=0;i<nums.size();i++)
	{
		if(nums[i]<min) min = nums[i];
	}
	int res = 0;
	for(unsigned int i=0;i<nums.size();i++)
	{
		res += nums[i]-min;
	}
	return res;
}
