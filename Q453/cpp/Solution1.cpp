#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int minMoves(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {1,3,4,2,1,3,4};
	// vector<int> nums = {1,2,3};
	vector<int> nums = {1,5,8,20};
	cout << mysol.minMoves(nums) << endl;
	return 0;
}

int Solution::minMoves(vector<int>& nums)
{
	int A = 0;
	int n = nums.size();
	for(int i=0;i<n;i++)
	{
		A += nums[i];
	}
	sort(nums.begin(), nums.end());
	int m = max(n,nums[n-1]);

	while(true)
	{
		cout << "m: " << m << endl;
		if((n*m-A) % (n-1) == 0 && (n*m - A) / (n-1) >= 1) break;
		else m++;
	}
	return (n*m-A)/(n-1);
}
