#include <iostream>
using namespace std;

class Solution
{
	public:
		bool isPalindrome(int x);
};

int main()
{
	Solution sol1;
	int num;

	cout << "Enter integer: ";
	cin >> num;

	if ( sol1.isPalindrome(num) ) cout << num << " is palindrome." << endl;
	else cout << num << " is NOT palindrome!" << endl;

	return 0;
}

bool Solution::isPalindrome(int x)
{
	if ( x < 0 ) return false;
	else if ( x < 10 ) return true;

	int y = x, z = 0, k = 0;

	/*
	k = y % 10;
	z = z * 10 + k;
	y = ( y - k ) / 10;

	cout << "initial\n" << "k=" << k << endl << "z=" << z << endl << "y=" << y << endl << endl;
	*/

	while(y>=10)
	{
		k = y%10;

		if ( z <= 214748364 ) z = z * 10 + k;
		else return false;

		y = ( y - k ) / 10;
		if ( y == z ) 
		{
			cout << "found equal!" << endl;
			return true;
		}

		cout << "k=" << k << endl << "z=" << z << endl << "y=" << y << endl << endl;
	};

	z = z * 10 + y;
	cout << "last z=" << z << endl;

	return z == x;
}
