#include <iostream>
using namespace std;

class Solution
{
	public:
		bool isPalindrome(int x);
};

int main()
{
	Solution sol1;
	int num;

	cout << "Enter integer: ";
	cin >> num;

	if ( sol1.isPalindrome(num) ) cout << num << " is palindrome." << endl;
	else cout << num << " is NOT palindrome!" << endl;

	return 0;
}

bool Solution::isPalindrome(int x)
{
	if ( x < 0 ) return false;

	int y = x, z = 0;

	while(y>=10)
	{
		cout << "y=" << y << endl;
		if ( z <= 214748364 ) z = z * 10 + (y%10);
		else return false;

		y = ( y- ( y%10 ) ) / 10;
		cout << "z=" << z << endl;
	};

	z = z * 10 + y;

	return z == x;
}
