#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		bool judgeCircle(string moves);
};

int main()
{
	Solution mysol;
	string moves = "UD";
	cout << "Enter moves: ";
	cin >> moves;

	if (mysol.judgeCircle(moves))
	{
		cout << "true" << endl;
	}
	else cout << "false" << endl;

	return 0;
}

bool Solution::judgeCircle(string moves)
{
	int v = 0, h = 0;
	for(char c: moves)
	{
		switch(c)
		{
			case 'R':
				h++;
				break;
			case 'L':
				h--;
				break;
			case 'U':
				v++;
				break;
			case 'D':
				v--;
				break;
		}
	}
	return v == 0 && h == 0;
}
