#include <iostream>
#include <string>
#include <unordered_map>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		bool isAnagram(string s, string t);
};

int main()
{
	Solution mysol;
	string s, t;
	cout << "Enter string 1(s): ";
	cin >> s;
	cout << "Enter string 2(t): ";
	cin >> t;
	if(mysol.isAnagram(s,t)) cout << s << " and " << t << " are anagram." << endl;
	else cout << s << " and " << t << " are NOT anagram!" << endl;
	return 0;
}

bool Solution::isAnagram(string s, string t)
{
	sort(s.begin(),s.end());
	sort(t.begin(),t.end());
	return s == t;
	/*
	unordered_map<char,int> ms;
	unordered_map<char,int> mt;
	for(auto c: s) ms[c]++;
	for(auto c: t) mt[c]++;
	*/
}
