#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		int firstUniqChar(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.firstUniqChar(s) << endl;
	return 0;
}

int Solution::firstUniqChar(string s)
{
	vector<int> count(26,0);
	for(char c: s) count[c-'a']++;
	for(size_t i=0;i<s.length();i++)
	{
		if(count[s[i]-'a']==1) return i;
	}
	return -1;
}
