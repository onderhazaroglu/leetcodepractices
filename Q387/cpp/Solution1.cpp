#include <iostream>
#include <unordered_map>
#include <string>
using namespace std;

class Solution
{
	public:
		int firstUniqChar(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.firstUniqChar(s) << endl;
	return 0;
}

int Solution::firstUniqChar(string s)
{
	unordered_map<char,int> map;
	for(char c: s) map[c]++;
	for(size_t i=0;i<s.length();i++)
	{
		if(map[s[i]]==1) return i;
	}
	return -1;
}
