#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		bool isIsomorphic(string s, string t);
};

int main()
{
	Solution mysol;
	string s,t;
	cout << "Enter s: ";
	cin >> s;
	cout << "Enter t: ";
	cin >> t;
	cout << mysol.isIsomorphic(s,t) << endl;
	return 0;
}

bool Solution::isIsomorphic(string s, string t)
{
	if(s.length()!=t.length()) return false;
	else if(s.empty() && t.empty()) return true;
	unordered_map<char,char> maps, mapt;

	for(int i=0;i<int(s.length());i++)
	{
		if(maps[s[i]]==0 && mapt[t[i]]==0)
		{
			maps[s[i]]=t[i];
			mapt[t[i]]=s[i];
		}
		else if(maps[s[i]]!=t[i] || mapt[t[i]]!=s[i]) return false;
	}
	return true;
}
