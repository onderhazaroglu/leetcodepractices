#include <iostream>
#include <unordered_map>
using namespace std;

int main()
{
	unordered_map<char,char> map;
	cout << map['a'] << endl;
	cout << map['b'] << endl;
	if(map['a']==' ') cout << "space" << endl;
	if(map['a']==0) cout << "zero" << endl;
	if(map['a']=='a') cout << "a" << endl;
	return 0;
}
