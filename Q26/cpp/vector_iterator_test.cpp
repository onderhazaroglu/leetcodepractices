#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector<int> nums = {1, 2, 3};

	for(auto it=nums.begin();it != nums.end();it++)
	{
		if ( *it == 2 ) it = it+1;
	}

	for(auto it: nums)
	{
		cout << it << endl;
	}
	return 0;
}
