#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		int removeDuplicates(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {-1, 0, 0, 1, 1, 2, 3, 3, 4};
	cout << mysol.removeDuplicates( nums ) << endl;
	return 0;
}

int Solution::removeDuplicates(vector<int>& nums)
{
	for(auto it=nums.begin(); it != nums.end();++it) { cout << *it << ", "; }
	cout << endl;

	unsigned int size = nums.size();
	for(auto it=nums.begin()+1; it != nums.end();++it)
	{
		if ( *it == *(it-1) ) nums.erase( it );
	}
	size = nums.size();
	for(auto it=nums.begin(); it != nums.end();++it) { cout << *it << ", "; }
	cout << endl;
	return static_cast<int>( size );
}
