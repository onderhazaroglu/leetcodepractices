#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		int removeDuplicates(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums;

	int val = 0, i=0;

	while( val >= 0 )
	{
		i++;
		cout << "Enter " << i << ". element or -1 to quit: ";
		cin >> val;
		if ( val == -1 ) break;
		else 
		{
			nums.push_back( val );
		}
	}

	for(auto it=nums.begin(); it != nums.end();++it) { cout << *it << ", "; }
	cout << endl;

	cout << "final size: " << mysol.removeDuplicates( nums ) << endl;
	for(auto it=nums.begin(); it != nums.end();++it) { cout << *it << ", "; }
	cout << endl;

	return 0;
}

int Solution::removeDuplicates(vector<int>& nums)
{
	int size = nums.size(), k=0;
	if ( size < 2 ) return size;

	cout << "size=" << size << endl;

	for(int i=1;i<size;i++)
	{
		if ( nums[i] > nums[k] ) { nums[++k] = nums[i]; }
	}

	return k+1;
}
