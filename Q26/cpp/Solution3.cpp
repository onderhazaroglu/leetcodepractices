#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		int removeDuplicates(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {-1, 0, 0, 1, 1, 2, 3, 3, 4};
	vector<int> nums = {1, 1, 1};
	// vector<int> nums = {1};
	cout << mysol.removeDuplicates( nums ) << endl;
	return 0;
}

int Solution::removeDuplicates(vector<int>& nums)
{
	for(auto it=nums.begin(); it != nums.end();++it) { cout << *it << ", "; }
	cout << endl;

	int size = nums.size();
	cout << "size=" << size << endl;

	int i=0;
	while( i<size-1 )
	{
		if ( nums[i] == nums[i+1] ) { nums.erase( nums.begin()+ i ); size--; }
		else i++;
	}

	size = nums.size();
	for(auto it=nums.begin(); it != nums.end();++it) { cout << *it << ", "; }
	cout << endl;
	cout << "size=" << i+1 << endl;
	return static_cast<int>( size );
}
