#include <iostream>
#include <vector>
using namespace std;

class NumArray
{
	vector<int> nums;
	vector<int> sum;

	public:
		NumArray(vector<int> nums) {
			this->nums = nums;
			int size = nums.size();
			this->sum = vector<int>(size+1,0);

			for(int i=0;i<int(nums.size());i++)
			{
				sum[i+1] = sum[i] + nums[i];
			}
		}

		int sumRange(int i,int j) {
			return sum[j+1]-sum[i];
		}
};

int main()
{
	vector<int> nums = {-2,0,3,-5,2,-1};
	NumArray na(nums);
	cout << na.sumRange(0,2) << endl;
	cout << na.sumRange(2,5) << endl;
	cout << na.sumRange(0,5) << endl;
	return 0;
}
