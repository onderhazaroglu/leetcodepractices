#include <iostream>
#include <vector>
#include <unordered_map>
#include <utility>
using namespace std;

struct keyHash {
	size_t operator()(const pair<int,int>& p) const
	{
		return hash<int>()(p.first) ^ hash<int>()(p.second) << 1;
	}
};

struct keyEqual {
	bool operator() (const pair<int,int>& lhs, const pair<int,int>& rhs) const
	{
		return lhs.first==rhs.first && lhs.second==rhs.second;
	}
};

class NumArray
{
	vector<int> nums;
	unordered_map<pair<int,int>,int,keyHash,keyEqual> map;

	public:
		NumArray(vector<int> nums) {
			this->nums = nums;
			int size = nums.size();
			map[pair<int,int>(0,0)]=nums[0];
			for(int i=0;i<size;i++)
			{
				for(int j=i+1;j<size;j++)
				{
					map[pair<int,int>(i,j)]=map[pair<int,int>(i,j-1)]+nums[j];
				}
			}
		}

		int sumRange(int i,int j);
};

int main()
{
	vector<int> nums = {-2,0,3,-5,2,-1};
	NumArray na(nums);
	cout << na.sumRange(0,2) << endl;
	cout << na.sumRange(2,5) << endl;
	cout << na.sumRange(0,5) << endl;
	return 0;
}

int NumArray::sumRange(int i,int j)
{
	return nums.empty()?0:map[pair<int,int>(i,j)];
	/*
	int sum = 0;
	for(;i<=j;i++)
	{
		sum += nums[i];
	}
	return sum;
	*/
}
