#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		char findTheDifference(string s, string t);
};

int main()
{
	Solution mysol;

	string s = "", t = "";
	cout << "Enter s:";
	cin >> s;

	cout << "Enter t:";
	cin >> t;

	cout << mysol.findTheDifference(s, t) << endl;
	return 0;
}

char Solution::findTheDifference(string s, string t)
{
	unordered_map<char,unsigned> letters;
	for(char i: s) letters[i]++;
	char res = ' ';

	for(char j: t)
	{
		if(letters[j]>0) letters[j]--;
		else
		{
			res = j;
			break;
		}
	}
	return res;
}
