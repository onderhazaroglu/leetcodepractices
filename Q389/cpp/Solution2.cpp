#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		char findTheDifference(string s, string t);
};

int main()
{
	Solution mysol;

	string s = "", t = "";
	cout << "Enter s:";
	cin >> s;

	cout << "Enter t:";
	cin >> t;

	cout << mysol.findTheDifference(s, t) << endl;
	return 0;
}

char Solution::findTheDifference(string s, string t)
{
	int sums = 0, sumt = 0;
	for(char i: s) sums += i;
	for(char i: t) sumt += i;

	return static_cast<char>(sumt-sums);
}
