#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int arrayPairSum(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,4,3,2};
	cout << mysol.arrayPairSum(nums) << endl;
	return 0;
}

int Solution::arrayPairSum(vector<int>& nums)
{
	vector<int> vec(20001,0);
	for(size_t i=0;i<nums.size();i++)
	{
		vec[nums[i]+10000]++;
	}

	unsigned int t = 0;
	int sum = 0;
	for(size_t i=0;i<20001;)
	{
		if(vec[i]>0 && t==0)
		{
			sum += i-10000;
			t = 1;
			vec[i]--;
		}
		else if (vec[i]>0 && t==1)
		{
			vec[i]--;
			t = 0;
		}
		else i++;
	}
	return sum;
}
