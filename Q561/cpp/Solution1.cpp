#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int arrayPairSum(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,4,3,2};
	cout << mysol.arrayPairSum(nums) << endl;
	return 0;
}

int Solution::arrayPairSum(vector<int>& nums)
{
	std::sort(nums.begin(), nums.end());
	int sum = 0;
	for(unsigned int i=0;i<nums.size()-1;i+=2)
	{
		sum += nums[i];
	}
	return sum;
}
