#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q);
};

int main()
{
	Solution mysol;

	TreeNode* r1 = new TreeNode(6);
	r1->left = new TreeNode(2);
	r1->left->left = new TreeNode(0);

	r1->left->right = new TreeNode(4);

	r1->left->right->left = new TreeNode(3);
	r1->left->right->right = new TreeNode(5);

	r1->right = new TreeNode(8);
	r1->right->left = new TreeNode(7);

	TreeNode* p = r1->right->left;;

	r1->right->right = new TreeNode(9);

	TreeNode* q = r1->right->right;

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	print_tree(r1);

	TreeNode* res = mysol.lowestCommonAncestor(r1,p,q);
	if(res) cout << "res: " << res->val << endl;
	return 0;
}

TreeNode* Solution::lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q)
{
	TreeNode* temp = root;
	while(temp)
	{
		if(p->val<temp->val && q->val<temp->val) temp = temp->left;
		else if(p->val>temp->val && q->val>temp->val) temp = temp->right;
		else return temp;
	}
	/*
	if(!p || !q || !root) return res;
	while(root)
	{
		if( (p->val<root->val && q->val>root->val) || p->val==root->val || q->val==root->val ) return root;
		if(p->val<root->val && q->val<root->val) root = root->left;
		else if(p->val>root->val && q->val>root->val) root = root->right;
	}
	*/
	return temp;
}
