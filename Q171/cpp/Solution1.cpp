#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		int titleToNumber(string s);
};

int main()
{
	Solution mysol;
	string col;
	cout << "Enter col: ";
	cin >> col;

	cout << mysol.titleToNumber(col) << endl;
	return 0;
}

int Solution::titleToNumber(string s)
{
	int num = 0;
	for(unsigned int i=0;i<s.length();i++)
	{
		num += num*(26-1) + s[i] - 65 + 1;
	}
	return num;
}
