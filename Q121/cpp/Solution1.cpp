#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int maxProfit(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {7,1,5,3,6,4,2,15,8,9,10,11};
	cout << mysol.maxProfit(nums) << endl;
	return 0;
}

int Solution::maxProfit(vector<int>& nums)
{
	int size = nums.size();
	if(size<2) return 0;
	int maxp = 0, min = nums[0];
	for(int i=1;i<size;i++)
	{
		maxp = max(maxp, nums[i]-min);
		if(nums[i]<min) min = nums[i];
	}
	return maxp;
}
