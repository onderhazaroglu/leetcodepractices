#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		bool containsNearbyDuplicate(vector<int>& nums,int k);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,2,3,5,7,4,8,7,6,5};
	int k = 3;
	cout << mysol.containsNearbyDuplicate(nums,k) << endl;
	return 0;
}

bool Solution::containsNearbyDuplicate(vector<int>& nums,int k)
{
	unordered_map<int,int> map;
	for(int i=0;i<int(nums.size());i++)
	{
		if(map.find(nums[i])==map.end()) map[nums[i]] = i;
		else
		{
			if(abs(map[nums[i]]-i)<=k) return true;
			else map[nums[i]] = i;
		}
	}
	return false;
}
