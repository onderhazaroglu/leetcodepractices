#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int findLengthOfLCIS(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,3,5,4,7};
	cout << mysol.findLengthOfLCIS(nums) << endl;
	return 0;
}

int Solution::findLengthOfLCIS(vector<int>& nums)
{
	int size = nums.size();
	if(size<2) return size;

	int max = 0, cur = 1;
	for(int i=1;i<size;i++)
	{
		if(nums[i]>nums[i-1]) cur++;
		else cur = 1;

		if(cur>max) max = cur;
	}
	return max;
}
