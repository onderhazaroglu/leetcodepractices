#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		string addString(string num1, string num2);
};

int main()
{
	Solution mysol;
	string num1, num2;
	cout << "Enter number 1: ";
	cin >> num1;
	cout << "Enter number 2: ";
	cin >> num2;
	cout << mysol.addString(num1,num2) << endl;
	return 0;
}

string Solution::addString(string num1, string num2)
{
	int size1 = num1.length(), size2 = num2.length(), i=size1-1, j=size2-1, tsum = 0, rem = 0;
	string sum = "";

	while(i >=0 || j >=0)
	{
		tsum = (i>=0?num1[i]-48:0) + (j>=0?num2[j]-48:0);
		sum.insert(0,1,char((tsum+rem)%10+48));
		rem = (tsum+rem)/10;
		cout << "sum: " << sum << endl;
		i--;j--;
	}
	if(rem>0) sum.insert(0,1,char(rem+48) );
	return sum;
}
