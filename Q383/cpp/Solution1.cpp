#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		bool canConstruct(string ransomNote, string magazine);
};

int main()
{
	Solution mysol;
	string ransomNote, magazine;
	cout << "Enter ransomNote: ";
	cin >> ransomNote;
	cout << "Enter magazine: ";
	cin >> magazine;
	if(mysol.canConstruct(ransomNote,magazine)) cout << "can be constructed." << endl;
	else cout << "can NOT be constructed!" << endl;
	return 0;
}

bool Solution::canConstruct(string ransomNote, string magazine)
{
	unordered_map<char,int> ranmap, magmap;
	for(auto c: ransomNote) ranmap[c]++;
	for(auto c: magazine) magmap[c]++;

	for(auto it: ranmap)
	{
		if(it.second>magmap[it.first]) return false;
		cout << it.first << ":" << it.second << endl;
	}
	return true;
}
