#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		bool wordPattern(string pattern,string str);
};

int main()
{
	Solution mysol;
	string p,s;
	cout << "Enter pattern: ";
	cin >> p;
	cout << "Enter string: ";
	cin.ignore();
	getline(cin,s);
	cout << mysol.wordPattern(p,s) << endl;
	return 0;
}

bool Solution::wordPattern(string pattern,string str)
{
	istringstream iss(str);
	vector<string> tokens { istream_iterator<string> {iss}, istream_iterator<string> {} };
	if(pattern.length()!=tokens.size()) return false;
	unordered_map<char,string> mp;
	unordered_map<string,char> ms;

	for(int i=0;i<int(pattern.length());i++)
	{
		if(mp.find(pattern[i])==mp.end()) { mp[pattern[i]] = tokens[i]; ms[tokens[i]] = pattern[i]; }
		else if(mp[pattern[i]]!=tokens[i] || ms[tokens[i]]!=pattern[i]) return false;
	}
	return true;
	/*
	cout << tokens.size() << endl;
	for(string s: tokens) cout << s << endl;
	return pattern.length()==tokens.size();
	*/
}
