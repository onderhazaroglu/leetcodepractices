#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		bool wordPattern(string pattern,string str);
};

int main()
{
	Solution mysol;
	string p,s;
	cout << "Enter pattern: ";
	cin >> p;
	cout << "Enter string: ";
	cin.ignore();
	getline(cin,s);
	cout << mysol.wordPattern(p,s) << endl;
	return 0;
}

bool Solution::wordPattern(string pattern,string str)
{
	unordered_map<char,string> mp;
	unordered_map<string,char> ms;
	str += " ";

	for(auto c: pattern)
	{
		if(str.empty()) return false;

		auto pos = str.find(' ');
		string tmp = str.substr(0,pos);
		str = str.substr(pos+1);
		if( (mp.count(c) && mp[c] != tmp) || (ms.count(tmp) && ms[tmp] != c) ) return false;
		mp[c] = tmp;
		ms[tmp] = c;
	}
	return true;
}
