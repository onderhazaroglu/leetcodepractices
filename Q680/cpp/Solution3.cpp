#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		bool sub(string s);
		bool validPalindrome(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.validPalindrome(s) << endl;
	return 0;
}

bool Solution::sub(string s)
{
	int i=0,j=s.length()-1;
	while(i<j)
	{
		if(s[i]!=s[j]) return false;
		i++, j--;
	}
	return true;
}

bool Solution::validPalindrome(string s)
{
	int i=0,j=s.length()-1;
	if(j==0||j==1) return true;

	while(i<=j)
	{
		if(s[i]!=s[j])
		{
			if(sub(s.substr(i+1,j-i)) || sub(s.substr(i,j-i)) ) return true;
			else return false;
		}
		i++, j--;
	}
	return true;
}
