#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	int deleted = 0;

	public:
		bool sub(string s,int i,int j);
		bool validPalindrome(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.validPalindrome(s) << endl;
	return 0;
}

bool Solution::sub(string s,int i,int j)
{
	while(i<j)
	{
		for(int k=0;k<int(s.length());k++)
		{
			if(k==i || k==j) cout << "[" << s[k] << "]";
			else cout << s[k];
		}
		cout << endl;
		cout << "s[" << i << "]: " << s[i] << ", s[" << j << "] : " << s[j] << endl;
		if(s[i]==s[j]) i++, j--;
		else
		{
			if(deleted++) return false;
			else return ( s[i+1]==s[j] && sub(s,i+1,j) ) || ( s[i]==s[j-1] && sub(s,i,j-1) );
		}
	}
	return true;
}

bool Solution::validPalindrome(string s)
{
	return sub(s,0,s.length()-1);
}
