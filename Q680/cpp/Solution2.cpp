#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	int deleted = 0;
	public:
		bool sub(string s,int i,int j);
		bool validPalindrome(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.validPalindrome(s) << endl;
	return 0;
}

bool Solution::sub(string s,int i,int j)
{
	for(int k=0;k<int(s.length());k++)
	{
		if(k==i || k==j) cout << "[" << s[k] << "]";
		else cout << s[k];
	}
	cout << endl;
	cout << "s[" << i << "]: " << s[i] << ", s[" << j << "] : " << s[j] << endl;
	if(s[i]==s[j]) return i>=j || sub(s,i+1,j-1);
	else if( (s[i+1]==s[j] || s[i]==s[j-1]) && !deleted) { deleted++; return sub(s,i+1,j) || sub(s,i,j-1); }
	return false;
}

bool Solution::validPalindrome(string s)
{
	return sub(s,0,s.length()-1);
}
