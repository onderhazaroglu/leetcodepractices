#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<int> plusOne(vector<int>& digits);
};

int main()
{
	Solution mysol;
	vector<int> digits = {9,9,9};
	for(int i: mysol.plusOne(digits)) cout << i << " ";
	cout << endl;
	return 0;
}

vector<int> Solution::plusOne(vector<int>& digits)
{
	int i = digits.size()-1;
	while(digits[i]==9)
	{
		digits[i--]=0;
	}

	if(i>=0) digits[i]++;
	else digits.insert(digits.begin(),1);

	return digits;
}
