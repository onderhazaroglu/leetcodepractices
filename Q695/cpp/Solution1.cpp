#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int grid_search(vector<vector<int>>& grid, vector<vector<int>>& map,unsigned int i, unsigned int j);
		int maxAreaOfIsland(vector<vector<int>>& grid);
};

int main()
{
	Solution mysol;
	vector<vector<int>> grid = {
		vector<int> {0,0,1,0,0,0,0,1,0,0,0,0,0},
		vector<int> {0,0,0,0,0,0,0,1,1,1,0,0,0},
		vector<int> {0,1,1,0,1,0,0,0,0,0,0,0,0},
		vector<int> {0,1,0,0,1,1,0,0,1,0,1,0,0},
		vector<int> {0,1,0,0,1,1,0,0,1,1,1,0,0},
		vector<int> {0,0,0,1,1,0,0,0,0,0,1,0,0},
		vector<int> {0,0,0,0,0,0,0,1,1,1,1,0,0},
		vector<int> {0,0,0,0,0,0,0,1,1,0,0,0,0} };

	cout << "Maximum area: " << mysol.maxAreaOfIsland(grid) << endl;
	return 0;
}

int Solution::grid_search(vector<vector<int>>& grid, vector<vector<int>>& map,unsigned int i, unsigned int j)
{
	cout << "=== Running search for (" << i << "," << j << ") ===" << endl;
	unsigned int area = 1;
	map[i][j] = 1;

	if(i<grid.size()-1 && map[i+1][j]<1 && grid[i+1][j]>0)
	{
		area += grid_search(grid,map,i+1,j);
		map[i+1][j] = 1;
	}
	else cout << "Nope (" << int(i)+1 << "," << j << ")" << endl;
	if(i>0 && map[i-1][j]<1 && grid[i-1][j]>0)
	{
		area += grid_search(grid,map,i-1,j);
		map[i-1][j] = 1;
	}
	else cout << "Nope (" << int(i)-1 << "," << j << ")" << endl;

	if(j<grid[0].size()-1 && map[i][j+1]<1 && grid[i][j+1])
	{
		area += grid_search(grid,map,i,j+1);
		map[i][j+1] = 1;
	}
	else cout << "Nope (" << i << "," << int(j)+1 << ")" << endl;
	if(j>0 && map[i][j-1]<1 && grid[i][j-1])
	{
		area += grid_search(grid,map,i,j-1);
		map[i][j-1] = 1;
	}
	else cout << "Nope (" << i << "," << int(j)-1 << ")" << endl;

	cout << "=== End of search (" << i << "," << j << ") ===" << endl;
	return area;
}

int Solution::maxAreaOfIsland(vector<vector<int>>& grid)
{
	if(grid.size()==0 && grid[0].size() == 0) return 0;
	unsigned int area = 0, maxarea = 0, h = grid.size(), w = grid[0].size();

	vector<vector<int>> map(h,vector<int>(w,-1));

	for(unsigned int i=0;i<h;i++)
	{
		for(unsigned int j=0;j<w;j++)
		{
			cout << "At: (" << i << "," << j << ")" << endl;
			if(grid[i][j]==0 || map[i][j]>-1)
			{
				cout << "\tSkipping (" << i << "," << j << "): " << grid[i][j] << "(" << map[i][j] << ")" << endl;
				continue;
			}
			area = grid_search(grid,map,i,j);
			cout << "Area: " << area << endl;
			if(area>maxarea) maxarea = area;
		}
	}
	return maxarea;
}
