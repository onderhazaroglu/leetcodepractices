#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		int leftSum(TreeNode* root,bool left);
		int sumOfLeftLeaves(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(3);
	r1->left = new TreeNode(9);
	r1->right = new TreeNode(20);
	r1->right->left = new TreeNode(15);
	r1->right->right = new TreeNode(7);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	cout << mysol.sumOfLeftLeaves(r2) << endl;

	return 0;
}

int Solution::leftSum(TreeNode* root,bool left)
{
	if(!root) return 0;
	return left && !root->left && !root->right?root->val:leftSum(root->left,true) + leftSum(root->right,false);
}
int Solution::sumOfLeftLeaves(TreeNode* root)
{
	if(!root) return 0;
	return leftSum(root->left,true) + leftSum(root->right,false);
}
