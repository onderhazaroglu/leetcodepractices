#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		string addBinary(string a,string b);
};

int main()
{
	Solution mysol;
	string a,b;
	cout << "Enter a: ";
	cin >> a;
	cout << "Enter b: ";
	cin >> b;
	cout << mysol.addBinary(a,b) << endl;
	return 0;
}

string Solution::addBinary(string a,string b)
{
	int lena = a.length(), lenb = b.length();
	string ar(a.rbegin(),a.rend()), br(b.rbegin(),b.rend());
	int ml = max(lena, lenb);
	int rem = 0;
	string res;

	for(int i=0;i<ml;i++)
	{
		cout << "i: " << i << endl;
		if(i<lena && i<lenb)
		{
			if(ar[i]=='0' && br[i]=='0') { res.push_back(char(48+rem)); rem = 0; }
			else if(ar[i]=='1' && br[i]=='1') { res.push_back(char(48+rem)); rem = 1; }
			else
			{
				if(rem==0) res.push_back('1');
				else { res.push_back('0'); rem = 1; }
			}
		}
		else if(i<lena)
		{
			if(rem==0 && ar[i]=='0') res.push_back('0');
			else
			{
				if(ar[i]=='0') { res.push_back(char(48+rem)); rem = 0; }
				else { res.push_back( char(48+(rem==0?1:0)) ); }
			}
		}
		else
		{
			if(rem==0 && br[i]=='0') res.push_back('0');
			else
			{
				if(br[i]=='0') { res.push_back(char(48+rem)); rem = 0; }
				else { res.push_back( char(48+(rem==0?1:0)) ); }
			}
		}
		cout << "res: " << res << endl;
		cout << "rem: " << rem << endl;

	}
	if(rem>0) res.push_back(48+1);

	return string(res.rbegin(),res.rend());
}
