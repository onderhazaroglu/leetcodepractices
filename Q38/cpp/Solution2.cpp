#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		string countAndSay(int n);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter num: ";
	cin >> num;
	// cout << mysol.countAndSay(num) << endl;
	// cout << "1112234445" << endl;
	cout << mysol.countAndSay(num) << endl;
	return 0;
}

string Solution::countAndSay(int n)
{
	string res="1";
	for(int i=1;i<n;i++) {
		string temp=move(res);
		res="";
		int count=1;
		char cur=temp[0];
		for(int j=1;j<int(temp.size());j++) {
			if(temp[j]==cur)
				count++;
			else {
				res.push_back(count+'0');
				res.push_back(cur);
				count=1;
				cur=temp[j];
			}
		}
		res.push_back(count+'0');
		res.push_back(cur);
	}
	return res;
}
