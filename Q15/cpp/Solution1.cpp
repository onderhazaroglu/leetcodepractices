#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>

using namespace std;

struct mypair
{
	int first;
	int second;
	mypair(int nf, int ns): first(nf), second(ns) {}
	mypair(): first(0), second(0) {}
};


class Solution
{
	public:
		unordered_map<int,int> twoSum(vector<int> &nums, int target);
		vector<vector<int>> threeSum(vector<int>& nums, int target);
};


/*
int main() {
	Solution sol1;

	// int my_ints[] = {2, 11, 7, 15};
	int my_ints[] = {-1, 0, 1, 2, -1, -4};

	vector<int> my_nums (&my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) );
	unordered_map<int,int> res;

	res = sol1.twoSum(my_nums, 1);

	for(auto it: res)
	{
		cout << it.first << " : " << it.second << endl;
	}

	return 0;
}
*/

int main()
{
	Solution mysol;

	int my_ints[] = {-1, 0, 1, 2, -1, -4};

	vector<int> my_nums (&my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) );

	vector<vector<int>> results = mysol.threeSum( my_nums, 0);

	cout << "in main results size: " << results.size() << endl;

	for(auto it: results)
	{
		cout << it[0] << ", " << it[1] << ", " << it[2] << endl;
	}

	cout << endl;
	return 0;
}

unordered_map<int,int> Solution::twoSum(vector<int> &nums, int target) {
	vector<mypair> pairs;
	unordered_map <int, int> map;
	unordered_map <int, int> res;

	for (unsigned int i=0;i<nums.size();i++) {
		std::unordered_map<int,int>::const_iterator got = map.find( target-nums[i] );

		if ( ( got != map.end() && got->first != int(nums[i]) ) ) {
			cout << "found " << target-nums[i] << " for " << nums[i] << endl;
			mypair newpair;
			newpair.first = nums[i];
			newpair.second = target-nums[i];

			pairs.push_back( newpair );

			if (nums[i] < got->first) res[nums[i]] = got->first;
			else res[got->first] = nums[i];
		}
		map[nums[i]] = i;
	}

	return res;
}

vector<vector<int>> Solution::threeSum(vector<int>& nums, int target)
{
	Solution mysol;
	vector<mypair> pairs;
	set <vector<int>> myset;
	// unordered_map<int,mypair> mtriplets;
	// unordered_map<mypair,int> mtriplets;
	set<vector<int>> mtriplets;

	vector<vector<int>> results;

	unsigned int nsize = nums.size();

	vector<int> tnums;

	for(unsigned int i=0;i<nsize;i++)
	{
		cout << "looking with " << nums[i] << endl << "------------" << endl;
		unordered_map <int, int> res;

		tnums = nums;
		tnums.erase(tnums.begin()+i);

		res = mysol.twoSum(tnums, target-nums[i]);

		for(auto it: res)
		{
			cout << "RES: " << nums[i] << " -> " << it.first << " -> " << it.second << endl;
			if ( nums[i] <= it.first )
			{
				cout << "\n1| " << nums[i] << " " << it.first << " " << it.second << endl;
				int my_ints[] = { nums[i], it.first, it.second };
				mtriplets.insert( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );

			}
			else if ( nums[i] >= it.second )
			{
				cout << "\n2| " << it.first << " " << it.second << " " << nums[i] << endl;
				int my_ints[] = { it.first, it.second, nums[i] };
				mtriplets.insert( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );
			}
			else
			{
				cout << "\n3| " << it.first << " " << nums[i] << " " << it.second << endl;
				int my_ints[] = { it.first, nums[i], it.second };
				mtriplets.insert( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );
			}

		}

		cout << "For " << nums[i] << ", size mtriplets: " << mtriplets.size() << endl;


		cout << "--------------" << endl;
	}

	for(auto it: mtriplets)
	{
		// int my_ints[] = {it.first, it.second.first, it.second.second};
		// results.push_back( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );
		results.push_back( it );
	}

	cout << "\n\nsize mtriplets: " << mtriplets.size() << endl;

	for(auto it: mtriplets)
	{
		// cout << "[" << it.first << "]" << "[" << it.second.first << "]" << "[" << it.second.second << "]" << endl;
		cout << "[" << it[0] << "]" << "[" << it[1] << "]" << "[" << it[2] << "]" << endl;
	}
	
	return results;
}
