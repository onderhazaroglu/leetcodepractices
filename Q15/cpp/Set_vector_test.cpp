#include <iostream>
#include <vector>
#include <set>
using namespace std;

int main()
{
	set<vector<int>> myset;
	
	int my_ints[] = {1, 2, 3};
	myset.insert( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );

	int my_ints2[] = {1, 2, 4};
	myset.insert( vector<int>( &my_ints2[0], &my_ints2[0] + sizeof(my_ints2) / sizeof(int) ) );

	int my_ints3[] = {1, 2, 3};
	myset.insert( vector<int>( &my_ints3[0], &my_ints3[0] + sizeof(my_ints3) / sizeof(int) ) );

	cout << myset.size() << endl;

	return 0;
}
