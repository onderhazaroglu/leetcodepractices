#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

class Solution
{
	public:
		vector<vector<int>> threeSum(vector<int>& nums);
};


int main()
{
	Solution mysol;

	// int my_ints[] = {-1, 0, 1, 2, -1, -4, 3,5,-3,1,0,0,7,-7};
	// int my_ints[] = {-1, 0, 1, 2, -1, -4};

	vector<int> my_nums {-1, 0, 1, 2, -1, -4, 3,5,-3,1,0,0,7,-7};

	vector<vector<int>> results = mysol.threeSum( my_nums );

	for(auto it: results)
	{
		cout << "[" << it[0] << "]" << "[" << it[1] << "]" << "[" << it[2] << "]" << endl;
	}

	return 0;
}


vector<vector<int>> Solution::threeSum(vector<int>& nums)
{
	int size = nums.size();
	vector<vector<int>> results;

	std::sort(nums.begin(),nums.end());

	for(int i=0;i<size-2;i++)
	{
		if (i>0 && (nums[i]==nums[i-1]) ) continue;
		unsigned int l = i+1, u = size-1;

		while(l < u)
		{
			auto sum = nums[l] + nums[u];

			if ( sum > -nums[i] ) u--;
			else if ( sum < -nums[i] ) l++;
			else
			{
				results.push_back( vector<int>{nums[i],nums[l],nums[u]} );
				while(l+1<u && nums[l]==nums[l+1]) l++;
				while(l<u-1 && nums[u]==nums[u-1]) u--;
				l++; u--;
			}	
		}

	}

	return results;
}
