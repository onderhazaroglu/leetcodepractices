#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>
#include <algorithm>

using namespace std;

class Solution
{
	public:
		vector<vector<int>> threeSum(vector<int>& nums);
};


int main()
{
	Solution mysol;

	int my_ints[] = {-1, 0, 1, 2, -1, -4, 3,5,-3,1,0,0,7,-7};
	// int my_ints[] = {-1, 0, 1, 2, -1, -4};

	vector<int> my_nums (&my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) );

	vector<vector<int>> results = mysol.threeSum( my_nums );

	for(auto it: results)
	{
		cout << "[" << it[0] << "]" << "[" << it[1] << "]" << "[" << it[2] << "]" << endl;
	}

	return 0;
}


vector<vector<int>> Solution::threeSum(vector<int>& nums)
{

	vector<vector<int>> results;

	set<vector<int>> mtriplets;

	std::sort(nums.begin(),nums.end());

	unsigned int size = nums.size();
	int sum = 0;

	for(unsigned int i=0;i<size;i++)
	{
		unsigned int front = i+1, back = size-1;

		while(front < back)
		{
			sum = nums[front] + nums[back];

			if ( sum > -nums[i] ) back--;
			else if ( sum < -nums[i] ) front++;
			else
			{
				cout << "found " << nums[i] << ", " << nums[front] << ", " << nums[back] << endl;
				int my_ints[] = { nums[i], nums[front], nums[back] };
				mtriplets.insert( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );
				front++;
			}	
		}

	}

	for(auto it: mtriplets)
	{
		results.push_back( it );
	}

	return results;
}
