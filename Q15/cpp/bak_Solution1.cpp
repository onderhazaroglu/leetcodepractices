#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>

using namespace std;

struct mypair
{
	int first;
	int second;
};


class Solution
{
	public:
		// vector<mypair> twoSum(vector<int> &nums, int target);
		unordered_map<int,int> twoSum(vector<int> &nums, int target);
		// vector<vector<int>> threeSum(vector<int>& nums);
};



int main() {
	Solution sol1;

	// int my_ints[] = {2, 11, 7, 15};
	int my_ints[] = {-1, 0, 1, 2, -1, -4};

	vector<int> my_nums (&my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) );
	unordered_map<int,int> res;
	// vector<mypair> my_nums2;

	// my_nums2 = sol1.twoSum(my_nums, 0);
	res = sol1.twoSum(my_nums, 1);

	for(auto it: res)
	{
		cout << it.first << " : " << it.second << endl;
	}

	/*
		for(unsigned int i=0;i<res.size();i++) {
		std::cout<< res[i] << ", " << my_nums[my_nums2[i].second] << endl;
	}
	*/
	return 0;
}


/*
int main()
{
	cout << endl;
	return 0;
}
*/

unordered_map<int,int> Solution::twoSum(vector<int> &nums, int target) {
	vector<mypair> pairs;
	unordered_map <int, int> map;
	unordered_map <int, int> res;

	for (unsigned int i=0;i<nums.size();i++) {
		std::unordered_map<int,int>::const_iterator got = map.find( target-nums[i] );

		if ( ( got != map.end() && got->first != int(nums[i]) ) ) {
			cout << "found" << target-nums[i] << " for " << nums[i] << endl;
			mypair newpair;
			newpair.first = nums[i];
			newpair.second = target-nums[i];

			pairs.push_back( newpair );

			if (nums[i] < got->first) res[nums[i]] = got->first;
			else res[got->first] = nums[i];
		}
		map[nums[i]] = i;
	}

	return res;
}

/*
vector<vector<int>> Solution::threeSum(vector<int>& nums, int target)
{
	Solution mysol;
	vector<mypair> pairs;
	set <vector<int>> myset;
	int a,b,c;

	unsigned int nsize = nums.size();

	vector<int> tnums;

	for(unsigned int i=0;i<nsize;i++)
	{
		tnums = nums;
		tnums.erase(tnums.begin()+i);

		pairs = mysol.twoSum(tnums, target-nums[i]);

		for(unsigned int j=0;j<pairs.size();j++)
		{
			b = nums[i];
			if ( pairs[
		}
		std::unordered_map<int,int>::const_iterator got = map.find( target-nums[i] );
	}
}
*/
