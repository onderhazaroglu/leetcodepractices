#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>

using namespace std;

class Solution
{
	public:
		unordered_map<int,int> twoSum(vector<int> &nums, int target);
		vector<vector<int>> threeSum(vector<int>& nums, int target);
};


int main()
{
	Solution mysol;

	int my_ints[] = {-1, 0, 1, 2, -1, -4, 3,5,-3,1,0,0,7,-7};

	vector<int> my_nums (&my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) );

	vector<vector<int>> results = mysol.threeSum( my_nums, 0);

	cout << "in main results size: " << results.size() << endl;

	for(auto it: results)
	{
		cout << it[0] << ", " << it[1] << ", " << it[2] << endl;
	}

	cout << endl;
	return 0;
}

unordered_map<int,int> Solution::twoSum(vector<int> &nums, int target) {
	unordered_map <int, int> map;
	unordered_map <int, int> res;

	for (unsigned int i=0;i<nums.size();i++) {
		std::unordered_map<int,int>::const_iterator got = map.find( target-nums[i] );

		if ( ( got != map.end() && got->second != int(i) ) ) {

			if (nums[i] < got->first) res[nums[i]] = got->first;
			else res[got->first] = nums[i];
		}
		map[nums[i]] = i;
	}

	return res;
}

vector<vector<int>> Solution::threeSum(vector<int>& nums, int target)
{
	set<vector<int>> mtriplets;

	vector<vector<int>> results;

	unsigned int nsize = nums.size();

	vector<int> tnums;

	for(unsigned int i=0;i<nsize;i++)
	{
		unordered_map <int, int> res;

		tnums = nums;
		tnums.erase(tnums.begin()+i);

		res = twoSum(tnums, target-nums[i]);

		for(auto it: res)
		{
			if ( nums[i] <= it.first )
			{
				int my_ints[] = { nums[i], it.first, it.second };
				mtriplets.insert( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );

			}
			else if ( nums[i] >= it.second )
			{
				int my_ints[] = { it.first, it.second, nums[i] };
				mtriplets.insert( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );
			}
			else
			{
				int my_ints[] = { it.first, nums[i], it.second };
				mtriplets.insert( vector<int>( &my_ints[0], &my_ints[0] + sizeof(my_ints) / sizeof(int) ) );
			}
		}
	}

	for(auto it: mtriplets)
	{
		results.push_back( it );
		cout << "[" << it[0] << "]" << "[" << it[1] << "]" << "[" << it[2] << "]" << endl;
	}

	return results;
}
