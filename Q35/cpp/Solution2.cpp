#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int searchInsert(vector<int>& nums,int target);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {1,3,5,6};
	vector<int> nums = {1,3};
	int target;
	cout << "Enter target: ";
	cin >> target;
	cout << mysol.searchInsert(nums,target) << endl;
	return 0;
}

int Solution::searchInsert(vector<int>& nums,int target)
{
	int l=0, r=nums.size()-1;
	if(target>nums.back()) return nums.size();
	while(l<r)
	{
		int mid = (l+r)/2;
		if(target<=nums[l]) return l;
		else if(target>=nums[r]) return target==nums[r]?r:r+1;
		else if(target==nums[mid]) return mid;
		else if(target<nums[mid]) r=mid;
		else l=mid+1;
	}
	return l;
}
