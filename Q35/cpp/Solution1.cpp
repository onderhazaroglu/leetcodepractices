#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int searchInsert(vector<int>& nums,int target);
		int binsearch(vector<int>& nums,int l,int r,int target);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,3};
	int target;
	cout << "Enter target: ";
	cin >> target;
	cout << mysol.searchInsert(nums,target) << endl;
	return 0;
}

int Solution::binsearch(vector<int>& nums,int l,int r,int target)
{
	if(l<=r-2)
	{
		int mid = (l+r)/2;
		if(target<=nums[l]) return l;
		else if(target>=nums[r]) return target==nums[r]?r:r+1;
		else if(target==nums[mid]) return mid;
		else if(target<nums[mid]) return binsearch(nums,l,mid-1,target);
		else return binsearch(nums,mid+1,r,target);
	}

	if(target<=nums[l]) return l;
	else if(target>nums[l] && target<=nums[r]) return r;
	else return r+1;
}

int Solution::searchInsert(vector<int>& nums,int target)
{
	return nums.empty()?0:binsearch(nums,0,nums.size()-1,target);
}
