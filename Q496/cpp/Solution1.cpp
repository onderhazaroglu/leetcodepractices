#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		vector<int> nextGreaterElement(vector<int>& findNums, vector<int>& nums);
};

int find_ng_bs( vector<int>& nums, int tar, int l, int r)
{
	cout << "tar: " << tar << ", l: " << l << ", r: " << r << endl;
	if ( tar == nums[(l+r)/2] ) return tar;
	else if ( tar < nums[l] ) return nums[l];
	else if ( tar > nums[r] ) return -1;
	else if ( l == r -1 ) return nums[r];
	else if ( tar < nums[(l+r)/2] ) return find_ng_bs( nums, tar, l, (l+r)/2 );
	else return find_ng_bs( nums, tar, (l+r)/2 +1, r );
}

int main()
{
	Solution mysol;

	vector<int> nums = {50, 40, 70, 20, 10, 30, 60, 70};
	std::sort(nums.begin(),nums.end());

	int tar = 0;
	cout << "Enter target: ";
	cin >> tar;

	cout << find_ng_bs( nums, tar, 0, 7 ) << endl;

	vector<int> findNums = {8, 15, 32, 60, 70};
	vector<int> foundNums = mysol.nextGreaterElement(findNums, nums);

	for(auto i: nums)
	{
		cout << i << ", ";
	}
	cout << endl;
	for(auto i: findNums)
	{
		cout << i << ", ";
	}
	cout << endl;
	for(auto i: foundNums)
	{
		cout << i << ", ";
	}

	cout << endl;
	return 0;
}

vector<int> Solution::nextGreaterElement(vector<int>& findNums, vector<int>& nums)
{
	vector<int> res;
	if ( nums.empty() || findNums.empty() ) return res;
	std::sort(nums.begin(),nums.end());
	unsigned int size = nums.size();

	for(auto n: findNums)
	{
		if ( n+1 <= nums[size-1] ) res.push_back( find_ng_bs( nums, n+1, 0, size-1 ) );
		else res.push_back( -1 );
	}
	return res;
}
