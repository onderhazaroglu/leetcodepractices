#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<int> selfDividingNumbers(int left, int right);
};

int main()
{
	Solution mysol;
	int left,right;
	cout << "Enter left: ";
	cin >> left;
	cout << "Enter right: ";
	cin >> right;

	vector<int> result = mysol.selfDividingNumbers(left,right);
	for(int i: result)
	{
		cout << i << endl;
	}
	return 0;
}

vector<int> Solution::selfDividingNumbers(int left, int right)
{
	vector<int> res;

	for(int i=left;i<=right;i++)
	{
		cout << "Number: " << i << endl;
		int n = i;
		bool sd = true;
		do
		{
			cout << "n1: " << n << endl;
			if(n % 10 == 0 || i % ( n % 10 ) != 0) { sd = false; break; }
			cout << "dividing..." << endl;
			n /= 10;
			cout << "n2: " << n << endl;
		}
		while(n>0);
		if(sd) { cout << "Adding " << i << endl; res.push_back(i); }
	}
	return res;
}
