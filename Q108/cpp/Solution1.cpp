#include <iostream>
#include <vector>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		TreeNode* sortedArrayToBST(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {-10,-3,0,5,9};
	print_tree(mysol.sortedArrayToBST(nums));
	return 0;
}

TreeNode* Solution::sortedArrayToBST(vector<int>& nums)
{
	if(nums.empty()) return NULL;
	for(auto n: nums)
	{
		cout << n << ", ";
	}
	cout << endl;

	TreeNode* root;
	int size = nums.size();
	int mid = size/2;
	root = new TreeNode(nums[mid]);
	if(mid>0)
	{
		vector<int> lnums(nums.begin(),nums.begin()+mid);
		root->left = sortedArrayToBST(lnums);
	}
	if(mid<size-1)
	{
		vector<int> rnums(nums.begin()+mid+1,nums.end());
		root->right = sortedArrayToBST(rnums);
	}
	return root;
}
