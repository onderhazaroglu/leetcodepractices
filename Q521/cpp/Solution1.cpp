#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		int findLUSlength(string a, string b);
};

int main()
{
	Solution mysol;
	string a, b;
	cin >> a;
	cin >> b;
	cout << mysol.findLUSlength(a,b) << endl;
	return 0;
}

int Solution::findLUSlength(string a, string b)
{
	int al = a.length(), bl = b.length();
	if(al == bl) return a == b ? 0 : al;
	else if(al>bl) return al;
	else return bl;
}
