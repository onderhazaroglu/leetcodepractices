#include <iostream>
#include <string>
#include <unordered_map>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}

class Solution {
	unordered_map<int, int> mp;
	int ans, tar;

	void dfs(TreeNode * p, int s)
	{
		s += p->val;
		if (mp.count(s - tar))
			ans += mp[s - tar];

		mp[s]++;

		if (p->left)
			dfs(p->left, s);

		if (p->right)
			dfs(p->right, s);

		mp[s]--;
		s -= p->val;
	}

	public:
	int pathSum(TreeNode* root, int sum) {
		if (!root) return 0;
		tar = sum;
		ans = 0;
		mp[0] = 1;
		dfs(root, 0);
		return ans;
	}
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(10);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(10);
	r2->left = new TreeNode(5);
	r2->right = new TreeNode(-3);
	r2->right->right = new TreeNode(11);
	r2->left->left = new TreeNode(3);
	// r2->left->left->left = new TreeNode(3);
	r2->left->left->left = new TreeNode(0);
	r2->left->left->right = new TreeNode(-2);
	r2->left->right = new TreeNode(2);
	r2->left->right->right = new TreeNode(1);

	print_tree(r2);
	cout << mysol.pathSum(r2,8) << endl;

	return 0;
}
