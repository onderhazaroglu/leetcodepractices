#include <iostream>
#include <string>
#include <unordered_map>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

int main()
{
	TreeNode* r1 = new TreeNode(1);
	cout << r1 << endl;
	
	return 0;
}

