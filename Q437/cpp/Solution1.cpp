#include <iostream>
#include <string>
#include <unordered_map>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		int countPaths(TreeNode* root, int sum);
		int pathSum(TreeNode* root, int sum);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(10);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(10);
	r2->left = new TreeNode(5);
	r2->right = new TreeNode(-3);
	r2->right->right = new TreeNode(11);
	r2->left->left = new TreeNode(3);
	// r2->left->left->left = new TreeNode(3);
	r2->left->left->left = new TreeNode(0);
	r2->left->left->right = new TreeNode(-2);
	r2->left->right = new TreeNode(2);
	r2->left->right->right = new TreeNode(1);

	print_tree(r2);
	cout << mysol.pathSum(r2,8) << endl;

	return 0;
}

int Solution::countPaths(TreeNode* root, int sum)
{
	return (sum-root->val==0?1:0) + (root->left?countPaths(root->left,sum-root->val):0) + (root->right?countPaths(root->right,sum-root->val):0);
}

int Solution::pathSum(TreeNode* root, int sum)
{
	if(!root) return 0;
	return countPaths(root,sum) + pathSum(root->left,sum) + pathSum(root->right,sum);
}
