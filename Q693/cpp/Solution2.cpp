#include <iostream>
#include <vector>
#include <bitset>
using namespace std;

class Solution
{
	public:
		bool hasAlternatingBits(int n);
};

int main()
{
	Solution mysol;
	int num1;
	cout << "Enter number 1: ";
	cin >> num1;
	if(mysol.hasAlternatingBits(num1)) cout << num1 << " has alternating bits." << endl;
	else cout << num1 << " doesn't have alternating bits!" << endl;
	return 0;
}

bool Solution::hasAlternatingBits(int n)
{
	n = n ^ (n >> 1);
	return bool(!(n&(n+1)));
}
