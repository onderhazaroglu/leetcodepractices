#include <iostream>
#include <vector>
#include <bitset>
using namespace std;

class Solution
{
	public:
		bool hasAlternatingBits(int n);
};

int main()
{
	Solution mysol;
	int num1;
	cout << "Enter number 1: ";
	cin >> num1;
	if(mysol.hasAlternatingBits(num1)) cout << num1 << " has alternating bits." << endl;
	else cout << num1 << " doesn't have alternating bits!" << endl;
	return 0;
}

bool Solution::hasAlternatingBits(int n)
{
	bitset<32> b(n);
	cout << "bits:" << endl;
	for(unsigned int j=0;j<b.size();j++) cout << b[j] << endl;
	cout << "end" << endl;
	bool res = true;
	cout << b[0] << " ";
	for(unsigned int i=1;i<b.size();i++) { cout << b[i] << " " << endl; if(b[i] == b[i-1]) return false; }
	return res;
}
