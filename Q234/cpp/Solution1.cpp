#include <iostream>
using namespace std;

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(NULL) {}
};

void print_listnode(ListNode* head)
{
	ListNode* temp = head;

	while(temp != NULL)
	{
		cout << temp->val;
		temp = temp->next;
		if(temp != NULL) cout << "->";
		else cout << endl;
	}
}

class Solution
{
	public:
		bool isPalindrome(ListNode* head);
};

int main()
{
	Solution mysol;
	int val;
	cout << "Enter node: ";
	cin >> val;

	ListNode* head = new ListNode(val);
	ListNode* temp = head;

	while(val != -1000)
	{
		cout << "Enter node: ";
		cin >> val;
		if(val != -1000) temp->next = new ListNode(val);
		temp = temp->next;
	}
	print_listnode(head);
	cout << mysol.isPalindrome(head) << endl;

	return 0;
}

bool Solution::isPalindrome(ListNode* head)
{
	if(!head||!head->next) return true;
	ListNode* t = head->next;
	ListNode* p = head;
	ListNode* head2, *hp;

	int count = 1;
	while(t)
	{
		cout << "head2: " << (head2?head2->val:-1000) << ", hp: " << (hp?hp->val:-1000) << endl;
		cout << "p: " << p->val << ", t: " << t->val << endl;
		count++;
		hp = head2;
		cout << "hp: " << hp->val << ", h: " << head2->val << endl;
		head2 = new ListNode(t->val);
		head2->next = hp;
		// head2->next = new ListNode(p->val);

		cout << "hp: " << hp->val << ", h: " << head2->val << endl;
		p = t;
		t = t->next;
		print_listnode(head2);
	}

	ListNode* t1 = head;
	ListNode* t2 = head2;

	cout << "count: " << count << endl;
	for(int i=0;i<count/2;i++)
	{
		cout << "t1: " << t1->val << ", t2: " << t2->val << endl;
		if(t1->val!=t2->val) return false;
		t1 = t1->next;
		t2 = t2->next;
	}
	return true;
}
