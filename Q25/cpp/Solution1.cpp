#include <iostream>
using namespace std;

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(NULL) {}
};

void print_listnode(ListNode* head)
{
	ListNode* temp = head;

	while(temp != NULL)
	{
		cout << temp->val;
		temp = temp->next;
		if(temp != NULL) cout << "->";
		else cout << endl;
	}
}

class Solution
{
	public:
		ListNode* reverseKGroup(ListNode* head, int k);
};

int main()
{
	Solution mysol;
	int val;
	cout << "Enter node: ";
	cin >> val;

	ListNode* head = new ListNode(val);
	ListNode* temp = head;

	while(val != -1)
	{
		cout << "Enter node: ";
		cin >> val;
		if(val != -1) temp->next = new ListNode(val);
		temp = temp->next;
	}
	int k;
	cout << "Enter k: ";
	cin >> k;

	// print_listnode(head);
	print_listnode(mysol.reverseKGroup(head,k));

	return 0;
}

ListNode* Solution::reverseKGroup(ListNode* head, int k)
{
	cout << "reversing group for head: " << head->val << endl;
	ListNode* temp = head;
	ListNode* t = head;
	ListNode* th = head;
	ListNode* res = th;
	ListNode* tnn = NULL;

	int num = 0;
	while(temp != NULL)
	{
		num++;
		temp = temp->next;
	}
	if(num<k) return head;

	int s = 1;

	while(s<k && t != NULL && t->next != NULL)
	{
		tnn = t->next->next;
		t->next->next = th;
		th = t->next;
		t->next = tnn;
		s++;
		cout << "s: " << s << ", k: " << k << endl;

		print_listnode(res);
		cout << "-------------" << endl;
	}
	if(s==k) res = th;
	cout << "Group end. res head: " << res->val << endl;
	print_listnode(res);
	if(t != NULL && t->next != NULL) t->next = reverseKGroup(t->next,k);

	return res;
}
