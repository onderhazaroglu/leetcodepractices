#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		bool isMatch(string s, string p);
};

int main()
{
	Solution mysol;
	string s,p;
	cout << "Enter string s: ";
	cin >> s;
	cout << "Enter string p: ";
	cin >> p;

	if(mysol.isMatch(s,p)) cout << s << " matches " << p << endl;
	else cout << s << " does NOT match " << p << endl;

	return 0;
}

bool Solution::isMatch(string s, string p)
{
	unsigned int sl = s.length(), pl = p.length();
	bool first_match = s[0] == p[0] || p[0] == '.';
	return first_match && isMatch(s.substr(1,sl-1),p.substr(1,pl-1));
}
