#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		bool isMatch(string s, string p);
};

int main()
{
	Solution mysol;
	string s,p;
	cout << "Enter string s: ";
	cin >> s;
	cout << "Enter string p: ";
	cin >> p;

	if(mysol.isMatch(s,p)) cout << s << " matches " << p << endl;
	else cout << s << " does NOT match " << p << endl;

	return 0;
}

bool Solution::isMatch(string s, string p)
{
	unsigned i=0, j=0;
	while(p[j]!='\0' || s[i]!='\0')
	{
		cout << "matching " << s[i] << "(" << i << ") <-> ";
		if(p[j+1]=='*') cout << p[j] << p[j+1] << "(" << j << "-" << j+1 << ")" << endl;
		else cout << p[j] << "(" << j << ")" << endl;

		if(p[j]=='.')
		{
			if(p[j+1]=='*' && p[j+1]=='\0') { cout << "A1" << endl; return true; }
			else if(p[j+1]=='*' && p[j+1]!='\0')
			{
				cout << "A2-1" << endl;
				while(p[j+1]=='.')
				{
					while(p[j+1]=='.') j++;
					if(p[j+1]=='*') j++;
				}
				while(s[i]!='\0' && s[i]!=p[j]) { cout << "s[i] <-> p[j] : " << s[i] << " <-> " << p[j] << endl; i++; }
				if(s[i]=='\0') { cout << "A2-2" << endl; return false; }
			}
			else { cout << "A3" << endl; i++; j++; }
		}
		else
		{
			if(p[j+1]=='*')
			{
				cout << "B" << endl;
				while(s[i]==p[j]) i++;
				j+=2;
			}
			else
			{
				if(s[i]==p[j]) { i++; j++; cout << "C" << endl; }
				else { cout << "D" << endl; return false; }
			}
		}
	}
	return true;
}
