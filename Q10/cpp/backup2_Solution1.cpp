#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool checkMatch(string st, unsigned int si, string pt, unsigned int pi, vector<vector<int>> &ms);
		bool isMatch(string s, string p);
};

int main()
{
	Solution mysol;
	string s,p;
	cout << "Enter string s: ";
	cin >> s;
	cout << "Enter string p: ";
	cin >> p;

	if(mysol.isMatch(s,p)) cout << s << " matches " << p << endl;
	else cout << s << " does NOT match " << p << endl;

	return 0;
}

bool Solution::checkMatch(string s, unsigned int si, string p, unsigned int pi, vector<vector<int>> &ms)
{
	unsigned int sl = s.length(), pl = p.length();
	string st = s.substr(si,sl-si), pt = p.substr(pi,pl-pi);

	cout << "matching " << st << " <-> " << pt << endl;

	if(pt.empty()) return st.empty();

	cout << "\tfirst match: " << st[0] << " <-> " << pt[0];
	bool first_match = !st.empty() && (pt[0]==st[0] || pt[0]=='.');
	if(first_match) cout << " (OK)" << endl;
	else cout << " (NOT)" << endl;
	cout << "A" << endl;

	if(pt.length()>=2 && pt[1]=='*')
	{
		cout << "B1" << endl;
		ms[si][pi] = ms[si][pi] > 0 ? ms[si][pi] : checkMatch(s,si,p,pi+2,ms) || (first_match && checkMatch(s,si+1,p,pi,ms));
	}
	else
	{
		cout << "B2" << endl;
		ms[si][pi] = ms[si][pi] > 0 ? ms[si][pi] : first_match && checkMatch(s,si+1,p,pi+1,ms);
	}
	cout << "C" << endl;
	return ms[si][pi];
}

bool Solution::isMatch(string s, string p)
{
	if(p.empty()) return s.empty();
	vector<vector<int>> ms(s.length()+1, vector<int> (p.length()+1, -1));

	return checkMatch(s,0,p,0,ms);

	/*
	cout << "\tfirst match: " << s[0] << " <-> " << p[0] << endl;
	bool first_match = !s.empty() && (p[0]==s[0] || p[0]=='.');

	if(p.length()>=2 && p[1]=='*')
	{
		ms[si][pi] = isMatch(s,p.substr(2,p.length()-2)) || (first_match && isMatch(s.substr(1,s.length()-1),p));
	}
	else
	{
		ms[si][pi] = first_match && isMatch(s.substr(1,s.length()-1),p.substr(1,p.length()-1));
	}
	return ms[si][pi];
	*/
}
