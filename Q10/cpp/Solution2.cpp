#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<vector<int>> ms;
		bool checkMatch(string s, unsigned int i,string p, unsigned int j);
		bool isMatch(string s, string p);
};

int main()
{
	Solution mysol;
	string s,p;
	cout << "Enter string s: ";
	cin >> s;
	cout << "Enter string p: ";
	cin >> p;

	if(mysol.isMatch(s,p)) cout << s << " matches " << p << endl;
	else cout << s << " does NOT match " << p << endl;

	return 0;
}

bool Solution::checkMatch(string s, unsigned int i,string p, unsigned int j)
{
	unsigned int sl = s.length(), pl = p.length();
	if(ms[i][j]<0)
	{
		cout << "A" << endl;
		bool ans, fm;
		if(j == p.length()) ans = i == s.length();
		else
		{
			cout << "B" << endl;
			fm = i < s.length() && (p[j]==s[i] || p[j]=='.');
			if(j+1<p.length() && p[j+1]=='*') ans = (checkMatch(s,i,p.substr(2,pl-2),j+2) || fm) && checkMatch(s.substr(1,sl-1),i+1,p,j);
			else ans = fm && checkMatch(s.substr(1,sl-1),i+1, p.substr(1,pl-1),j+1);
		}
		ms[i][j] = ans;
		cout << "C" << endl;
	}
	return ms[i][j];
}


bool Solution::isMatch(string s, string p)
{
	if(p.empty()) return s.empty();
	vector<vector<int>> ms(s.length()+1, vector<int> (p.length()+1, -1));

	return checkMatch(s,0,p,0);
}
