#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		string toHex(int num);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter number 1: ";
	cin >> num;
	cout << mysol.toHex(num) << endl;
	return 0;
}

string Solution::toHex(int num)
{
	string res;
	while(num)
	{
		int rem = num % 16;
		res.push_back( char(rem<10?rem+48:rem+87) );
		num /= 16;
	}
	return string(res.rbegin(),res.rend());
}
