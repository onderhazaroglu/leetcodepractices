#include <iostream>
#include <string>
using namespace std;

const string HEX = "0123456789abcdef";

class Solution
{
	const char hex[16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
	public:
		string toHex(int num);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter number 1: ";
	cin >> num;
	cout << mysol.toHex(num) << endl;
	return 0;
}

string Solution::toHex(int num)
{
	if(num==0) return "0";
	string res;
	unsigned int n = num;
	while(n)
	{
		res = hex[n%16] + res;
		n /= 16;
	}
	return res;
}
