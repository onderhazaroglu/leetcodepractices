#include <iostream>
#include <string>
using namespace std;

const string HEX = "0123456789abcdef";

class Solution
{
	public:
		string toHex(int num);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter number 1: ";
	cin >> num;
	cout << mysol.toHex(num) << endl;
	return 0;
}

string Solution::toHex(int num)
{
	cout << "num: " << num << endl;
	if(num==0) return "0";
	int count = 0;
	string result;
	while(num && count++ < 8)
	{
		cout << "nun: " << num << ", res: " << result << endl;
		result = HEX[(num & 0xf)] + result;
		num >>= 4;
	}
	return result;
}
