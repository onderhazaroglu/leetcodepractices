#include <iostream>
#include <set>
using namespace std;

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(NULL) {}
};

void print_linkedlist(ListNode* head)
{
	ListNode* temp = head;

	while(temp != NULL)
	{
		cout << temp->val;
		temp = temp->next;
		if(temp != NULL) cout << "->";
		else cout << endl;
	}
}

class Solution
{
	public:
		ListNode* getIntersectionNode(ListNode* headA, ListNode* headB);
};

int main()
{
	Solution mysol;
	int val;
	cout << "Enter node: ";
	cin >> val;

	ListNode* headA = new ListNode(val);
	ListNode* temp = headA;

	ListNode* intersection;

	while(val != -1)
	{
		cout << "Enter headA node: ";
		cin >> val;
		if(val != -1) temp->next = new ListNode(val);
		if(val == 4) intersection = temp->next;
		temp = temp->next;
	}

	cout << "Enter node: ";
	cin >> val;

	ListNode* headB = new ListNode(val);
	temp = headB;

	while(val != -1)
	{
		cout << "Enter headB node: ";
		cin >> val;
		if(val != -1)
		{
			if(val==4) { temp->next = intersection; val = -1; }
			else temp->next = new ListNode(val);
		}
		temp = temp->next;
	}

	print_linkedlist(headA);
	print_linkedlist(headB);
	print_linkedlist(mysol.getIntersectionNode(headA,headB));

	return 0;
}

ListNode* Solution::getIntersectionNode(ListNode* headA, ListNode* headB)
{
	set<ListNode*> s;
	ListNode *tA=headA, *tB=headB;
	while(tA)
	{
		s.insert(tA);
		tA=tA->next;
	}

	while(tB)
	{
		if(s.find(tB)!=s.end()) return tB;
		tB=tB->next;
	}
	return NULL;
}
