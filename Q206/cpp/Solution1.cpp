#include <iostream>
using namespace std;

struct ListNode
{
	int val;
	ListNode *next;
	ListNode(): next(NULL) {}
	ListNode(int x): val(x), next(NULL) {}
};

class Solution
{
	public:
		ListNode* reverseList(ListNode* head);
		ListNode* mergeTwoLists(ListNode* l1, ListNode* l2);
		ListNode* mergeTwoLists2(ListNode* l1, ListNode* l2);
};

void print_list( ListNode* head )
{
	cout << "print_list1: " << endl;
	ListNode* temp = head;
	if ( temp == NULL )
	{
		cout << "List doesn't have a node." << endl;
		return;
	}

	do
	{
		cout << temp->val;
		if ( temp->next != NULL )
		{
			cout << "->";
		}
		temp = temp->next;
	}
	while ( temp != NULL );
	cout << endl;
}

void print_list2( ListNode* head )
{
	cout << "print_list2: " << endl;
	ListNode* temp = head;
	if ( temp == NULL )
	{
		cout << "List doesn't have a node." << endl;
		return;
	}

	do
	{
		cout << temp->val;
		if ( temp->next != NULL )
		{
			cout << "->";
		}
		temp = temp->next;
	}
	while ( temp != NULL );
	cout << endl;
}


int main()
{
	ListNode* head1 = NULL;
	ListNode* temp1 = NULL;
	int val = 0, i=0;

	while( val >= 0 )
	{
		i++;
		cout << "Enter " << i << ". element or -1 to quit: ";
		cin >> val;
		if ( val == -1 ) break;
		else if ( i == 1 )
		{
			head1 = new ListNode(val);
			temp1 = head1;
		}
		else { temp1->next = new ListNode(val); temp1 = temp1->next; }
	}

	Solution mysol;
	ListNode* rl = mysol.reverseList( head1 );
	cout << "A" << endl;

	print_list( rl );
	print_list2( rl );

	return 0;
}


ListNode* Solution::reverseList(ListNode* head)
{
	ListNode* temp = head;

	if(temp == NULL || temp->next == NULL) return temp;

	ListNode* p = NULL;
	ListNode* n = temp->next;

	while(temp != NULL)
	{
		cout << "Head: " << temp->val << endl;
		temp->next = p;
		p = temp;
		temp = n;
		if(n != NULL) n = n->next;
	}

	temp = p;
	return temp;
}
