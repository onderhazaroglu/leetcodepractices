#include <iostream>
#include <vector>
#include <climits>
using namespace std;

class Solution
{
	public:
		int missingNumber(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,3,4,2,5,7,8,0};
	cout << mysol.missingNumber(nums) << endl;
	return 0;
}

int Solution::missingNumber(vector<int>& nums)
{
	int miss = 0, i = 0, size = nums.size();
	for(i=0;i<size;i++)
	{
		miss ^= i ^ nums[i];
	}

	miss ^= i;
	return miss;
}
