#include <iostream>
#include <vector>
#include <climits>
using namespace std;

class Solution
{
	public:
		int missingNumber(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,3,4,2,5,7,8,0};
	cout << mysol.missingNumber(nums) << endl;
	return 0;
}

int Solution::missingNumber(vector<int>& nums)
{
	int total_sum = 0, max = INT_MIN;
	int cur_sum = 0;
	int size = nums.size();
	bool found_zero = false;

	for(int i=0;i<size;i++)
	{
		if(nums[i]==0) found_zero = true;
		cur_sum += nums[i];
		if(nums[i]>max) max = nums[i];
		total_sum = max*(max+1)/2;
	}
	return (found_zero&&total_sum-cur_sum==0)?max+1:total_sum - cur_sum;
}
