#include <iostream>
#include <cmath>
#include <vector>
using namespace std;

class Solution {
	public:
		int reverse(int x);
};

int main() {

	// cout << 2147483647 + 1 << endl;

	int x = -3214, y = 543;
	cout << x % 10 << endl
		<< y % 10 << endl;

	int num = 0;
	cout << "Enter int: ";
	cin >> num;

	Solution sol1;
	int result = sol1.reverse(num);
	cout << "Result: " << result << endl;

	return 0;
}


int Solution::reverse(int x) {

	if(x<10 && x>-10) return x;
	vector<int> dvec;

	int anum = abs(x), ndigits = 0, temp = x;

	ndigits = static_cast<int>( log10 (anum) ) +1;

	cout << "ndigits: " << ndigits << endl;

	int k = 0, d = 0, nn = 0;
	vector<int>::iterator it;

	for(int i=1;i<=ndigits;i++) {
		cout << "For i=" << i << ", digit: ";
		d = temp % 10;
		temp /= 10;

		cout << d << endl;
		it = dvec.begin();
		dvec.insert(it, d);
		cout << "test\n" << nn <<
			nn + d * pow(10, ndigits -k-1) << endl;

		if( d>0 && nn + d * pow(10, ndigits -k-1) > 2147483648 ) return 0;
		if( d<0 && nn + d * pow(10, ndigits -k-1) < -2147483648 ) return 0;

		nn += d * pow(10, ndigits - ++k);
		cout << "nn: " << nn << endl;
	}

	cout << nn << endl;
	return nn;
}
