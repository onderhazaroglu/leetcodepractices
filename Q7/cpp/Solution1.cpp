#include <iostream>
#include <cmath>
#include <vector>
using namespace std;

class Solution {
	public:
		int reverse(int x);
};

int main() {

	int x = -3214, y = 543;
	cout << x % 10 << endl
		<< y % 10 << endl;

	int num = 0;
	cout << "Enter int: ";
	cin >> num;

	Solution sol1;
	int result = sol1.reverse(num);
	cout << "Result: " << result << endl;

	return 0;
}


int Solution::reverse(int num) {
	vector<int> dvec;
	vector<int> steps;

	int anum = abs(num), ndigits = 0;

	/*
	while(temp > 1) {
		temp /= 10;
		ndigits++;
	}*/

	ndigits = static_cast<int>( log10 (anum) ) +1;

	int rem = 0, k = 0, d = 0, nn = 0;
	vector<int>::iterator it;

	for(int i=10;i<anum*10;i*=10) {
		k++;
		cout << "For i=" << i << ", digit: ";
		d = ( (num-rem) % i ) / ( i / 10);

		cout << d << endl;
		it = dvec.begin();
		dvec.insert(it, d);
		steps.push_back(i);
		nn += d * pow(10, ndigits -k);
		cout << "nn: " << nn << endl;
	}

	int newNum = 0;

	for(int j=0;j<k;j++) {
		cout << dvec[j] << endl;
		newNum += dvec[j] * steps[j]/10;
	}

	cout << newNum << endl;
	cout << nn << endl;
	return nn;
}
