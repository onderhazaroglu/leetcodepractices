#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool repeatedSubstring(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.repeatedSubstring(s) << endl;
	return 0;
}

bool Solution::repeatedSubstring(string s)
{
	if(s.empty()) return false;
	int i=1, len = s.length();
	string c;
	c.push_back(s[0]);

	while(i<=len/2)
	{
		cout << "i:" << i << "/" << len << endl;
		cout << "s[i]:" << s[i];
		int clen = c.length();
		if(s[i]==c[0] && (len-i)%clen == 0)
		{
			cout << ", c: " << c << endl;
			// string tc = c;
			// tc.push_back(s[i]);
			// cout << ", tc: " << tc << endl;
			bool mss = true;
			for(int j=i+1;j<len;j++)
			{
				if(s[j]!=c[(j-i)%clen]) { cout << "s[j=" << j << "](" << s[j] << ") <> c[" << (j-i)%clen << "](" << c[(j-i)%clen] << ")" << endl; mss = false; break; }
				else cout << "s[j=" << j << "](" << s[j] << ") == c[" << (j-i)%clen << "](" << c[(j-i)%clen] << ")" << endl;
			}
			if(mss) return mss;
		}
		else cout << endl;
		c.push_back(s[i]);
		i++;
	}
	return false;
}
