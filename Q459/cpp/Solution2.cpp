#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool repeatedSubstring(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.repeatedSubstring(s) << endl;
	return 0;
}

bool Solution::repeatedSubstring(string s)
{
	vector<int> dp(s.length(), 0);
	int l = 0, r = 1;

	while(r < int(s.length()))
	{
		cout << "in-> l:" << l << "(" << s[l] << "), r:" << r << "(" << s[r] << ")" << endl;
		for(int k=0;k<int(dp.size());k++) cout << "dp[" << k << "]:" << dp[k] << ", ";
		cout << endl;

		if(s[r] == s[l])
			dp[r++] = ++l;
		else if(l == 0)
			r++;
		else
			l = dp[l - 1];

		cout << "out-> l:" << l << "(" << s[l] << "), r:" << r << "(" << s[r] << ")" << endl;
		for(int k=0;k<int(dp.size());k++) cout << "dp[" << k << "]:" << dp[k] << ", ";
		cout << endl << endl;
	}

	return dp[r - 1] && !(s.length() % (s.length() - dp[r - 1]));
}
