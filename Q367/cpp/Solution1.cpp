#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Solution
{
	public:
		bool isPerfectSquare(int num);
};

int main()
{
	Solution mysol;
	int num;
	cin >> num;
	cout << mysol.isPerfectSquare(num) << endl;
	return 0;
}

bool Solution::isPerfectSquare(int num)
{
	if(num==1||num==4||num==9) return true;
	int nd = 0, l = 1, r = 1, temp = num;
	while(temp>0) { nd++; temp /= 10; }

	for(int i=1;i<=(nd+1)/2;i++) r*= 10;
	for(int i=1;i<=(nd-1)/2;i++) l*= 10;

	cout << "l: " << l << ", r: " << r << endl;

	while(l<r)
	{
		int mid = (l + r)/2;
		cout << "l: " << l << ", r: " << r << ", mid: " << mid << endl;
		if(num*1.0/l==double(l)||num*1.0/mid==double(mid)||num*1.0/r==double(r)) return true;
		else if(num*1.0/r>r) return false;
		else if(mid<num*1.0/mid) l = mid+1;
		else r = mid-1;
	}
	return false;
}
