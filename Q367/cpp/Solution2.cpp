#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Solution
{
	public:
		bool isPerfectSquare(int num);
};

int main()
{
	Solution mysol;
	int num;
	cin >> num;
	cout << mysol.isPerfectSquare(num) << endl;
	return 0;
}

bool Solution::isPerfectSquare(int num)
{
	long l=1,r=num;
	while(l<=r){
		long mid = l+(r-l)/2;
		long t = mid*mid;
		if(t > num) r=mid-1;
		else if(t < num) l=mid+1;
		else return true;
	}
	return false;
}
