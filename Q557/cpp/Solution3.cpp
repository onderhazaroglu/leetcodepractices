#include <iostream>
#include <sstream>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		string reverseWords(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter a string:";
	getline(cin,s);

	cout << mysol.reverseWords(s) << endl;
	return 0;
}

string Solution::reverseWords(string s)
{
	size_t front = 0;
	for(size_t i = 0;i <= s.length(); i++)
	{
		if(i == s.length() || s[i] == ' ')
		{
			cout << "s[" << i << "]:[" << s[i] << "]" << endl;
			cout << "[front,i]:[" << front << "," << i << "]" << ":[" << s[front] << "," << s[i] << "]" << endl;
			reverse(&s[front], &s[i]);
			front = i + 1;
		}
	}
	return s;
}
