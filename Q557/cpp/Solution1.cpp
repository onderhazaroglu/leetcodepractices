#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		string reverseWords(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter a string:";
	getline(cin,s);

	cout << mysol.reverseWords(s) << endl;
	return 0;
}

string Solution::reverseWords(string s)
{
	string delim = " ", word = "", res = "";
	size_t prev = 0, cur = 0;

	do
	{
		cur = s.find(delim,prev);
		word = s.substr(prev, cur-prev);
		string reversed(word.rbegin(),word.rend());
		cout << reversed << endl;
		if (res != "") res += " ";
		res += reversed;
		prev = cur+1;
	}
	while(cur != string::npos);
	return res;
}
