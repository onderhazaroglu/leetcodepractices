#include <iostream>
#include <sstream>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		string reverseWords(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter a string:";
	getline(cin,s);

	cout << mysol.reverseWords(s) << endl;
	return 0;
}

string Solution::reverseWords(string s)
{
	stringstream ss(s);
	const char delim = ' ';
	string word = "", res;

	while(getline(ss,word,delim))
	{
		reverse(word.rbegin(),word.rend());
		if(!res.empty()) res += " "+word;
		else res += word;
	}
	return res;
}
