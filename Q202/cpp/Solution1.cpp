#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		bool isHappy(int n);
};

int main()
{
	Solution mysol;
	int n;
	cout << "Enter number 1: ";
	cin >> n;
	cout << mysol.isHappy(n) << endl;
	return 0;
}

bool Solution::isHappy(int n)
{
	unordered_map<int,int> map;
	while(n!=1 && map.find(n)==map.end())
	{
		map[n] = 1;
		// cout << "n: " << n << endl;
		int k = n, sum = 0;
		while(k>0)
		{
			sum += (k%10)*(k%10);
			k /= 10;
		}

		if(sum==1) return true;
		else
		{
			// cout << "sum: " << sum << endl;
			n = sum;
		}
	}
	return n==1?true:false;
}
