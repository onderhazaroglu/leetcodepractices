#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int maximumPRoduct(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {1,3,4,2,1,3,4};
	cout << mysol.maximumPRoduct(nums) << endl;
	return 0;
}

int Solution::maximumPRoduct(vector<int>& nums)
{
	int size = nums.size();
	sort(nums.begin(),nums.end());
	return max(nums[size-1]*nums[size-2]*nums[size-3], nums[0]*nums[1]*nums[size-1]);
}
