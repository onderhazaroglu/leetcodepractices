#include <iostream>
#include <climits>
#include <cstdlib>
using namespace std;

class Solution
{
	public:
		int divide(int dividend, int divisor);
};

int main()
{
	Solution mysol;

	cout << INT_MAX << endl;
	int dividend, divisor;

	cout << "Enter dividend: ";
	cin >> dividend;
	cout << "Enter divisor: ";
	cin >> divisor;

	cout << mysol.divide(dividend, divisor) << endl;

	return 0;
}

int Solution::divide(int dividend, int divisor)
{
	if ( !divisor || (dividend == INT_MIN && divisor == -1) ) return INT_MAX;

	int sign = ((dividend < 0) ^ (divisor < 0)) ? -1 : 1;

	long long dvd = labs( dividend );
	long long dvs = labs( divisor );
	int res = 0;

	while( dvd >= dvs )
	{
		long long temp = dvs, mt = 1;
		while( dvd >= (temp << 1) )
		{
			temp <<= 1;
			mt <<= 1;
		}
		dvd -= temp;
		res += mt;
	}
	return sign == 1 ? res : -res;
}
