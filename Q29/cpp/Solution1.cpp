#include <iostream>
#include <climits>
#include <cstdlib>
using namespace std;

class Solution
{
	public:
		int divide(int dividend, int divisor);
};

int main()
{
	Solution mysol;

	cout << INT_MAX << endl;
	int dividend, divisor;

	cout << "Enter dividend: ";
	cin >> dividend;
	cout << "Enter divisor: ";
	cin >> divisor;

	cout << mysol.divide(dividend, divisor) << endl;

	return 0;
}

int Solution::divide(int dividend, int divisor)
{
	if ( !divisor || (dividend == INT_MIN && divisor == -1) ) return INT_MAX;

	bool neg = false;
	if ( (dividend < 0 && divisor > 0) || (dividend > 0 && divisor < 0) ) neg = true;

	int i=0;
	dividend = abs( dividend );
	divisor = abs( divisor );
	while(dividend >= divisor)
	{
		dividend -= divisor;
		i++;
	}
	return neg ? -i : i;
}
