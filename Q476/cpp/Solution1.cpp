#include <iostream>
using namespace std;

class Solution
{
	public:
		int findComplement(int num);
};

int main()
{
	Solution mysol;

	cout << ~5 << endl;
	cout << int(5^1) << endl;
	cout << unsigned(~0) << endl;
	cout << endl;
	cout << mysol.findComplement( 5 ) << endl;
	return 0;
}

int Solution::findComplement(int num)
{
	unsigned mask = ~0;
	while( mask & num ) mask <<= 1;
	return num^(~mask);
}
