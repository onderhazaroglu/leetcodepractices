#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << " )" << endl;
	}
}


class Solution {
	public:
		int maxdiameter = 0;
		int diameterOfBinaryTree(TreeNode* root);
		int diameter(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	TreeNode* root = new TreeNode(1);
	root->left = new TreeNode(2);
	root->left->left = new TreeNode(3);
	root->left->left->left = new TreeNode(4);
	root->left->left->left->left = new TreeNode(5);
	root->left->left->left->left->left = new TreeNode(6);

	root->left->left->left->right = new TreeNode(10);
	root->left->left->left->right->right = new TreeNode(11);
	root->left->left->left->right->right->right = new TreeNode(12);
	root->left->left->left->right->right->right->right = new TreeNode(13);
	root->left->left->left->right->right->right->right->right = new TreeNode(17);

	root->left->left->right = new TreeNode(8);
	root->left->left->right->right = new TreeNode(9);
	root->left->left->right->right->right = new TreeNode(14);
	root->left->left->right->right->right->right = new TreeNode(15);
	root->left->left->right->right->right->right->right = new TreeNode(16);

	print_tree(root);

	cout << mysol.diameterOfBinaryTree(root) << endl;

	return 0;
}

int Solution::diameterOfBinaryTree(TreeNode* root)
{
	diameter(root);
	return maxdiameter;
}

int Solution::diameter(TreeNode* root)
{
	if(!root || (!root->left && !root->right)) {cout << "Node: NULL" << endl; return 0; }
	if(!root->left)
	{
		cout << "Node: " << root->val << endl;
		int d = diameter(root->right);
		if(1+d>maxdiameter) maxdiameter=1+d;
		cout << "Node " << root->val << " val: " << 1+d << endl;
		return 1+d;
	}
	if(!root->right)
	{
		cout << "Node: " << root->val << endl;
		int d = diameter(root->left);
		if(1+d>maxdiameter) maxdiameter=1+d;
		cout << "Node " << root->val << " val: " << 1+d << endl;
		return 1+d;
	}

	cout << "Node: " << root->val << endl;

	int l = diameter(root->left);
	cout << "l: " << l << endl;
	int r = diameter(root->right);
	cout << "r: " << r << endl;

	if(l+r+2>maxdiameter) maxdiameter=l+r+2;
	int ret = max(l,r);

	cout << "Node " << root->val << " val: " << 1+ret << endl;
	return ret+1;
}
