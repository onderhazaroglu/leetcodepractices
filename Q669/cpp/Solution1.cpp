#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}

class Solution
{
	public:
		TreeNode* trimBST(TreeNode* root, int L, int R);
};

int main()
{
	Solution mysol;
	/*
	TreeNode* root = new TreeNode(3);
	root->left = new TreeNode(0);
	root->right = new TreeNode(4);
	root->left->right = new TreeNode(2);
	root->left->right->left = new TreeNode(1);
	*/

	TreeNode* root = new TreeNode(5);
	root->left = new TreeNode(4);
	root->right = new TreeNode(6);
	root->left->left = new TreeNode(2);
	root->left->left->left = new TreeNode(1);
	root->left->left->right = new TreeNode(3);

	TreeNode* res = mysol.trimBST(root,2,3);
	cout << res->val << endl;

	print_tree( mysol.trimBST(root,1,3) );

	return 0;
}

TreeNode* Solution::trimBST(TreeNode* root, int L, int R)
{
	if(root == NULL) return root;
	else if(root->val > R) return trimBST(root->left,L,R);
	else if(root->val < L) return trimBST(root->right,L,R);
	else
	{
		root->left = trimBST(root->left,L,R);
		root->right = trimBST(root->right,L,R);
	}
	return root;
}
