#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int maxSubArray(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {-2,1,-3,4,-1,2,1,-5,4};
	cout << mysol.maxSubArray(nums) << endl;
	return 0;
}

int Solution::maxSubArray(vector<int>& nums)
{
	int tempMax = nums[0], maxFinal = nums[0];
	for(unsigned int i=1;i<nums.size();i++)
	{
		tempMax = max(tempMax+nums[i],nums[i]);
		maxFinal = max(tempMax,maxFinal);
	}
	return maxFinal;
}
