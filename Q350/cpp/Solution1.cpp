#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		vector<int> intersect(vector<int>& nums1, vector<int>& nums2);
};

int main()
{
	Solution mysol;
	vector<int> nums1 = {1,3,4,2,1,3,4,2,2,2};
	vector<int> nums2 = {4,2,3,2,5,3,3};
	for(auto i: mysol.intersect(nums1,nums2)) cout << i << endl;
	return 0;
}

vector<int> Solution::intersect(vector<int>& nums1, vector<int>& nums2)
{
	if(nums1.size()==0 || nums2.size()==0) return vector<int>();
	vector<int> res;
	unordered_map<int,int> map1, map2;

	for(auto i: nums1) map1[i]++;
	for(auto i: nums2) map2[i]++;

	for(auto t: map1)
	{
		do
		{
			if(map2[t.first]>0)
			{
				res.push_back(t.first);
				map1[t.first]--;
				map2[t.first]--;
			}
		}
		while(map2[t.first]>0 && map1[t.first]>0);
	}
	return res;
}
