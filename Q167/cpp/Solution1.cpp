#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<int> twoSum(vector<int>& numbers, int target);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {-3,-3,-3,-3,-3,-3,-3,-1,-1,-1,-1,-1,-1,0,1,3,4,8,8,8,8,8,8,8,10,10,10,10,10,10};
	vector<int> nums = {0,0,3,4};
	for(int i: nums)
	{
		cout << i << ", ";
	}
	cout << endl;

	for(int i: mysol.twoSum(nums, 0))
	{
		cout << i << "(" << nums[i] << "), ";
	}
	cout << endl;
	return 0;
}

vector<int> Solution::twoSum(vector<int>& numbers, int target)
{
	int i = 0, size = numbers.size()-1, sum = 0, j = size;
	for(i = 0;i<size;)
	{
		while(i<j)
		{
			cout << "i: " << numbers[i] << ", j:" << numbers[j] << endl;
			sum = numbers[i]+numbers[j];
			if(sum == target) return vector<int> {i, j};
			else if(sum<target) { j = size; do {i++;} while(numbers[i]==numbers[i-1]) ; break; }
			else { do {j--;} while(numbers[j] == numbers[j+1]); cout << "new j: " << j << "(" << numbers[j] << ")" << endl; }
		}
	}
	return vector<int> {};
}
