#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		vector<int> twoSum(vector<int>& numbers, int target);
};

int main()
{
	Solution mysol;
	vector<int> nums = {-3,-3,-3,-3,-3,-3,-3,-1,-1,-1,-1,-1,-1,0,1,3,4,8,8,8,8,8,8,8,10,10,10,10,10,10};
	// vector<int> nums = {0,0,3,4};
	for(int i: nums)
	{
		cout << i << ", ";
	}
	cout << endl;

	for(int i: mysol.twoSum(nums, 0))
	{
		cout << i << "(" << nums[i] << "), ";
	}
	cout << endl;
	return 0;
}

vector<int> Solution::twoSum(vector<int>& numbers, int target)
{
	unordered_map<int,vector<int>> map;
	// std::unordered_map::const_iterator found;
	int size = numbers.size();

	int i=0;
	while(i<size)
	{
		cout << "checking for i=" << i << "(" << numbers[i] << "), j=" << target-numbers[i] << endl;
		if(map.find(target-numbers[i]) != map.end()) { cout << "found it: " << numbers[i] << ", " << target-numbers[i] << endl; return vector<int> {map[target-numbers[i]][0], i}; }
		else
		{
			cout << "Adding " << numbers[i] << " -> index: " << i << endl;
			map[numbers[i]].push_back(i);
			// do {i++;} while(numbers[i]==numbers[i-1]);
			i++;
		}
	}
	return vector<int> {};
}
