#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

class Solution
{
	public:
		int findContentChildren(vector<int>& g, vector<int>& s);
};

int main()
{
	Solution mysol;
	vector<int> g = {1,3,4,2,1,3,4};
	vector<int> s = {1,2,2,3,1,2,5,4};
	cout << mysol.findContentChildren(g,s) << endl;
	return 0;
}

int Solution::findContentChildren(vector<int>& g, vector<int>& s)
{
	int count=0, i=0, j=0, gsize = g.size(), ssize = s.size();
	sort(g.begin(),g.end());
	sort(s.begin(),s.end());

	while(i<gsize && j<ssize)
	{
		if(s[j]>=g[i])
		{
			count++;
			i++;
		}
		j++;
	}
	return count;
}
