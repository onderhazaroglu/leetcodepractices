#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool isToeplitzMatrix(vector<vector<int>>& matrix);
};

int main()
{
	Solution mysol;
	vector<vector<int>> matrix = { vector<int> {1,2,3,4}, vector<int> {5,1,2,3}, vector<int> {9,5,1,2} };
	vector<vector<int>> m2 = { vector<int> {1,2,3,4}, vector<int> {5,1,2,3}, vector<int> {9,5,1,2} };
	vector<vector<int>> m3 = { vector<int> {11,74,0,93},vector<int> {40,11,74,7} };
	cout << mysol.isToeplitzMatrix(m3) << endl;
	return 0;
}

bool Solution::isToeplitzMatrix(vector<vector<int>>& matrix)
{
	if(matrix.empty() || matrix[0].empty()) return true;
	int cols = matrix[0].size(), rows = matrix.size();

	for(auto v: matrix)
	{
		for(auto i: v) cout << i << " ";
		cout << endl;
	}

	cout << "cols:" << cols << ", rows:" << rows << endl;
	cout << "loop 1" << endl;
	for(int i=0;i<cols;i++)
	{
		cout << "i: " << i << endl;
		for(int j=1;j<rows && i+j<cols;j++)
		{
			cout << "i:" << i << ", j:" << j << endl;
			cout << "m[0][" << i << "]:" << matrix[0][i] << " == m[0+" << j << "=" << 0+j << "][" << i << "+" << j << "=" << i+j << "]:" << matrix[j][i+j] << endl;
			if(matrix[0][i]!=matrix[0+j][i+j]) return false;
		}
	}

	cout << "loop 2" << endl;
	for(int i=1;i<rows;i++)
	{
		for(int j=1;i+j<rows && j<cols;j++)
		{
			cout << "m[" << i << "][0]:" << matrix[i][0] << " == m[" << i << "+" << j << "=" << i+j << "]:" << matrix[i+j][j] << endl;
			if(matrix[i][0]!=matrix[i+j][0+j]) return false;
		}
	}
	return true;
}
