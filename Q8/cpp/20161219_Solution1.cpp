#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class Solution
{
	public:
		int myAtoi(string str);
};

int main()
{
	Solution solution1;
	string str;
	cout << "Please enter a number as a string: ";
	cin >> str;

	int result = solution1.myAtoi(str); 
	cout << result << endl
		<< 2 * result << endl;

	return 0;
}

int Solution::myAtoi(string str)
{
	cout << str << endl;
	short int start = 0, size = str.length();
	int num = 0;
	bool neg = false; //, dot = false;


	// Removing whitespaces
	while(str[start] == ' ') { cout << "whitespace removed" << endl; start++; }

	// Checking for the + or - for the first char
	if ( str[start] == '+' )
	{
		// plus = true;
		cout << "found plus" << endl;
		start++;
	}
	else if ( str[start] == '-' )
	{	
		neg = true;
		cout << "found minus" << endl;
		start++;
	}
	else if ( str[start] < 48 || str[start] > 57 )
	{
		cout << "invalid first char" << endl;
		return 0;
	}


	for(int i=start; i<size; i++)
	{
		// if ( ( str[i] < 48 || str[i] > 57) && str[i] != 46 )
		if ( str[i] < 48 || str[i] > 57 )
		{
			cout << "invalid char found: \"" << str[i] << "\". string has been cut." << endl;
			size = i;
			break;
		}
	}

	cout << "start: " << start << endl
		<< "size: " << size << endl;
		

	cout << "starts (" << start << ") with: \"" << str[start] << "\", ends with: \"" << str[size-1] << "\"" << endl;
	int i = 0, addition = 0;
	for(int t=size-1;t>=start;t--)
	{
		/*
		// if ( ( str[t] < 48 || str[t] > 57) && str[t] != 46 )
		if ( str[t] < 48 || str[t] > 57 )
		{
			num=0; i = 0;
			continue;
		}
		else if ( str[t] == '.' )
		{
			if ( dot ) return 0;
			cout << "found dot" << endl;
			dot = true; num = 0; i = 0;
			continue;
		}
		*/

		addition = static_cast<int>( pow(10,i++) * ( static_cast<int>( str[t] ) - 48 ) );

		if ( addition < 0 )
		{
			if (neg) { return -2147483648; }
			else return 2147483647;
		}
		else if ( neg && addition > ( 2147483648 - ( (-1) * num ) ) )
		{
			cout << "S1" << endl;
			return -2147483648;
		}
		else if ( !neg && addition > ( 2147483647 - num ) )
		{
			cout << "S2" << endl;
			return 2147483647;
		}
		else 
		{
			if (neg)
			{
				cout << "S3" << endl;
				num -= addition;
			}
			else 
			{
				cout << "S4" << endl;
				num += addition;
			}
		}
		
		// num += static_cast<int>( pow(10,i++) * ( static_cast<int>( str[t] ) - 48 ) );
		cout << "for " << str[t] << " num is " << num << endl;
	}

	/*
	if ( num < 0 && !neg )
	{
		num = 2147483647;
	}
	else
	{
		if ( neg )
		{
			cout << "found neg" << endl;
			num = num * (-1);
		}
	}
	*/

	return num;
}
