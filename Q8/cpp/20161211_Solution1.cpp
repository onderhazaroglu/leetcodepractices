#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class Solution
{
	public:
		int myAtoi(string str);
};

int main()
{
	Solution solution1;
	string str;
	cout << "Please enter a number as a string: ";
	cin >> str;

	int result = solution1.myAtoi(str); 
	cout << result << endl
		<< 2 * result << endl;

	return 0;
}

int Solution::myAtoi(string str)
{
	cout << str << endl;
	short int start = 0, size = str.length();
	int num = 0;
	bool neg = false;
	size_t dot = str.find('.');

	// Checking for the invalid integer strings
	if (size > 10) {
		cout << "Invalid string." << endl;
		return 0;
	}
	else
	{
		// Removing whitespaces
		while(str[start] == ' ') { start++; }

		// Checking for the + or - for the first char
		if ( str[start] == '+' )
		{
			// plus = true;
			cout << "found plus" << endl;
			start++;
		}
		else if ( str[start] == '-' )
		{	
			neg = true;
			cout << "found minus" << endl;
			start++;
		}
	}

	int i = 0;
	// for(std::string::reverse_iterator rit=str.rbegin(); rit != str.rend(); ++rit)
	for(int t=size-1;t>=start;t--)//std::string::reverse_iterator rit=str.rbegin(); rit != str.rend(); ++rit)
	{
		if ( str[t] == ' ' ) 
		{
			num=0;
			continue;
		}
		else if ( str[t] == '-' )
		{
			cout << "found invalid char" << endl;
			return 0;
		}
		else if ( str[t] == '+' )
		{
			cout << "found invalid char" << endl;
			return 0;
		}
		else if ( str[t] == '.' )
		{
			cout << "found dot" << endl;
			num = 0;i = 0;
			continue;
		}

		num += static_cast<int>( pow(10,i++) * ( static_cast<int>( str[t] ) - 48 ) );
		cout << "for " << str[t] << " num is " << num << endl;
	}

	if ( num < 0 )
	{
		num = 2147483647;
	}
	else
	{
		if ( neg )
		{
			cout << "found neg" << endl;
			num = num * (-1);
		}
	}

	return num;
}
