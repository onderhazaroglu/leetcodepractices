#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class Solution
{
	public:
		int myAtoi(string str);
};

int main()
{
	Solution solution1;
	string str;
	cout << "Please enter a number as a string: ";
	cin >> str;

	int result = solution1.myAtoi(str); 
	cout << "result: " << result << endl
		<< 2 * result << endl;

	return 0;
}

int Solution::myAtoi(string str)
{
	cout << str << endl;
	short int start = 0, size = str.length();
	int num = 0;
	bool neg = false; //, dot = false;

	// Removing whitespaces at the beginning
	while(str[start] == ' ') { start++; }

	if ( str[start] == '-' ) { neg = true; start++; }
	else if ( str[start] == '+' ) { start++; }

	for(int i=start;i<size;i++)
	{
		if ( str[i] >= 48 && str[i] <= 57 )
		{
			if ( num > 214748364 ) return 2147483647;
			else if ( num < -214748364 ) return -2147483648;
			else if (neg) { num = num * 10 - ( str[i] - '0' ); }
			else { num = num * 10 + str[i] - '0'; }

			if ( neg && num > 0 )
			{
				cout << "c1" << endl;
				return -2147483648;
			}
			else if ( !neg && num < 0 )
			{
				cout << "c2" << endl;
				return 2147483647;
			}
		}
		else return num;

		cout << "for " << str[i] << " num is " << num << endl;
	}

	return num;
}
