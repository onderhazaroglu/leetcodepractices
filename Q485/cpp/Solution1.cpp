#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int findMaxConsecutiveOnes(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {1,1,0,1,1,1,1,0,1,1,1};
	vector<int> nums = {1};
	cout << "Result: " << mysol.findMaxConsecutiveOnes(nums) << endl;
	cout << endl;
	return 0;
}

int Solution::findMaxConsecutiveOnes(vector<int>& nums)
{
	int size = nums.size(), maxOnes = 0, numOnes = 0;

	for(int i=0;i<size;i++)
	{
		if(nums[i] == 1) { numOnes++; }
		else 
		{ 
			if(numOnes > maxOnes) 
			{ 
				maxOnes = numOnes; 
			}; 
			numOnes = 0;
		}
	}
	return maxOnes < numOnes? numOnes : maxOnes;
}
