#include <iostream>
#include <vector>
#include <bitset>
#include <cmath>
using namespace std;

class Solution
{
	public:
		bool isPowerOfFour(int num);
};

int main()
{
	Solution mysol;
	int num;
	cin >> num;
	cout << mysol.isPowerOfFour(num) << endl;
	return 0;
}

bool Solution::isPowerOfFour(int num)
{
	if(num==1) return true;
	if(num<3) return false;
	while(num>1)
	{
		if(num%4!=0) return false;
		num /= 4;
	}
	return true;
}
