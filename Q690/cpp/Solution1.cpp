#include <iostream>
#include <vector>
#include <unordered_map>
#include <queue>
using namespace std;

class Employee {
	public:
		int id;
		int importance;
		vector<int> subordinates;
		Employee(int newid,int newimp,vector<int> newsub): id(newid), importance(newimp), subordinates(newsub) {}
};

class Solution
{
	public:
		int getImportance(vector<Employee*> employees, int id);
};

int main()
{
	Solution mysol;
	vector<Employee*> employees;

	Employee* t = new Employee(1,20,vector<int> {2,3,4});
	employees.push_back( t );
	t = new Employee(2,15,vector<int> {3,4});
	employees.push_back( t );
	t = new Employee(3,10,vector<int>());
	employees.push_back( t );
	t = new Employee(4,5,vector<int> {5});
	employees.push_back( t );
	t = new Employee(5,1,vector<int>());
	employees.push_back( t );

	int id;
	cout << "Enter id: ";
	cin >> id;

	cout << mysol.getImportance(employees,id) << endl;
	return 0;
}

int Solution::getImportance(vector<Employee*> employees, int id)
{
	int val = 0;
	unordered_map<int,Employee*> emap;
	unordered_map<int,int> added;
	queue<int> q;

	for(auto e: employees) emap[e->id] = e;
	cout << "size: " << employees.size() << endl;
	cout << "map size: " << emap.size() << endl;

	q.push(id);

	while(!q.empty())
	{
		int eid = q.front();
		cout << "Cur id: " << eid << endl;
		cout << "Adding value?: " << added[eid] << endl;
		if(added[eid] != 1)
		{
			cout << "S" << endl;
			val+=emap[eid]->importance;
			cout << "A" << endl;
			added[eid] = 1;
			cout << "B" << endl;
		}
		cout << "C" << endl;
		q.pop();
		cout << "D" << endl;
		if(emap[eid]->subordinates.size()>0)
		{
			for(auto qeid: emap[eid]->subordinates) { cout << "pushing " << qeid << endl; q.push(qeid); }
		}
		cout << "E" << endl;
	}
	return val;
}
