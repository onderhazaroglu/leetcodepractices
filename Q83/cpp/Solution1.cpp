#include <iostream>
using namespace std;

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(NULL) {}
};

void print_listnode(ListNode* head)
{
	ListNode* temp = head;

	while(temp != NULL)
	{
		cout << temp->val;
		temp = temp->next;
		if(temp != NULL) cout << "->";
		else cout << endl;
	}
}

class Solution
{
	public:
		ListNode* deleteDuplicates(ListNode* head);
};

int main()
{
	Solution mysol;
	int val;
	cout << "Enter node: ";
	cin >> val;

	ListNode* head = new ListNode(val);
	ListNode* temp = head;

	while(val != -1)
	{
		cout << "Enter node: ";
		cin >> val;
		if(val != -1) temp->next = new ListNode(val);
		temp = temp->next;
	}

	print_listnode(head);
	print_listnode(mysol.deleteDuplicates(head));

	return 0;
}

ListNode* Solution::deleteDuplicates(ListNode* head)
{
	ListNode* temp;
	if(head && head->next) temp = head->next;
	else return head;

	ListNode* prev = head;

	while(temp)
	{
		if(temp->val==prev->val) prev->next = temp->next;
		else prev = temp;
		temp = temp->next;
	}
	return head;
}
