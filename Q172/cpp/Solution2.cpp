#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int trailingZeroes(int n);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter num: ";
	cin >> num;
	cout << mysol.trailingZeroes(num) << endl;
	return 0;
}

int Solution::trailingZeroes(int n)
{
	if(n<5) return 0;
	return n/5 + trailingZeroes(n/5);
}
