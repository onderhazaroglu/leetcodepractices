#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int trailingZeroes(int n);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter num: ";
	cin >> num;
	cout << mysol.trailingZeroes(num) << endl;
	return 0;
}

int Solution::trailingZeroes(int n)
{
	int t = 0;
	while(n>=5)
	{
		t += n / 5;
		n /= 5;
	}
	return t;
}
