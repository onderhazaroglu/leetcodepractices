#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		bool checkPatch(unordered_map<char,int>& mp, unordered_map<char,int>& ms);
		vector<int> findAnagrams(string s, string p);
};

int main()
{
	Solution mysol;
	string s,p;
	cout << "Enter s: ";
	cin >> s;
	cout << "Enter p: ";
	cin >> p;
	for(int i: mysol.findAnagrams(s,p)) cout << i << endl;
	return 0;
}

bool Solution::checkPatch(unordered_map<char,int>& mp, unordered_map<char,int>& ms)
{
	cout << "new checkpatch call" << endl;
	for(auto it: ms)
	{
		if(it.second==0) continue;
		cout << "ms[" << it.first << "]: " << ms[it.first] << " - " << "mp[" << it.first << "]: " << mp[it.first] << endl;
		if(mp[it.first]!=ms[it.first]) return false;
	}
	return true;
}

vector<int> Solution::findAnagrams(string s, string p)
{
	vector<int> res;
	unordered_map<char,int> mp, ms;

	int plen = p.length(), slen = s.length();

	for(int i=0;i<plen;i++) mp[p[i]]++, ms[s[i]]++;

	for(auto it: mp)
	{
		cout << "mp[" << it.first << "]:" << mp[it.first] << ", ";
	}
	cout << endl;

	for(auto it: ms)
	{
		cout << "ms[" << it.first << "]:" << ms[it.first] << ", ";
	}
	cout << endl;

	if(checkPatch(mp,ms)) res.push_back(0);

	cout << "STARTING... " << endl << endl;

	for(int i=1;i<=slen-plen;i++)
	{
		ms[s[i-1]]--, ms[s[i-1+plen]]++;
		for(auto it: ms)
		{
			if(it.second==0) continue;
			cout << "ms[" << it.first << "]:" << ms[it.first] << ", ";
		}
		cout << endl;
		if(mp[s[i]]>0 && checkPatch(mp,ms)) res.push_back(i);
	}
	return res;
}
