#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		bool findAnagrams(const string s, unordered_map<char,int>& mp, int idx, int plen);
		vector<int> findAnagrams(string s, string p);
};

int main()
{
	Solution mysol;
	string s,p;
	cout << "Enter s: ";
	cin >> s;
	cout << "Enter p: ";
	cin >> p;
	for(int i: mysol.findAnagrams(s,p)) cout << i << endl;
	return 0;
}

bool Solution::findAnagrams(const string s, unordered_map<char,int>& mp, int idx, int plen)
{
	int count = plen;
	unordered_map<char,int> ms;

	for(int i=idx;i<idx+plen;i++)
	{
		if(mp[s[i]]>0 && ++ms[s[i]]<=mp[s[i]]) count--;
		else return false;
	}
	return count==0;
}

vector<int> Solution::findAnagrams(string s, string p)
{
	vector<int> res;
	unordered_map<char,int> mp, ms;

	int plen = p.length(), slen = s.length();

	for(char c: p) mp[c]++;

	for(int i=0;i<slen;i++)
	{
		if(mp[s[i]]>0 && (i+plen)<=slen && findAnagrams(s,mp,i,plen)) res.push_back(i);
	}
	return res;
}
