#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int maxCount(int m, int n, vector<vector<int>>& ops);
};

int main()
{
	Solution mysol;
	vector<vector<int>> ops = { vector<int> {2,2}, vector<int> {3,3} };
	cout << mysol.maxCount(3,3,ops) << endl;
	return 0;
}

int Solution::maxCount(int m, int n, vector<vector<int>>& ops)
{
	for(auto v: ops)
	{
		m = min(m,v[0]);
		n = min(n,v[1]);
	}
	return m*n;
}
