#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		vector<string> findRelativeRanks(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {20,35,50,5,4,3,2,1};
	vector<int> nums = {1};
	for(string s: mysol.findRelativeRanks(nums))
	{
		cout << s << ", ";
	}
	cout << endl;
	return 0;
}

vector<string> Solution::findRelativeRanks(vector<int>& nums)
{
	int size = nums.size();
	if(size<1) return vector<string> {};
	else if (size<2) return vector<string> {"Gold Medal"};

	unordered_map<int,int> map;

	for(int i=0;i<size;i++)
	{
		map[nums[i]] = i;
	}

	vector<string> res(size,"");
	// res.resize(size);

	std::sort(nums.begin(),nums.end(), std::greater<int>());
	res[map[nums[0]]] = "Gold Medal";
	res[map[nums[1]]] = "Silver Medal";
	if(size>2) res[map[nums[2]]] = "Bronze Medal";

	for(int i=3;i<size;i++)
	{
		res[map[nums[i]]] = to_string(i+1);
	}
	return res;
}
