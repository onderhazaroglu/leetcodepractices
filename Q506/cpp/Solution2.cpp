#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;

class Solution
{
	public:
		vector<string> findRelativeRanks(vector<int>& nums);
};

int main()
{
	Solution mysol;
	vector<int> nums = {20,35,50,5,4,3,2,1};
	// vector<int> nums = {1};
	for(string s: mysol.findRelativeRanks(nums))
	{
		cout << s << ", ";
	}
	cout << endl;
	return 0;
}

vector<string> Solution::findRelativeRanks(vector<int>& nums)
{
	int size = nums.size();
	if(size<1) return vector<string> {};
	else if (size<2) return vector<string> {"Gold Medal"};

	priority_queue< pair<int,int> > pq;

	for(int i=0;i<size;i++)
	{
		pq.push(make_pair(nums[i],i));
	}

	vector<string> res(size,"");
	int count = 1;

	for(int i=0;i<size;i++)
	{
		if(count==1) res[pq.top().second] = "Gold Medal";
		else if(count==2) res[pq.top().second] = "Silver Medal";
		else if(count==3) res[pq.top().second] = "Bronze Medal";
		else res[pq.top().second] = to_string(i+1);
		pq.pop();
		count++;
	}

	return res;
}
