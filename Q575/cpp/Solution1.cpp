#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
	public:
		int distributeCandies(vector<int>& candies);
};

int main()
{
	Solution mysol;
	vector<int> nums;
	int n = 0;
	while(n != -1)
	{
		cout << "Enter integer: ";
		cin >> n;
		if(n != -1) nums.push_back(n);
	}
	cout << mysol.distributeCandies(nums) << endl;
	return 0;
}

int Solution::distributeCandies(vector<int>& candies)
{
	unordered_map<int,int> map;
	for(auto i: candies)
	{
		map[i]++;
	}
	int res = candies.size()/2;
	int msize = map.size();
	cout << "res: " << res << ", msize: " << msize << endl;
	return res > msize ? msize : res;
}
