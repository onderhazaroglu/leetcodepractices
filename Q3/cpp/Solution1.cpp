#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class Solution {
	public:
		int lengthOfLongestSubstring(string str);
};

int main() {

	string k = "!\\\"#$%&'()*+,-. /:;  <=>?@[\\\\]^_`{|}~";

	cout << k.substr(0,1) << endl;
	cout << k.substr(0,3) << endl;


	int *m = new int[5]();
	fill(m, m+5, -1);
	for (unsigned int i=0;i<5;i++) {
		cout << m[i] << endl;
	}


	Solution sol1;
	string str;
	cout << "Please enter a string: ";
	// cin >> str;
	ifstream infile("input2.txt");
	getline(infile, str);

	infile.close();

	int result = sol1.lengthOfLongestSubstring( str );
	cout << "result: " << result << endl;


	return 0;
}

int Solution::lengthOfLongestSubstring(string str) {

	cout << "input: " << str << endl << endl;

	for (unsigned t=0;t<str.length();t++) {
		cout << "char: " << str[t] << ", code: " << int(str[t]) << endl;
	}

	cout << endl << endl;



	int cur = 0, max = 0, *p;
	p = new int[500] ();
	fill(p, p+500, -1);
	string cstr = "", lstr = "";

	for (unsigned int i=0;i<str.length();i++) {
		if ( p[int(str[i])] > -1 ) {
			cout << "    if worked: char " << str[i] << " , " << p[int(str[i])] << endl;
			if ( cur > max ) {
				max = cur;
				lstr = cstr;
				cout << "temp max = " << max << endl;
			}
			cstr = str.substr( p[int(str[i])] + 1, i - p[int(str[i])] );
			cout << "cutting from " << p[int(str[i])] + 1 << " to " << i << endl;
			cur = i - p[int(str[i])];
			// p = new int[300] ();
			fill(p, p+ p[int(str[i])], -1);
			p[int(str[i])] = i;
		}
		else {
			cout << "else worked: char ";
			cur++;
			cstr += str[i];
			p[int(str[i])] = i;
			cout << str[i] << " , " << p[int(str[i])] << endl;
		}
		cout << "cur string: " << cstr << endl;
	}

	if ( cur > max ) {
		max = cur;
		lstr = cstr;
	}

	cout << "max: " << max << endl;
	cout << "max string: " << lstr << endl;

	return max;
}
