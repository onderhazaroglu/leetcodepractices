#include <iostream>
#include <set>
using namespace std;

int main() {

	set<char> myset;
	char c = 'A';
	myset.insert( c );

	if ( myset.find( 'A' ) != myset.end() ) {
		cout << "exists" << endl;
	}
	else {
		cout << "doesn't exist!" << endl;
	}
	return 0;
}
