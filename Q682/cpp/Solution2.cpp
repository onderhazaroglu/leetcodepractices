#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		int calPoints(vector<string>& ops);
};

int main()
{
	Solution mysol;
	// vector<string> ops = {"5","2","C","D","+"};
	vector<string> ops = {"5","-2","4","C","D","9","+","+"};
	cout << mysol.calPoints(ops) << endl;
	return 0;
}

int Solution::calPoints(vector<string>& ops)
{
	vector<int> vs; // valid scores
	size_t size = 0;
	int sum = 0;
	if(ops.size() == 0) return 0;

	for(string i: ops)
	{
		cout << "Processing i: " << i << endl;
		if(i == "+")
		{
			if(size >= 2) vs.push_back( vs[size-1] + vs[size-2] );
			else if(size == 1) vs.push_back( vs[size-1] );
			else vs.push_back( 0 );
			size++;
			sum += vs[size-1];
		}
		else if(i == "D")
		{
			if(size >= 1) vs.push_back( 2*vs[size-1] );
			else vs.push_back( 0 );
			size++;
			sum += vs[size-1];
		}
		else if(i == "C")
		{
			if(size >= 1) { vs.pop_back(); size--; sum -= vs[size-1]; }
		}
		else { vs.push_back( stoi(i) ); size++; sum+= vs[size-1]; }
		cout << "Sum: " << sum << endl;
	}
	return sum;
}
