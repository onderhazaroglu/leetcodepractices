#include <iostream>
#include <vector>
#include <bitset>
using namespace std;

class Solution
{
	public:
		int countPrimeSetBits(int L,int R);
};

int main()
{
	Solution mysol;
	int L, R;
	cout << "Enter L: ";
	cin >> L;
	cout << "Enter R: ";
	cin >> R;
	cout << mysol.countPrimeSetBits(L,R) << endl;
	return 0;
}

int Solution::countPrimeSetBits(int L,int R)
{
	int res = 0;
	for(int i=L;i<=R;i++)
	{
		bitset<20> bi = i;
		if(bi.count()==2||bi.count()==3||bi.count()==5||bi.count()==7||bi.count()==11||bi.count()==13||bi.count()==17||bi.count()==19) res++;
	}
	return res;
}
