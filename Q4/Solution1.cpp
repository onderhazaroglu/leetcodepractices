#include <iostream>
#include <vector>
using namespace std;

class Solution {
	public:
		double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2);
};


int main() {

	// int nums1[] = {1, 2, 4, 9};
	// int nums2[] = {2, 10, 11};

	int nums1[] = {1, 2, 4};
	int nums2[] = {3, 5};

	vector<int> v1( nums1, nums1 + sizeof(nums1) / sizeof(int) );
	vector<int> v2( nums2, nums2 + sizeof(nums2) / sizeof(int) );

	Solution sol1;

	double median = sol1.findMedianSortedArrays( v1, v2 );

	cout << "median: " << median << endl;

	return 0;
}



double Solution::findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
	vector<int> all;
	int n = nums1.size();
	int m = nums2.size();

	cout << "n: " << n << ", m: " << m << endl;

	int i=0, j=0;

	while ( i<n || j<m ) {

		if ( i<n && j<m ) {
			if ( nums1[i] <= nums2[j] ) all.push_back( nums1[i++] );
			else all.push_back( nums2[j++] );
		}
		else if ( i<n ) all.push_back( nums1[i++] );
		else all.push_back( nums2[j++] );

		cout << "size: " << all.size() << endl;
	}

	int median = 0.0;

	for(unsigned int i=0;i<all.size();i++) {
		cout << all[i] << endl;
	}

	if ( all.size() % 2 == 0 ) {
		cout << "if" << endl;
		median = ( all[all.size()/2-1] + all[all.size()/2] ) / 2.0;
	}
	else {
		cout << "else" << endl;
		median = all[all.size()/2];
	}

	return median;
}
