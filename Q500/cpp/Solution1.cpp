#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
using namespace std;

class Solution
{
	public:
		vector<string> findWords(vector<string>& words);
};

int main()
{
	Solution mysol;

	string word = "";
	vector<string> words, rwords;

	while(true)
	{
		cout << "Enter a word or -1 to quit: ";
		cin >> word;
		if ( word == "-1" ) break;

		words.push_back( word );
	}

	rwords = mysol.findWords( words );

	for(auto w: rwords)
	{
		cout << w << endl;
	}

	return 0;
}


vector<string> Solution::findWords(vector<string>& words)
{
if ( words.empty() ) return words;

unordered_set<char> r1 = {'Q','W','E','R','T','Y','U','I','O','P','q','w','e','r','t','y','u','i','o','p'};
unordered_set<char> r2 = {'A','S','D','F','G','H','J','K','L','a','s','d','f','g','h','j','k','l'};
unordered_set<char> r3 = {'Z','X','C','V','B','N','M','z','x','c','v','b','n','m'};

vector<unordered_set<char>> rows = {r1, r2, r3};

vector<string> res;

for(unsigned int i=0;i<words.size();i++)
{
	int row = 0;

	for(int j=0;j<3;j++)
	{
		if( rows[j].count(words[i][0]) )
		{
			row = j;
			break;
		}
	}

	res.push_back( words[i] );

	for(unsigned int k=1;k<words[i].size();k++)
	{
		if(rows[row].count(words[i][k]) == 0 )
		{
			res.pop_back();
			break;
		}
	}
}
return res;
}
