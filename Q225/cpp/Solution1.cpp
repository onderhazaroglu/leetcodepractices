#include <iostream>
#include <stack>
#include <queue>
using namespace std;

class MyStack {
	public:
		queue<int> lq, rq;
		/** Initialize your data structure here. */
		MyStack() {
			lq = queue<int>();
			rq = queue<int>();
		}

		/** Push element x to the back of queue. */
		void push(int x) {
			rq.push(x);
			while(!lq.empty())
				rq.push(lq.front()), lq.pop();
			lq = rq, rq = queue<int>();
		}

		/** Removes the element from in front of queue and returns that element. */
		int pop() {
			int res = top();
			lq.pop();
			return res;
		}

		/** Get the front element. */
		int top() {
			return lq.front();
		}

		/** Returns whether the queue is empty. */
		bool empty() {
			return lq.empty();
		}
};

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue obj = new MyQueue();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.peek();
 * bool param_4 = obj.empty();
 */

int main()
{
	MyStack ms;
	ms.push(2);
	ms.push(3);
	ms.push(5);
	cout << "top after 5:" << ms.top() << endl;
	cout << endl;
	ms.push(7);
	ms.push(8);
	ms.push(9);

	while(!ms.empty())
	{
		cout << ms.pop() << " ";
	}
	cout << endl;


	MyStack ms2;
	ms2.push(1);
	ms2.push(2);
	cout << ms2.top() << endl;

	return 0;
}
