#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		int repeatedStringMatch(string A,string B);
};

int main()
{
	Solution mysol;
	string A,B;
	cout << "Enter A: ";
	cin >> A;
	cout << "Enter B: ";
	cin >> B;
	cout << mysol.repeatedStringMatch(A,B) << endl;
	return 0;
}

int Solution::repeatedStringMatch(string A,string B)
{
	string as = A;
	int lenb = B.length(), lena = A.length();
	int k = lenb/lena + (lenb%lena>0?1:0) + 1;
	cout << "k: " << k << endl;

	for(int i=1;i<k;i++) as += A;
	int lenas = k*lena;
	cout << "as: " << as << endl;

	int i=0, j=0, count = lenb;
	while(i<lenas)
	{
		cout << "as[" << i << "]:" << as[i] << ", B[" << j << "]:" << B[j] << endl;
		if(as[i]==B[j]) count--, j++;
		else count = lenb, j=0;
		cout << "count: " << count << endl;
		if(count==0) return i>(k-1)*lena?k:k-1;
		i++;
	}
	cout << "count: " << count << endl;
	cout << "END" << endl;
	return -1;
}
