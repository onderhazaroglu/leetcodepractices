#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		int repeatedStringMatch(string A,string B);
};

int main()
{
	Solution mysol;
	// string A = "axaxaya",B = "axaya";
	string A = "abcd",B = "cdabcdab";
	cout << " A: " << A << endl << " B: " << B << endl;
	cout << mysol.repeatedStringMatch(A,B) << endl;
	return 0;
}

int Solution::repeatedStringMatch(string A,string B)
{
	string as = A;
	int lenb = B.length(), lena = A.length();
	int k = lenb/lena + (lenb%lena>0?1:0) + 1;

	for(int i=1;i<k;i++) as += A;
	int lenas = k*lena;
	cout << "as: " << as << endl;
	cout << "k: " << k << endl;

	int i=0;
	while(i<lenas-lenb)
	{
		cout << "as[" << i << "]:" << as[i] << endl;
		if(as[i]==B[0] && i+lenb<lenas && as.compare(i,lenb,B)==0) return i+lenb>(k-1)*lena?k:k-1;
		i++;
	}
	cout << "END" << endl;
	return -1;
}
