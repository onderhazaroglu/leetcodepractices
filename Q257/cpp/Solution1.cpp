#include <iostream>
#include <string>
#include <stack>
#include <vector>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		void binTreePaths(TreeNode* root, string s, vector<string>& p);
		vector<string> binaryTreePaths(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	TreeNode* r3 = new TreeNode(1);
	r3->left = new TreeNode(2);
	r3->right = new TreeNode(3);
	r3->left->right = new TreeNode(5);

	for(string s: mysol.binaryTreePaths(r3)) cout << s << endl;
	return 0;
}

void Solution::binTreePaths(TreeNode* root, string s, vector<string>& p)
{
	s = s.empty()?to_string(root->val):s+"->"+to_string(root->val);
	if(!root->left && !root->right) p.push_back(s);
	if(root->left) binTreePaths(root->left,s,p);
	if(root->right) binTreePaths(root->right,s,p);
}

vector<string> Solution::binaryTreePaths(TreeNode* root)
{
	vector<string> p;
	if(root) binTreePaths(root,"",p);
	return p;
}
