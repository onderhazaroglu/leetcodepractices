#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		int lup(TreeNode* root,int len);
		int longestUnivaluePath(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(4);
	r1->left->left = new TreeNode(4);
	r1->left->right = new TreeNode(4);

	r1->right = new TreeNode(5);
	r1->right->left = new TreeNode(5);

	TreeNode* r2 = new TreeNode(5);
	r2->left = new TreeNode(5);
	r2->right = new TreeNode(3);
	r2->right->right = new TreeNode(3);
	r2->right->right->right = new TreeNode(3);
	r2->right->right->right->right = new TreeNode(3);
	r2->right->right->right->right->right = new TreeNode(2);

	r2->left->left = new TreeNode(4);
	r2->left->right = new TreeNode(1);
	r2->left->left->left = new TreeNode(4);
	r2->left->left->left->left = new TreeNode(4);
	r2->left->left->left->left->left = new TreeNode(2);
	r2->left->left->left->left->right = new TreeNode(4);

	print_tree(r2);
	cout << mysol.longestUnivaluePath(r2) << endl;
	cout << "NEW" << endl;
	cout << mysol.longestUnivaluePath(r1) << endl;

	return 0;
}

int Solution::lup(TreeNode* root,int len)
{
	cout << "r: " << root->val << ", len: " << len << endl;
	return max( max(len, (root->left?(root->val==root->left->val?lup(root->left,len+1):lup(root->left,0)):0)), (root->right?(root->val==root->right->val?lup(root->right,len+1):lup(root->right,0)):0));
	/*
	return max( root->left && root->val==root->left->val?lup(root->left,len+1):
	if(root->left && root->
	int nval = (n && root->val==n->val?1:0);
	return max( nval + lup(root,n->left), nval + lup(root,n->right) );
	*/
}

int Solution::longestUnivaluePath(TreeNode* root)
{
	if(!root) return 0;
	return lup(root,0);
}
