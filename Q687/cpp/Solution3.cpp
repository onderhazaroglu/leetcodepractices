#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	int res;
	public:
		int lup(TreeNode* root);
		int longestUnivaluePath(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(4);
	r1->left->left = new TreeNode(4);
	r1->left->right = new TreeNode(4);

	r1->right = new TreeNode(5);
	r1->right->left = new TreeNode(5);

	TreeNode* r2 = new TreeNode(5);
	r2->left = new TreeNode(5);
	r2->right = new TreeNode(3);
	r2->right->right = new TreeNode(3);
	r2->right->right->right = new TreeNode(3);
	r2->right->right->right->right = new TreeNode(3);
	r2->right->right->right->right->right = new TreeNode(2);

	r2->left->left = new TreeNode(4);
	r2->left->right = new TreeNode(1);
	r2->left->left->left = new TreeNode(4);
	r2->left->left->left->left = new TreeNode(4);
	r2->left->left->left->left->left = new TreeNode(2);
	r2->left->left->left->left->right = new TreeNode(4);

	print_tree(r2);
	cout << mysol.longestUnivaluePath(r2) << endl;
	cout << "NEW" << endl;
	print_tree(r1);
	cout << mysol.longestUnivaluePath(r1) << endl;

	return 0;
}

int Solution::lup(TreeNode* root)
{
	if(!root) return 0;
	int leftlup = lup(root->left);
	int rightlup = lup(root->right);
	int lval = 0, rval = 0;
	if(root->left && root->left->val==root->val)
		lval += leftlup + 1;
	if(root->right && root->right->val==root->val)
		rval += rightlup + 1;
	res = max( res, lval + rval );
	cout << "r: " << root->val << ", max(lval=" << lval << ",rval=" << rval << ")=" << max(lval,rval) << endl;
	return max( lval, rval );
}

int Solution::longestUnivaluePath(TreeNode* root)
{
	res = 0;
	lup(root);
	return res;
}
