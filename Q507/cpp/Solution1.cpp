#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool checkPerfectNumber(int num);
};

int main()
{
	Solution mysol;
	int num;
	cin >> num;
	cout << mysol.checkPerfectNumber(num) << endl;
	return 0;
}

bool Solution::checkPerfectNumber(int num)
{
	if(num<=0) return false;
	int sum = 0;
	for(int i=1;i*i<=num;i++)
	{
		if(num%i==0)
		{
			sum+=i;
			if(i*i!=num) sum += num / i;
		}
	}
	return sum-num==num;
}
