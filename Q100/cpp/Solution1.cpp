#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		bool isSameTree(TreeNode* p, TreeNode* q);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->right = new TreeNode(3);

	TreeNode* r3 = new TreeNode(1);
	r3->left = new TreeNode(2);
	r3->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	if(mysol.isSameTree(r1,r3)) cout << "Trees are same." << endl;
	else cout << "Trees are NOT same!" << endl;

	return 0;
}

bool Solution::isSameTree(TreeNode* p, TreeNode* q)
{
	if(!p && !q) return true;

	// version 2 istead of the following block
	else if(p && q) return p->val == q->val && isSameTree(p->left,q->left) && isSameTree(p->right,q->right);
	else return false;
	//
	/*
	else if(bool(p) != bool(q) || (p && q && p->val != q->val)) return false;
	return isSameTree(p->left,q->left) && isSameTree(p->right,q->right);
	*/
}
