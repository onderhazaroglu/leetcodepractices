#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <climits>
using namespace std;

bool cmp(pair<int,int> a,pair<int,int> b) { return bool(a.second < b.second); }

class Solution
{
	public:
		int findShortestSubArray(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {1,3,4,2,1,3,4,1,2,2,3,1,4,5};
	vector<int> nums = {1,2,2,3,1};
	cout << mysol.findShortestSubArray(nums) << endl;
	return 0;
}

int Solution::findShortestSubArray(vector<int>& nums)
{
	vector<int> si(50000,-1);
	vector<int> ei(50000,-1);
	unordered_map<int,int> map;
	int max = 0;

	for(size_t i=0;i<nums.size();i++)
	{
		map[nums[i]]++;
		if(map[nums[i]]>max) max = map[nums[i]];
		if(si[nums[i]]<0) si[nums[i]] = i;
		ei[nums[i]] = i;
	}
	vector<int> maxel;
	// sort(map.begin(),map.end(),cmp);
	for(auto t: map)
	{
		cout << "el: " << t.first << " (" << t.second << ")" << endl;
		if(t.second == max) maxel.push_back(t.first);
	}

	int min = INT_MAX;
	for(auto i: maxel)
	{
		cout << "maxel: " << i << endl;
		if(ei[i]-si[i]+1<min) min = ei[i]-si[i]+1;
	}

	return min;
}
