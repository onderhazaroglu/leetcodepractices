#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		bool validPar(string s);
		vector<string> permutation(char l,string s);
		vector<string> generateParenthesis(int n);
};

int main()
{
	Solution mysol;
	int n;
	cout << "Enter number: ";
	cin >> n;
	vector<string> res = mysol.generateParenthesis(n);
	for(auto s: res) cout << s << endl;
	return 0;
}

vector<string> Solution::generateParenthesis(int n)
{
	set<string> sres;
	string par = "()";
	for(int i=1;i<n;i++) par += "()";
	vector<string> vpr = permutation(0,par);
	for(auto s: vpr) if(validPar(s)) sres.insert(s);
	vector<string> res(sres.begin(),sres.end());
	return res;
}

vector<string> Solution::permutation(char l,string s)
{
	// cout << "start char:[" << l << "], string:[" << s << "]" << endl;
	// if(l==0) cout << "l==0" << endl;
	// else cout << "l!=0" << endl;

	vector<string> results;
	if(s.length()==1) { if(l!=0) { results.push_back( l+s ); } else results.push_back( s ); return results; }
	vector<string> nr = permutation(s[0],s.substr(1,s.length()-1));

	for(auto i: nr)
	{
		if(l!=0) { results.push_back(l+i); } else results.push_back( i );
	}
	for(unsigned int i=1;i<s.length();i++)
	{
		std::swap(s[0],s[i]);
		nr = permutation(s[0],s.substr(1,s.length()-1));
		for(auto i: nr) { if(l!=0) { results.push_back(l+i); } else results.push_back( i ); }
		std::swap(s[i],s[0]);
	}
	return results;
}

bool Solution::validPar(string s)
{
	int b = 0, e = 0;
	for(unsigned int i=0;i<s.length();i++)
	{
		if(s[i] == '(') b++;
		else if(s[i] == ')') e++;
		else return false;

		if(b<e) return false;
	}
	return true;
}
