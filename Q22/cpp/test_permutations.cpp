#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

vector<string> permutation(char l,string s)
{
	// cout << "start char:[" << l << "], string:[" << s << "]" << endl;
	vector<string> results;
	if(s.length()==1) { results.push_back( l+s ); return results; }
	vector<string> nr = permutation(s[0],s.substr(1,s.length()-1));

	for(auto i: nr)
	{
		// cout << l+i << endl; 
		results.push_back(l+i); 
	}
	for(unsigned int i=1;i<s.length();i++)
	{
		std::swap(s[0],s[i]);
		nr = permutation(s[0],s.substr(1,s.length()-1));
		for(auto i: nr) results.push_back(l+i);
		std::swap(s[i],s[0]);
	}
	return results;
}

int main()
{
	string str = "onder";
	vector<string> res = permutation(0,str);
	for(auto i: res)
	{
		cout << i << endl;
	}
	return 0;
}
