#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
	public:
		void addPar(vector<string> &v, string s, int l, int r);
		vector<string> generateParenthesis(int n);
};

int main()
{
	Solution mysol;
	int n;
	cout << "Enter number: ";
	cin >> n;
	vector<string> res = mysol.generateParenthesis(n);
	for(auto s: res) cout << s << endl;
	return 0;
}

vector<string> Solution::generateParenthesis(int n)
{
	vector<string> res;
	addPar(res,"",n,0);
	return res;
}

void Solution::addPar(vector<string> &v, string s, int l, int r)
{
	cout << "Call to addPar: str[" << s << "], l:[" << l << "], r:[" << r << "]" << endl;
	if(l==0 && r==0) {
		v.push_back(s);
		return;
	}
	if(r>0) addPar(v,s+")",l,r-1);
	if(l>0) addPar(v,s+"(",l-1,r+1);
}
