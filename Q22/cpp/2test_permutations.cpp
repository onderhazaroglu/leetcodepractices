#include<string>
#include<iostream>
#include<algorithm>

template <int N>
struct Factorial
{
	static const long value = Factorial<N-1>::value * N;
};

template <>
struct Factorial<0> 
{
	static const long value = 1;
};

int main()
{
	std::string str("123");
	auto num_perm = Factorial<3>::value;

	std::cout << "num_perm=" << num_perm << std::endl;
	auto permutate = [](int k, std::string s)
	{
		for(auto j=1u; j < s.length(); ++j)
		{
			std::swap(s[k % (j+1)], s[j]); 
			k = k / (j+1);
		}
		return s;
	};

	std::cout << str << std::endl;
	for(int pi = 0u; pi < num_perm - 1; ++pi)
	{
		std::cout << permutate(pi, str) << std::endl;
	}


	return 0;
}
