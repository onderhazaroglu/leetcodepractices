#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool isPowerThree(int n);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter number: ";
	cin >> num;
	cout << mysol.isPowerThree(num) << endl;
	return 0;
}

bool Solution::isPowerThree(int n)
{
	return n>0 && (n==1 || 1162261467 % n == 0);
}
