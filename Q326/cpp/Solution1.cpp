#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool isPowerThree(int n);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter number: ";
	cin >> num;
	cout << mysol.isPowerThree(num) << endl;
	return 0;
}

bool Solution::isPowerThree(int n)
{
	while(n>1)
	{
		if(n%3!=0) return false;
		n /= 3;
		if(n==1) return true;
	}
	return n==1;
}
