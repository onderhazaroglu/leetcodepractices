#include <iostream>
#include <stack>
#include <vector>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		int sum = 0;
		int sumbr(TreeNode* root);
		int findTilt(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r3 = new TreeNode(1);
	r3->left = new TreeNode(2);
	r3->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);


	// cout << mysol.findTilt(r1) << endl;
	cout << mysol.findTilt(r2) << endl;
	// cout << mysol.findTilt(r3) << endl;
	return 0;
}

int Solution::sumbr(TreeNode* root)
{
	if(!root) return 0;
	if(!root->left && !root->right) return root->val;
	return root->val + sumbr(root->left) + sumbr(root->right);
}

int Solution::findTilt(TreeNode* root)
{
	if(!root) return 0;
	int ls = sumbr(root->left), rs = sumbr(root->right);
	findTilt(root->left);
	findTilt(root->right);
	sum += ls<=rs?rs-ls:ls-rs;
	return sum;
}
