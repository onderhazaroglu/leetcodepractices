#include <iostream>
using namespace std;

class Solution
{
	public:
		bool canWinNum(int n);
};

int main()
{
	Solution mysol;
	int n;

	cout << "Enter # of stones: ";
	cin >> n;

	if(mysol.canWinNum(n)) cout << "You can WIN with " << n << " stones." << endl;
	else cout << "You will LOSE with " << n << " stones!" << endl;

	return 0;
}

bool Solution::canWinNum(int n)
{
	return n % 4 != 0;
}
