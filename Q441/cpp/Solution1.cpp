#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Solution
{
	public:
		int arrangeCoins(int n);
};

int main()
{
	Solution mysol;
	int num;
	cout << "Enter num: ";
	cin >> num;
	cout << mysol.arrangeCoins(num) << endl;
	return 0;
}

int Solution::arrangeCoins(int n)
{
	long int i = ceil(sqrt(2)*sqrt(n));
	while(true)
	{
		if(i*(i+1)/2>n) i--;
		else break;
	}
	return i;
}
