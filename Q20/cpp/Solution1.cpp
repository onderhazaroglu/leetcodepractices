#include <iostream>
#include <string>
#include <unordered_map>
#include <stack>
using namespace std;

class Solution
{
	public:
		bool isValid(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	if(mysol.isValid(s)) cout << s << " is valid." << endl;
	else cout << s << " is NOT valid!" << endl;
	return 0;
}

bool Solution::isValid(string s)
{
	unsigned int size = s.length();
	if(size==0) return true;

	unordered_map<char,char> map = { {')','('}, {'}','{'}, {']','['} };
	// map[')'] = '(';
	// map['}'] = '{';
	// map[']'] = '[';
	stack<char> pst;

	for(char i: s)
	{
		if(i=='(' || i=='{' || i=='[') pst.push(i);
		else if(i==')' || i=='}' || i==']')
		{
			if(!pst.empty() && pst.top() == map[i]) pst.pop();
			else
			{
				if(!pst.empty()) cout << "pst.top(): " << pst.top() << ", i: " << i << endl;
				return false;
			}
		}
		else
		{
			cout << "pst.top(): " << pst.top() << ", i: " << i << endl;
			return false;
		}
	}
	if(pst.empty()) return true;
	else return false;
}
