#include <iostream>
using namespace std;

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x): val(x), next(NULL) {}
};

class Solution {
	public:
	ListNode* addTwoNumbers(ListNode* l1, ListNode* l2);
};

void print_list(ListNode* l) {
	ListNode *temp = l;
	do {
		cout << temp->val;
		temp = temp->next;
	}
	while ( temp != NULL && cout << " -> " ); 

	cout << endl;
}

int main() {

	ListNode *x = new ListNode(5);
	x->next = new ListNode(4);
	// x->next->next = new ListNode(6);

	ListNode *y = new ListNode(5);
	y->next = new ListNode(6);
	y->next->next = new ListNode(4);



	Solution sol1;

	ListNode *out = sol1.addTwoNumbers(x, y);
	print_list( out );
	print_list( x );
	print_list( y );


	return 0;
}

ListNode* Solution::addTwoNumbers(ListNode* l1, ListNode* l2) {

	if ( l1 == NULL && l2 == NULL ) return NULL;
	ListNode *out = new ListNode(0);
	ListNode *tout = out;

	int r = 0;
	do {
		int sum = r;
		cout << "r: " << r << " sum: " << sum;
		r = 0;
		cout << "\nA\n";

		if ( l1 != NULL ) sum += l1->val;
		if ( l2 != NULL ) sum += l2->val;

		cout << "\nB\n";
		if ( sum > 9 ) {
			sum = sum % 10;
			r = 1;
		}

		cout << "\nC\n";
		cout << ", r: " << r << " sum: " << sum << endl;
		tout->val = sum;

		cout << "\nD\n";
		if ( r > 0 || ( l1 != NULL && l1->next != NULL ) || ( l2 != NULL && l2->next != NULL ) ) {
			tout->next = new ListNode(0);
		}

		cout << "\nE\n";
		tout = tout->next;
		if ( l1 != NULL ) l1 = l1->next;
		if ( l2 != NULL ) l2 = l2->next;
		cout << "\nF\n";
	}
	while ( r > 0 || l1 != NULL || l2 != NULL );


	return out;
}
