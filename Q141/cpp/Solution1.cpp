#include <iostream>
#include <unordered_map>
using namespace std;

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(NULL) {}
};

void print_listnode(ListNode* head)
{
	ListNode* temp = head;

	while(temp != NULL)
	{
		cout << temp->val;
		temp = temp->next;
		if(temp != NULL) cout << "->";
		else cout << endl;
	}
}

class Solution
{
	public:
		bool hasCycle(ListNode* head);
};

int main()
{
	Solution mysol;
	// int val;
	// cout << "Enter node: ";
	// cin >> val;

	ListNode* head = new ListNode(1);
	head->next = new ListNode(2);
	head->next->next = new ListNode(3);
	head->next->next->next = new ListNode(4);
	// head->next->next->next->next = head->next;
	head->next->next->next->next = new ListNode(5);

	cout << mysol.hasCycle(head) << endl;

	/*
	while(val != -1)
	{
		cout << "Enter node: ";
		cin >> val;
		if(val != -1) temp->next = new ListNode(val);
		temp = temp->next;
	}
	int k;
	cout << "Enter k: ";
	cin >> k;

	// print_listnode(head);
	// print_listnode(mysol.reverseKGroup(head,k));

	*/
	return 0;
}

bool Solution::hasCycle(ListNode* head)
{
	unordered_map<ListNode*,int> map;
	ListNode* temp = head;

	// int num = 0;
	while(temp != NULL)
	{
		if(map[temp]++>0) return true;
		temp = temp->next;
	}
	return false;
}
