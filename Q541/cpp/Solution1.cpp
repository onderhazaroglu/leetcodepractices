#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		string reverseStr(string s, int k);
};

int main()
{
	Solution mysol;
	string s;
	int k;
	cout << "Enter string: ";
	cin >> s;
	cout << "Enter k: ";
	cin >> k;

	cout << mysol.reverseStr(s,k) << endl;
	return 0;
}

string Solution::reverseStr(string s, int k)
{
	int len = s.length();
	int i = 0;
	for(i=0;i<len;i+=2*k)
	{
		cout << "changing.. i: " << i << ", i+k/2: " << (2*i+k)/2 << endl;
		for(int j=i;j<=(2*i+k)/2;j++)
		{
			char t = s[j];
			s[j] = s[2*i+k-1-j];
			s[2*i+k-1-j] = t;
			cout << "changed: " << s << endl;
		}
		cout << s << endl;
	}
	return s;
}
