#include <iostream>
#include <string>
using namespace std;

class Solution
{
	public:
		string reverseStr(string s, int k);
};

int main()
{
	Solution mysol;
	string s;
	int k;
	cout << "Enter string: ";
	cin >> s;
	cout << "Enter k: ";
	cin >> k;

	cout << mysol.reverseStr(s,k) << endl;
	return 0;
}

string Solution::reverseStr(string s, int k)
{
	int len = s.length();
	if(len<k) return s(s.begin(),s.rend());
	int i = 0;
	string res = "";
	for(i=0;i<len;i+=2*k)
	{
		string ns1;
		if(i+k<=len) ns1 = string(s.begin()+i,s.begin()+i+k);
		else ns1 = string(s.begin()+i,s.end());
		cout << "ns1: " << ns1 << endl;

		string ns2;
		if(len<i+k) ns2 = "";
		else if(len<i+(2*k)) ns2 = string(s.begin()+i+k,s.end());
		else ns2 = string(s.begin()+i+k,s.begin()+i+2*k);
		cout << "ns2: " << ns2 << endl;
		res += string(ns1.rbegin(),ns1.rend()) + ns2;
		cout << "res: " << res << endl;
	}
	return res;
}
