#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

class Solution
{
	public:
		int longestPalindrome(string s);
};

int main()
{
	Solution mysol;
	string s;
	cout << "Enter string: ";
	cin >> s;
	cout << mysol.longestPalindrome(s) << endl;
	return 0;
}

int Solution::longestPalindrome(string s)
{
	unordered_map<char,int> map;
	int odd = 0, len = 0;

	for(auto c: s)
	{
		map[c]++;
	}
	for(auto t: map)
	{
		if(t.second % 2 == 0) len+= t.second;
		else { len+= t.second -1; odd = 1; }
	}
	return len+odd;
}
