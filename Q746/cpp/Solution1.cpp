#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int minCostClimbingStairs(vector<int>& cost);
};

int main()
{
	Solution mysol;
	// vector<int> cost = {1,100,100,2,1,100,4,1,2,100,100,2,2,2,100,1};
	// vector<int> cost = {10,15,20};
	vector<int> cost = {1,100,1,1,1,100,1,1,100,1};
	cout << "cost: " << cost.size() << endl;
	cout << mysol.minCostClimbingStairs(cost) << endl;
	return 0;
}

int Solution::minCostClimbingStairs(vector<int>& cost)
{
	int csize = cost.size();
	cout << "\tcost size: " << csize << endl;
	if(cost.size()==0) { cout << "A" << endl; return 0; }
	else
	{
		int  r1, r2;

		if(csize>0)
		{
			vector<int> v1( cost.begin()+1,cost.end() );
			r1 = cost[0] + minCostClimbingStairs( v1 );
			cout << "v1: " << v1.size() << endl;
		}
		else r1 = 0;

		if(csize>1)
		{
			vector<int> v2( cost.begin()+2,cost.end() );
			r2 = cost[1] + minCostClimbingStairs( v2 );
			cout << "v2: " << v2.size() << endl;
		}
		else r2 = 0;

		cout << "B" << endl;
		int subcost = r2<r1?r2:r1;
		cout << "sub cost: " << subcost << endl;
		return r2<r1?r2:r1;
	}
}
