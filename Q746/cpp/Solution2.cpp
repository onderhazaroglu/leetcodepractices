#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int minCostClimbingStairs(vector<int>& cost);
};

int main()
{
	Solution mysol;
	// vector<int> cost = {1,100,100,2,1,100,4,1,2,100,100,2,2,2,100,1};
	// vector<int> cost = {10,15,20};
	vector<int> cost = {1,100,1,1,1,100,1,1,100,1};
	cout << "cost: " << cost.size() << endl;
	cout << mysol.minCostClimbingStairs(cost) << endl;
	return 0;
}

int Solution::minCostClimbingStairs(vector<int>& cost)
{
	int c1 = 0, c2 = 0;
	for(int i=cost.size()-1;i>=0;i--)
	{
		int cn = cost[i] + min(c1,c2);
		c2 = c1;
		c1 = cn;
	}
	return min(c1,c2);
}
