#include <iostream>
using namespace std;

struct ListNode
{
	int val;
	ListNode *next;
	ListNode(int x): val(x), next(NULL) {}
};

class Solution
{
	public:
		ListNode* removeNthFromEnd(ListNode* head, int n);
};

void print_list( ListNode* head )
{
	ListNode* temp = head;
	if ( temp == NULL )
	{
		cout << "List doesn't have a node." << endl;
		return;
	}

	do
	{
		cout << temp->val;
		if ( temp->next != NULL )
		{
			cout << "->";
		}
		temp = temp->next;
	}
	while ( temp != NULL );
	cout << endl;
}


int main()
{
	ListNode* head = new ListNode(1);
	ListNode* temp = head;
	int val = 0, i=0;

	while( val >= 0 )
	{
		i++;
		cout << "Enter " << i << ". element or -1 to quit: ";
		cin >> val;
		if ( val == -1 ) break;
		else if ( i == 1 ) head->val = val;
		else { temp->next = new ListNode(val); temp = temp->next; }
	}

	cout << "Enter node number from end to remove: ";
	cin >> val;

	Solution mysol;
	
	print_list( head );
	head = mysol.removeNthFromEnd( head, val );
	print_list( head );

	return 0;
}

ListNode* Solution::removeNthFromEnd(ListNode* head, int n)
{
	ListNode* prev = head;
	ListNode* nprev = head;
	ListNode* temp = head;
	int i = 0;
	int j = 2;

	// cout << temp->val << endl;

	while( temp->next != NULL)
	{
		temp = temp->next;
		cout << temp->val << endl;
		if ( ++j > n ) { nprev = nprev->next; cout << "nprev = " << nprev->val << endl; }
		if ( ++i > n ) { prev = prev->next; cout << "prev = " << prev->val << endl; }
	}

	cout << "i = " << i << ", n = " << n << endl;
	
	if ( n == i+1 )
	{
		head = head->next;
	}
	else if ( n == 1 )
	{
		prev->next = NULL;
	}
	else prev->next = nprev;
	return head;
}
