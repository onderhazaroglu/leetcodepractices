#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int singleNumber(vector<int>& nums);
};

int main()
{
	Solution mysol;
	// vector<int> nums = {1,3,4,2,1,3,4};
	// vector<int> nums = {1,3,4,2,1,3,4,2,5};
	vector<int> nums = {5,1,3,4,2,1,3,4,2};
	cout << mysol.singleNumber(nums) << endl;
	return 0;
}

int Solution::singleNumber(vector<int>& nums)
{
	int size = nums.size();
	std::sort(nums.begin(),nums.end());

	int i = 0;
	while(i<size)
	{
		if(i+1<size && nums[i] == nums[i+1]) i+=2;
		else break;
	}
	return nums[i];
}
