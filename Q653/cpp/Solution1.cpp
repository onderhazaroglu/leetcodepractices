#include <iostream>
#include <stack>
#include <queue>
#include <unordered_map>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		bool findTarget(TreeNode* root, int k);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(5);
	r1->left = new TreeNode(3);
	r1->left->left = new TreeNode(2);
	r1->left->right = new TreeNode(4);
	r1->right = new TreeNode(6);
	r1->right->right = new TreeNode(7);

	print_tree(r1);

	int k = 8;
	if(mysol.findTarget(r1,k)) cout << "two sum exist." << endl;
	else cout << "two sum does not exist!" << endl;

	return 0;
}

bool Solution::findTarget(TreeNode* root, int k)
{
	if(root == NULL) return false;

	unordered_map<int,int> map;
	queue<TreeNode*> q;
	q.push(root);

	while(!q.empty())
	{
		TreeNode* cur = q.front();
		map[cur->val] = 1;
		cout << cur->val << " is added." << endl;
		unordered_map<int,int>::const_iterator got = map.find (k-cur->val);
		if(got != map.end() && cur->val != k-cur->val) return true;
		else cout << k-cur->val << " is not found!" << endl;
		if(cur->left != NULL) q.push(cur->left);
		if(cur->right != NULL) q.push(cur->right);
		q.pop();
	}
	return false;
}
