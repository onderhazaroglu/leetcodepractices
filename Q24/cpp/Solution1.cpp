#include <iostream>
using namespace std;

struct ListNode
{
	int val;
	ListNode *next;
	ListNode(int x): val(x), next(NULL) {}
};

class Solution
{
	public:
		ListNode * swapPairs(ListNode* head);
		ListNode* mergeTwoLists(ListNode* l1, ListNode* l2);
		ListNode* mergeTwoLists2(ListNode* l1, ListNode* l2);
};

void print_list( ListNode* head )
{
	ListNode* temp = head;
	if ( temp == NULL )
	{
		cout << "List doesn't have a node." << endl;
		return;
	}

	do
	{
		cout << temp->val;
		if ( temp->next != NULL )
		{
			cout << "->";
		}
		temp = temp->next;
	}
	while ( temp != NULL );
	cout << endl;
}


int main()
{
	ListNode* head1 = NULL;
	ListNode* head2 = NULL;

	ListNode* temp1 = head1;
	ListNode* temp2 = head2;
	int val = 0, i=0;

	while( val >= 0 )
	{
		i++;
		cout << "Enter " << i << ". element or -1 to quit: ";
		cin >> val;
		if ( val == -1 ) break;
		else if ( i == 1 )
		{
			head1 = new ListNode(val);
			temp1 = head1;
		}
		else { temp1->next = new ListNode(val); temp1 = temp1->next; }
	}

	val = 0, i=0;

	while( val >= 0 )
	{
		i++;
		cout << "Enter " << i << ". element or -1 to quit: ";
		cin >> val;
		if ( val == -1 ) break;
		else if ( i == 1 )
		{
			head2 = new ListNode(val);
			temp2 = head2;
		}
		else { temp2->next = new ListNode(val); temp2 = temp2->next; }
	}

	Solution mysol;
	ListNode* ml;
	
	print_list( head1 );
	print_list( head2 );

	ml = mysol.mergeTwoLists( head1, head2 );
	cout << "Final:\n";
	print_list( ml );

	ListNode* sml =	mysol.swapPairs( ml );

	cout << "Swapped ml:\n";
	print_list( sml );

	ListNode* n1 = NULL;
	ListNode* n2 = NULL;
	ListNode* n3 = NULL;
	n3 = mysol.mergeTwoLists( n1, n2 );
	print_list( n3 );

	return 0;
}




ListNode* Solution::swapPairs(ListNode* head)
{
	ListNode* t = head;
	ListNode* tn = t->next;

	ListNode* p = NULL;
	ListNode* pp = NULL;
	ListNode* c = head;

	int i = 1;

	while( tn != NULL )
	{
		i++;
		pp = c;
		p = t;
		t = tn;
		tn = tn->next;
		c = t;
		cout << "Current node: " << c->val << "   ======================================" << endl;
		cout << "BEFORE: " << endl;

		cout << "p: " << p->val << ", p->next: ";
		if ( p->next != NULL ) cout << p->next->val;
		cout << endl;
		cout << "c: " << c->val << ", c->next: ";
		if ( c->next != NULL ) cout << c->next->val;
		cout << endl;

		if ( i % 2 == 0 )
		{
			p->next = c->next;
			cout << "A" << endl;
			c->next = p;
			cout << "B" << endl;
			if ( i == 2 ) head = c;
			cout << "head: " << head->val << endl;
			cout << "C" << endl;
		}

		if ( pp != NULL )
		{
			cout << "pp: " << pp->val << ", pp->next: ";
			pp->next = c;
			cout << pp->next->val << endl;
		}

		cout << "D" << endl;
		cout << "AFTER: " << endl;
		cout << "pp: ";
		/*
		if ( pp != NULL ) cout << "pp is NOT NULL" << endl;
		else cout << "pp is NULL!" << endl;
		*/

		if ( pp != NULL ) cout << pp->val;
		cout << ", pp->next: ";
		if ( pp != NULL && pp->next != NULL ) cout << pp->next->val;
		cout << endl;
		cout << "p: " << p->val << ", p->next: ";
		if ( p->next != NULL ) cout << p->next->val;
		cout << endl;
		cout << "c: " << c->val << ", c->next: ";
		if ( c->next != NULL ) cout << c->next->val;
		cout << endl;
		print_list( head );
	}
	return head;
}




ListNode* Solution::mergeTwoLists(ListNode* l1, ListNode* l2)
{
	ListNode* t1 = l1;
	ListNode* t2 = l2;
	int i = 0;
	ListNode* ml = NULL;
	ListNode* head = NULL;


	while( t1 != NULL || t2 != NULL )
	{
		if ( t2 == NULL )
		{
			cout << "t2 is null" << endl;
			if ( ml == NULL ) ml = t1;
			else ml->next = t1;

			if ( i++ == 0 )
			{
				cout << "head is set" << endl;
				head = ml;
			}
			else ml = ml->next;

			break;
		}
		else if ( t1 == NULL )
		{
			cout << "t1 is null" << endl;
			if ( ml == NULL ) ml = t1;
			else ml->next = t2;

			if ( i++ == 0 )
			{
				cout << "head is set" << endl;
				head = ml;
			}
			else ml = ml->next;

			break;
		}
		else
		{
			if ( t1->val <= t2->val )
			{
				cout << "t1(" << t1->val << ") <= t2(" << t2->val << ")" << endl;
				ml == NULL ? ml = new ListNode(t1->val) : ml->next = new ListNode(t1->val);
				t1 = t1->next;
			}
			else
			{
				cout << "t2(" << t2->val << ") < t1(" << t1->val << ")" << endl;
				ml == NULL ? ml = new ListNode(t2->val) : ml->next = new ListNode(t2->val);
				t2 = t2->next;
			}
			if ( i++ == 0 )
			{
				cout << "head is set" << endl;
				head = ml;
			}
			else ml = ml->next;
		}
		cout << "ml --> " << ml->val << endl;
		print_list( head );
	}
	return head;
}

ListNode* Solution::mergeTwoLists2(ListNode* l1, ListNode* l2)
{
	if ( l1 == NULL ) return l2;
	if ( l2 == NULL ) return l1;

	if ( l1->val < l2->val )
	{
		l1->next = mergeTwoLists2( l1->next, l2 );
		return l1;
	}
	else
	{
		l2->next = mergeTwoLists2( l2->next, l1 );
		return l2;
	}
}
