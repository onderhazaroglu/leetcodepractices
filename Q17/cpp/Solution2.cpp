#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
	public:
		string corr[10] = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

		void helper(string digits, unsigned int i, string curr, vector<string> &result);
		vector<string> letterCombinations(string digits);
};

void print_vector(vector<string> v)
{
	cout << "[";
	for(auto i: v)
	{
		cout << i << ", ";
	}
	cout << "]" << endl;
}

int main()
{
	Solution mysol;
	string digits;
	cout << "Enter digits: ";
	cin >> digits;
	vector<string> lcs = mysol.letterCombinations(digits);
	for(auto i: lcs)
	{
		cout << i << ", " << endl;
	}
	return 0;
}

void Solution::helper(string digits, unsigned int i, string curr, vector<string> &result)
{
	cout << "Helper running: " << "digits:[" << digits << "], " << "i:[" << i << "], curr:[" << curr << "]" << endl;
	if(i == digits.length())
	{
		result.push_back(curr);
		print_vector(result);
		return;
	}
	int digit = digits[i] - '0';
	cout << "digit: " << digit << endl;
	for (char c: corr[digit])
	{
		helper(digits, i+1, curr+c, result);
	}
}

vector<string> Solution::letterCombinations(string digits)
{
	if(digits.empty()) return vector<string>();
	vector<string> result;
	helper(digits,0, "", result);
	return result;
}
