#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
	public:
		vector<string> letterCombinations(string digits);
};

void print_vector(vector<string> v)
{
	cout << "[";
	for(auto i: v)
	{
		cout << i << ", ";
	}
	cout << "]" << endl;
}

int main()
{
	Solution mysol;
	string digits;
	cout << "Enter digits: ";
	cin >> digits;
	vector<string> lcs = mysol.letterCombinations(digits);
	for(auto i: lcs)
	{
		cout << i << ", " << endl;
	}
	return 0;
}

vector<string> Solution::letterCombinations(string digits)
{
	vector<string> lcs;
	vector<string> pr,pr1;
	unordered_map<char,string> mapping;
	mapping['2'] = "abc";
	mapping['3'] = "def";
	mapping['4'] = "ghi";
	mapping['5'] = "jkl";
	mapping['6'] = "mno";
	mapping['7'] = "pqrs";
	mapping['8'] = "tuv";
	mapping['9'] = "wxyz";

	unsigned int size = digits.length();	

	for(unsigned int i=0;i<size;i++)
	{
		pr.push_back( mapping[digits[i]] );
	}
	cout << "pr: ";
	print_vector(pr);

	for(unsigned int j=0;j<pr[0].length();j++)
	{
		pr1.push_back( string(1, pr[0][j] ) );
		lcs.push_back( string(1, pr[0][j] ) );
	}
	cout << "pr1: ";
	print_vector(pr1);

	for(unsigned int k=1;k<size;k++)
	{
		lcs.clear();
		vector<string> pr2;
		for(unsigned int j=0;j<pr[k].length();j++)
		{
			pr2.push_back( string(1, pr[k][j] ) );
		}
		cout << "pr2: ";
		print_vector(pr2);
		for(unsigned int m=0;m<pr1.size();m++)
		{
			for(unsigned int n=0;n<pr2.size();n++)
			{
				lcs.push_back( pr1[m]+pr2[n] );
			}
			cout << "lcs: ";
			print_vector(lcs);
		}
		pr1 = lcs;
		cout << "pr1: ";
		print_vector(pr1);
	}
	return lcs;
}
