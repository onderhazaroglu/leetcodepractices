#include <iostream>
#include <unordered_map>
#include <vector>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		vector<int> findMode(TreeNode* root);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(5);
	r2->left = new TreeNode(4);
	r2->right = new TreeNode(8);
	r2->left->left = new TreeNode(4);
	r2->right->left = new TreeNode(7);


	for(int i: mysol.findMode(r2)) cout << i << " ";
	cout << endl;
	return 0;
}

vector<int> Solution::findMode(TreeNode* root)
{
	if(!root) return vector<int>();
	vector<int> res;
	unordered_map<int,int> map;
	stack<TreeNode*> s;
	s.push(root);
	int max = 0;
	TreeNode* tn;

	while(!s.empty())
	{
		tn = s.top();
		if(++map[tn->val]>=max)
		{
			if(map[tn->val]==max) { cout << "map[" << tn->val << "]:" << map[tn->val] << endl; res.push_back(tn->val); }
			else { cout << "greater" << endl; res = vector<int> {tn->val}; }
			max = map[tn->val];
		}
		for(int i: res) cout << i << " ";
		cout << endl;

		s.pop();
		if(tn->left) s.push(tn->left);
		if(tn->right) s.push(tn->right);
	}
	return res;
}
