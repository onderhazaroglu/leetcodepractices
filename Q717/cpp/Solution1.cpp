#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		bool isOneBitCharacter(vector<int>& bits);
};

int main()
{
	Solution mysol;
	vector<int> bits = {1,1,1,0,0,1,0,1,1,1,0};
	if(mysol.isOneBitCharacter(bits)) cout << "One bit." << endl;
	else cout << "Not one bit!" << endl;
	return 0;
}

bool Solution::isOneBitCharacter(vector<int>& bits)
{
	unsigned int i = 0, last = 0;
	while(i<bits.size())
	{
		if(bits[i]==0) { i++; last = true; }
		else if(bits[i]==1) { i+=2; last = false; }
	}
	return last;
}
