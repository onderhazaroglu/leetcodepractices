#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		int compress(vector<char>& chars);
};

int main()
{
	Solution mysol;
	// vector<char> chars = {'a','a','b','b','c','c','c'};
	vector<char> chars = {'a','a','a','a','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b'};
	cout << endl << endl;
	cout << mysol.compress(chars) << endl;
	return 0;
}

vector<char> Solution::compress(vector<char>& chars)
{
	int lo=0;
	int cnt=0;
	for(int i=0; i<chars.size(); i++){
		cnt++;
		if(i==chars.size()-1||chars[i]!=chars[i+1]){
			chars[lo++]=chars[i];
			if(cnt>1){
				string nums=to_string(cnt);
				for(int i=0; i<nums.length(); i++){
					chars[lo++]=nums[i];
				}
			}
			cnt=0;
		}
	}
	return lo;
}
