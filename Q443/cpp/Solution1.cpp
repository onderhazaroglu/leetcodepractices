#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		<vector>int compress(vector<char>& chars);
};

int main()
{
	Solution mysol;
	// vector<char> chars = {'a','a','b','b','c','c','c'};
	vector<char> chars = {'a','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b'};
	cout << mysol.compress(chars) << endl;
	return 0;
}

vector<int> Solution::compress(vector<char>& chars)
{
	int size = chars.size();
	if(size<2) return size;
	int len = 1, p = 0, cur = 0;
	for(int i=1;i<size;i++)
	{
		cout << "c(" << i << "):" << chars[i] << ", p(" << p << "):" << chars[p];
		if(chars[i]==chars[p])
		{
			if(i-p==1) cur++, len++;
			else if(++cur>9) len++, cur = 0;
		}
		else len++, p = i;
		cout << ", len:" << len << endl;
	}
	return len;
}
