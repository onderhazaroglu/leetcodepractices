#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
	public:
		vector<char> compress(vector<char>& chars);
};

int main()
{
	Solution mysol;
	// vector<char> chars = {'a','a','b','b','c','c','c'};
	vector<char> chars = {'a','a','a','a','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b','b'};
	vector<char> res = mysol.compress(chars); 
	cout << endl << endl;
	for(char c: mysol.compress(chars)) cout << c;
	cout << endl;
	return 0;
}

vector<char> Solution::compress(vector<char>& chars)
{
	int size = chars.size();
	if(size<2) return vector<char>(chars);
	int p = 0, cur = 1;
	string res = { chars[0] };

	for(int i=1;i<size;i++)
	{
		// cout << "c(" << i << "):" << chars[i] << ", p(" << p << "):" << chars[p];
		if(chars[i]==chars[p]) cur++;
		else
		{
			if(cur>1) res+= to_string(cur);
			cur = 1, p = i, res.push_back(chars[i]);
		}
	}
	if(cur>1) res+= to_string(cur);
	cout << "res: " << res << endl;
	return vector<char>(res.begin(),res.end());
}
