#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <set>
using namespace std;

struct setcmp
{
	bool operator()(const string & l, const string & r)
	{
		return l.size() > r.size() || (l.size() == r.size() && l < r);
	}
};

class Solution {
	public:
		string longestWord(vector<string>& words) {

			/*
			sort(words.begin(), words.end(), [](const string & l, const string & r)
					{
					return l.size() > r.size() || (l.size() == r.size() && l < r);
					});
					*/

			set<string, setcmp> t;

			for(auto w : words)
			{
				t.insert(w);
				for(auto iter = t.begin();iter!=t.end();iter++)
				{
					cout << *iter << endl;
				}
			}

			for(auto iter = t.begin();iter!=t.end();iter++)
			{
				cout << *iter << endl;
			}

			for(auto w : t)
			{
				bool ok = true;
				for(unsigned int len = 1; len <= w.size(); ++len)
				{
					if(t.find(w.substr(0, len)) == t.end())
					{
						ok = false;
						break;
					}
				}

				if(ok == true)
				{
					return w;
				}
			}

			return "";
		}
};


int main()
{
	Solution mysol;
	vector<string> words { "a","banana","app","appl","apply","ap","apple" };
	cout << mysol.longestWord(words) << endl;
	return 0;
}
