#include <iostream>
#include <set>
using namespace std;

struct comp
{
	bool operator() (const string & l, const string & r)
	{
		return l.length() > r.length() || (l.length() == r.length() && l<r);
	}
};

int main()
{
	set<string,comp> s;
	string str = "";
	while(str != "q")
	{
		cout << "Enter string: ";
		cin >> str;
		s.insert(str);
	}

	cout << endl << endl << "Set content:" << endl;
	for(auto st: s)
	{
		cout << st << endl;
	}
}
