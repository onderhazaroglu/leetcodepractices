#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <set>
using namespace std;

class Solution
{
	public:
		string longestWord(vector<string>& words);
};

int main()
{
	Solution mysol;
	vector<string> words { "a","banana","app","appl","apply","ap","apple" };
	cout << mysol.longestWord(words) << endl;
	return 0;
}

string Solution::longestWord(vector<string>& words)
{
	string smallest = "";
	unsigned int longest = 0;
	unordered_map<string,int> m;

	for(string s: words) m[s] = 1;

	for(string s: words)
	{
		bool canbebuilt = true;
		for(unsigned int i=1;i<s.length();i++)
		{
			if(m.find(s.substr(0,i)) == m.end())
			{
				cout << s << " can NOT be built! ";
				cout << s.substr(0,i) << " is not in the vector." << endl;
				canbebuilt = false;
				break;
			}
		}

		if(canbebuilt)
		{
			cout << s << " can be built." << endl;
			if(smallest == "" || s.length()>longest || (s.length()==longest && s<smallest) )
			{
				smallest = s;
				longest = s.length();
			}
		}
	}
	return smallest;
}
