#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <set>
using namespace std;

struct cmp
{
	bool operator()(const string & l, const string & r)
	{
		return l.length() > r.length() || (l.length()==r.length() && l<r);
	}
};

class Solution
{
	public:
		string longestWord(vector<string>& words);
};

int main()
{
	Solution mysol;
	vector<string> words { "a","banana","app","appl","apply","ap","apple" };
	cout << mysol.longestWord(words) << endl;
	return 0;
}

string Solution::longestWord(vector<string>& words)
{
	set<string,cmp> myset;

	for(string s: words) myset.insert(s);

	for(auto s: myset) cout << s << endl;

	for(string s: myset)
	{
		bool canbebuilt = true;
		for(unsigned int i=1;i<s.length();i++)
		{
			if(myset.find(s.substr(0,i)) == myset.end())
			{
				cout << s << " can NOT be built! ";
				cout << s.substr(0,i) << " is not in the vector." << endl;
				canbebuilt = false;
				break;
			}
		}
		if(canbebuilt) return s;
	}
	return "";
}
