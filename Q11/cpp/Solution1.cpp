#include <iostream>
#include <vector>
using namespace std;

class Solution
{
	public:
		int maxArea(vector<int>& height);
};

int main()
{
	Solution sol1;

	vector<int> v;
	v.push_back( 3 );
	v.push_back( 1 );
	v.push_back( 5 );
	v.push_back( 7 );
	v.push_back( 2 );
	v.push_back( 4 );

	cout << sol1.maxArea( v ) << endl;

	return 0;
}

int Solution::maxArea(vector<int>& height)
{
	int max = 0, h = 0, b = 0;
	int size = static_cast<int>( height.size() );

	for(int i=0;i<size-1;i++)
	{
		for(int j=i+1;j<size;j++)
		{
			if(height[i]<height[j]) h = height[i];
			else h = height[j];

			if(i-j < 0) b = -(i-j);
			else b = i-j;

			if ( h * b > max ) max = h * b;
		}
	}
	return max;
}
