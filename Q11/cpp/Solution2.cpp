#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		int maxArea(vector<int>& height);
};

int main()
{
	Solution sol1;

	vector<int> v;
	v.push_back( 3 );
	v.push_back( 1 );
	v.push_back( 5 );
	v.push_back( 7 );
	v.push_back( 2 );
	v.push_back( 4 );

	cout << sol1.maxArea( v ) << endl;

	return 0;
}

int Solution::maxArea(vector<int>& height)
{
	int area = 0, max = 0;
	int size = height.size();
	if ( size < 2 ) return max;

	int l = 0, r = height.size()-1;

	while(l<r)
	{
		// area = ( height[l]<height[r] ? height[l] : height[r] ) * (r - l);
		area = std::min(height[l], height[r]) * (r - l);
		cout << "l=" << l << "(" << height[l] << "), r=" << r << "(" << height[r] << "), area=" << area << endl;

		if (height[l]<height[r]) l++;
		else r--;
		if (area>max) max = area;
	}

	return max;
}
