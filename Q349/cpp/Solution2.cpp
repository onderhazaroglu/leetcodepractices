#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		vector<int> intersection(vector<int>& nums1,vector<int>& nums2);
};

int main()
{
	Solution mysol;
	vector<int> nums1 = {1,3,4,2,1,3,4};
	vector<int> nums2 = {3,2,5,7,1,3,8};
	for(auto i: mysol.intersection(nums1,nums2))
	{
		cout << i << " " << endl;
	}
	return 0;
}

vector<int> Solution::intersection(vector<int>& nums1,vector<int>& nums2)
{
	if(nums1.size()==0 || nums2.size()==0) return vector<int>();

	set<int> s(nums1.begin(),nums1.end());
	vector<int> res;

	for(auto i: nums2)
	{
		if(s.find(i) != s.end())
		{
			res.push_back(i);
			s.erase(i);
		}
	}
	return res;
}
