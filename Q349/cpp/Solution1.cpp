#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
using namespace std;

class Solution
{
	public:
		vector<int> intersection(vector<int>& nums1,vector<int>& nums2);
};

int main()
{
	Solution mysol;
	vector<int> nums1 = {1,3,4,2,1,3,4};
	vector<int> nums2 = {3,2,5,7,1,3,8};
	for(auto i: mysol.intersection(nums1,nums2))
	{
		cout << i << " " << endl;
	}
	return 0;
}

vector<int> Solution::intersection(vector<int>& nums1,vector<int>& nums2)
{
	if(nums1.size()==0 || nums2.size()==0) return vector<int>();
	sort(nums1.begin(),nums1.end());
	sort(nums2.begin(),nums2.end());
	size_t i=0, j=0;
	set<int> s;
	while(i<nums1.size() && j<nums2.size())
	{
		if(nums1[i]==nums2[j])
		{
			s.insert(nums1[i]);
			i++;j++;
		}
		else
		{
			if(nums1[i]<nums2[j]) i++;
			else j++;
		}
	}
	return vector<int>(s.begin(),s.end());
}
