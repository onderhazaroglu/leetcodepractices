#include <iostream>
#include <stack>
using namespace std;

class MyQueue {
	public:
		stack<int> in, out;
		/** Initialize your data structure here. */
		MyQueue() {
			in = stack<int>();
			out = stack<int>();
		}

		/** Push element x to the back of queue. */
		void push(int x) {
			in.push(x);
		}

		/** Removes the element from in front of queue and returns that element. */
		int pop() {
			int res = peek();
			out.pop();
			return res;
		}

		/** Get the front element. */
		int peek() {
			if(out.empty())
				while(!in.empty())
					out.push(in.top()), in.pop();
			return out.top();
		}

		/** Returns whether the queue is empty. */
		bool empty() {
			return in.empty() && out.empty();
		}
};

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue obj = new MyQueue();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.peek();
 * bool param_4 = obj.empty();
 */

int main()
{
	MyQueue mq;
	mq.push(2);
	mq.push(3);
	mq.push(5);
	cout << "top after 5:" << mq.peek() << endl;
	cout << endl;
	mq.push(7);
	mq.push(8);
	mq.push(9);

	while(!mq.empty())
	{
		cout << mq.pop() << " ";
	}
	cout << endl;
	return 0;
}
