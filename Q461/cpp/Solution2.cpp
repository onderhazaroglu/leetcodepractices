#include <iostream>
#include <bitset>
using namespace std;

class Solution
{
	public:
		int hammingDistance(int x, int y);
};

int main()
{
	Solution mysol;
	int x, y;

	cout << "Enter x: ";
	cin >> x;
	bitset<16> xx(x);
	cout << xx << endl;

	cout << "Enter y: ";
	cin >> y;
	bitset<16> yy(y);
	cout << yy << endl;

	int a = mysol.hammingDistance(x,y);
	bitset<16> aa(a);
	cout << a << endl;
	cout << aa << endl;
	return 0;
}

int Solution::hammingDistance(int x, int y)
{
	int d = 0, n = x ^ y;

	while(n)
	{
		d++;
		n &= n - 1;
	}
	return d;
}
