#include <iostream>
using namespace std;

class Solution
{
	public:
		int hammingDistance(int x, int y);
};

int main()
{
	Solution mysol;
	int x, y;

	cout << "Enter x: ";
	cin >> x;
	cout << "Enter y: ";
	cin >> y;

	cout << mysol.hammingDistance(x,y) << endl;
	return 0;
}

int Solution::hammingDistance(int x, int y)
{
	if ( x == y ) return 0;

	int d = 0, n = x ^ y;

	while(n)
	{
		d += n & 1;
		n >>= 1;
	}
	return d;
}
