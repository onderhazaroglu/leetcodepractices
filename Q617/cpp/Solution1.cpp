#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		TreeNode* mergeTrees(TreeNode* t1, TreeNode* t2);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	TreeNode* r3 = mysol.mergeTrees(r1,r2);
	print_tree(r3);
	cout << r3->val << endl;

	return 0;
}

TreeNode* Solution::mergeTrees(TreeNode* t1, TreeNode* t2)
{
	cout << "t1:";
	if(t1 != NULL) cout << t1->val;
  else cout << "N";

 	cout<< ", t2:";
 	if(t2 != NULL) cout << t2->val;
	else cout << "N";
	cout << endl;

	TreeNode* nt = new TreeNode(0);
	if(t1 == NULL && t2 == NULL) return NULL;
	else if(t1 == NULL) { nt->val = t2->val; return nt; }
	else if(t2 == NULL) { nt->val = t1->val; return nt; }
	else nt->val = t1->val + t2->val;

	cout << "A" << endl;

	if(t1 != NULL && t2 != NULL) { cout << "B1" << endl; nt->left = mergeTrees(t1->left, t2->left); cout << "B1-2" << endl; nt->right = mergeTrees(t1->right, t2->right); }
	else if (t1 == NULL) { cout << "B2" << endl; nt->left = t2->left; nt->right = t2->right; }
	else if (t2 == NULL) { cout << "B3" << endl; nt->left = t1->left; nt->right = t1->right; }

	cout << t1->val << "," << t2->val << " end." << endl;
	return nt;
}
