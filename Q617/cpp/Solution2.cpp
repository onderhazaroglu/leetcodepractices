#include <iostream>
#include <stack>
using namespace std;

struct TreeNode {
	int val;
	TreeNode *left = NULL;
	TreeNode *right = NULL;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

void print_tree(TreeNode* root)
{
	stack<TreeNode*> st;
	st.push(root);
	TreeNode* tn;

	while(!st.empty())
	{
		tn = st.top();
		st.pop();
		cout << tn->val << " (";
		if(tn->left != NULL)
		{
			cout << " left: " << tn->left->val;
			st.push(tn->left);
		}

		if(tn->right != NULL)
		{
			cout << " right: " << tn->right->val;
			st.push(tn->right);
		}
		cout << ")" << endl;
	}
}


class Solution {
	public:
		TreeNode* mergeTrees(TreeNode* t1, TreeNode* t2);
};

int main()
{
	Solution mysol;
	TreeNode* r1 = new TreeNode(1);
	r1->left = new TreeNode(2);
	r1->left->right = new TreeNode(3);

	TreeNode* r2 = new TreeNode(2);
	r2->left = new TreeNode(1);
	r2->right = new TreeNode(3);
	r2->left->right = new TreeNode(4);
	r2->right->right = new TreeNode(7);

	TreeNode* r3 = mysol.mergeTrees(r1,r2);
	print_tree(r3);
	cout << r3->val << endl;

	return 0;
}

TreeNode* Solution::mergeTrees(TreeNode* t1, TreeNode* t2)
{
	cout << "t1:";
	if(t1 != NULL) cout << t1->val;
  else cout << "N";

 	cout<< ", t2:";
 	if(t2 != NULL) cout << t2->val;
	else cout << "N";
	cout << endl;

	if ( t1 && t2 ) {
		TreeNode* nt = new TreeNode(t1->val + t2->val);
		nt->left = mergeTrees(t1->left, t2->left);
		nt->right = mergeTrees(t1->right, t2->right);
		return nt;
	} else return t1 ? t1 : t2;
}
